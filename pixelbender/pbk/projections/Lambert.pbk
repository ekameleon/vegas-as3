/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

<languageVersion: 1.0;>

kernel Lambert
<   
    namespace   : "graphics.filters.projections";
    vendor      : "eKameleon";
    version     : 1 ;
    description : "In geometry, the Lambert projection is a conic map projection." ; 
>
{
    /**
     * The aspect ratio defines with the width and height of the picture to transform, typical ratio should be 2:1.
     */
    parameter float2 aspectRatio
    < 
        minValue     : float2(    0.0 ,    0.0 );
        maxValue     : float2( 4096.0 , 4096.0 );
        defaultValue : float2(  500.0 ,  250.0 );
    >;
    
    /**
     * Specifies an angle, as a degree between 1 and 360, for the field of view of the projection.
     */
    parameter float fieldOfView
    <
        minValue     :   1.0;
        maxValue     : 359.0;
        defaultValue : 100.0;
    >;
    
    /**
     * The distance between the camera and the view located in the z-axis.
     */
    parameter float focalLength
    <
        minValue: 1.0;
        maxValue: 4096.0;
        defaultValue: 128.0;
    >;
    
    input  image4 source;
    output pixel4 result;
    
    void evaluatePixel()
    {      
        float pi = 3.14159265358979;
        
        float w = aspectRatio.x ;
        float h = aspectRatio.y ;
        
        float rads = 2.0 * pi / w ;
        
        float2 pos = outCoord() ;
        
        float x = pos.x;
        float y = pos.y;
        
        float dx = x - w / 2.0 ;
        float dy = y - h / 2.0 ;
        
        float a = atan( dy , dx );
        float r = sqrt( dx * dx + dy * dy );
        
        float rho_FOV = ( fieldOfView * pi / 180.0 ) ;
        
        float rho_fov;
        float rho;
        float theta;
        
        float xe;
        float ye;
        
        float lat;
        float lon;
        
        rho_fov = tan( rho_FOV / 4.0 );
        rho = ( r / focalLength ) * rho_fov;
        
        if ( rho < 1.0 )
        {
            theta = 2.0 * asin( rho );
            
            lat = theta;
            lon = a - pi / 4.0 ;
            
            lat = mod( lat + pi, pi ) - pi / 2.0 ;
            lon = mod( lon + pi, pi * 2.0 ) - pi ;
            
            xe = -lon/rads ;
            ye =  lat/rads ;
        }
        else
        {
            xe = -1.0;
            ye = -1.0;
        }
        
        result = sampleNearest( source , float2( w/2.0 - xe, h/2.0 - ye ) );
    }
}
