﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

package molecule.samples 
{
    import molecule.Layout;
    
    import system.signals.Signaler;
    
    import flash.display.DisplayObjectContainer;
    import flash.geom.Rectangle;
    
    public class LayoutClass implements Layout 
    {
        public function get align():uint
        {
            return _align;
        }
        
        public function set align(value:uint):void
        {
            _align = value;
        }
        
        public function get bounds():Rectangle
        {
            return _bounds;
        }
        
        public function set bounds(area:Rectangle):void
        {
            _bounds = area;
        }
        
        public function get container():*
        {
            return _container;
        }
        
        public function set container( container:* ):void
        {
            _container = container ;
        }
        
        public function get measuredHeight():Number
        {
            return _bounds.height;
        }
        
        public function set measuredHeight(value:Number):void
        {
            _bounds.height = value;
        }
        
        public function get measuredWidth():Number
        {
            return _bounds.width;
        }
        
        public function set measuredWidth(value:Number):void
        {
            _bounds.width = value;
        }
        
        public function get renderer():Signaler
        {
            return _renderer;
        }
        
        public function set renderer(signal:Signaler):void
        {
            _renderer = signal ;
        }
        
        public function get updater():Signaler
        {
            return _updater ;
        }
        
        public function set updater(signal:Signaler):void
        {
            _updater = signal ;
        }
        
        public function isLocked():Boolean
        {
            return _locked;
        }
        
        public function lock():void
        {
            _locked = true;
        }
        
        public function unlock():void
        {
            _locked = false;
        }
        
        public function measure():void
        {
            throw "measure";
        }
        
        public function render():void
        {
            throw "render";
        }
        
        public function run(...arguments:Array):void
        {
            throw "run";
        }
        
        public function update():void
        {
            throw "update";
        }
        
        private var _align:uint;
        
        private var _bounds:Rectangle;
        
        private var _container:DisplayObjectContainer ;
        
        private var _locked:Boolean ;
        
        private var _renderer:Signaler ;
        
        private var _updater:Signaler ;

        public function get finishIt():Signaler
        {
            return null;
        }

        public function set finishIt(signal:Signaler):void
        {
        }

        public function get phase():String
        {
            return "";
        }

        public function get running():Boolean
        {
            return false;
        }

        public function get startIt():Signaler
        {
            return null;
        }

        public function set startIt(signal:Signaler):void
        {
        }

        public function notifyFinished():void
        {
        }

        public function notifyStarted():void
        {
        }

        public function clone():*
        {
        }
    }
}
