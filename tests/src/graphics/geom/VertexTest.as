/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2012
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

package graphics.geom 
{
    import library.ASTUce.framework.TestCase;
    
    public class VertexTest extends TestCase 
    {
        public function VertexTest(name:String = "")
        {
            super( name );
        }
        
        public var v:Vertex;
        
        public function setUp():void
        {
            v = new Vertex(10, 20, 30, 100, 200, 300) ;
        }
        
        public function tearDown():void
        {
            v = undefined ;
        }
        
        public function testConstructor():void
        {
            assertNotNull( v, "01 - constructor is null") ;
            assertTrue( v is Vertex, "02 - constructor is an instance of Vertex.") ;
        }
        
        public function testInherit():void
        {
            assertTrue( v is Vector3 , "inherit Vector3 failed.") ;
        }   
        
        public function testToSource():void
        {
            assertEquals( v.toSource() , "new graphics.geom.Vertex(10,20,30,100,200,300)", "toSource failed : " + v.toSource() ) ;
        }
        
        public function testToString():void
        {
            assertEquals( v.toString() , "[Vertex:{x:10,y:20,z:30,tx:100,ty:200,tz:300}]", "toString failed : " + v.toString() ) ;
        }
        
        public function testSX():void
        {
            assertEquals( v.sx , 0, "sx property failed : " + v.sx ) ;
        }
        
        public function testSY():void
        {
            assertEquals( v.sy , 0, "sy property failed : " + v.sy ) ;
        }
        
        public function testTX():void
        {
            assertEquals( v.tx , 100, "tx property failed : " + v.tx ) ;
        }
        
        public function testTY():void
        {
            assertEquals( v.ty , 200, "ty property failed : " + v.ty ) ;
        }
        
        public function testTZ():void
        {
            assertEquals( v.tz , 300, "tz property failed : " + v.tz ) ;
        }
        
        public function testWX():void
        {
            assertEquals( v.wx , 100, "wx property failed : " + v.wx ) ;
        }
        
        public function testWY():void
        {
            assertEquals( v.wy , 200, "wy property failed : " + v.wy ) ;
        }
        
        public function testWZ():void
        {
            assertEquals( v.wz , 300, "wz property failed : " + v.wz ) ;
        }
        
        public function testX():void
        {
            assertEquals( v.x , 10, "x property failed : " + v.x ) ;
        }
        
        public function testY():void
        {
            assertEquals( v.y , 20, "y property failed : " + v.y ) ;
        }
        
        public function testZ():void
        {
            assertEquals( v.z , 30, "z property failed : " + v.z ) ;
        }
        
        public function testGetTransform():void
        {
            var tv:Vector3 = v.getTransform() ;
            assertTrue(tv is Vector3, "01 - getTransform() method failed.") ;
            assertEquals(tv.x, 100  , "02 - getTransform() method failed with the x value.") ;
            assertEquals(tv.y, 200  , "03 - getTransform() method failed with the y value.") ;
            assertEquals(tv.z, 300,   "04 - getTransform() method failed with the z value.") ;
        }
        
        public function testGetWorld():void
        {
            var world:Vector3 = v.getWorld() ;
            assertEquals( world.x , 100 , "01 - getWorld() method failed with the x value.") ;
            assertEquals( world.y , 200 , "02 - getWorld() method failed with the y value.") ;
            assertEquals( world.z , 300 , "03 - getWorld() method failed with the z value.") ;
        }
        
        public function testClone():void
        {
            var clone:Vertex = v.clone() as Vertex ;
            
            clone.x  =  1000 ;
            clone.y  =  2000 ;
            clone.z  =  3000 ;
            clone.sx =  4000 ;
            clone.sy =  5000 ;
            clone.tx =  6000 ;
            clone.ty =  7000 ;
            clone.tz =  8000 ;
            clone.wx =  9000 ;
            clone.wy = 10000 ;
            clone.wz = 11000 ;
            
            assertNotNull( clone , "01 - clone method failed." ) ;
            
            assertFalse( v.x == clone.x   , "02-01 - clone property failed, v.x:"  + v.x  + " must be different of clone.x:" + clone.x   ) ;
            assertFalse( v.y == clone.y   , "02-02 - clone property failed, v.y:"  + v.y  + " must be different of clone.x:" + clone.y   ) ;
            assertFalse( v.z == clone.z   , "02-03 - clone property failed, v.z:"  + v.z  + " must be different of clone.z:" + clone.z   ) ;
            assertFalse( v.sx == clone.sx , "02-04 - clone property failed, v.sx:" + v.sx + " must be different of clone.sx:" + clone.sx ) ;
            assertFalse( v.sy == clone.sy , "02-05 - clone property failed, v.sy:" + v.sy + " must be different of clone.sy:" + clone.sy ) ;
            assertFalse( v.tx == clone.tx , "02-06 - clone property failed, v.tx:" + v.tx + " must be different of clone.tx:" + clone.tx ) ;
            assertFalse( v.ty == clone.ty , "02-07 - clone property failed, v.ty:" + v.ty + " must be different of clone.ty:" + clone.ty ) ;
            assertFalse( v.tz == clone.tz , "02-08 - clone property failed, v.tz:" + v.tz + " must be different of clone.tz:" + clone.tz ) ;
            assertFalse( v.wx == clone.tx , "02-09 - clone property failed, v.wx:" + v.wx + " must be different of clone.tx:" + clone.wx ) ;
            assertFalse( v.wy == clone.ty , "02-10 - clone property failed, v.wy:" + v.wy + " must be different of clone.ty:" + clone.wy ) ;
            assertFalse( v.wz == clone.tz , "02-11 - clone property failed, v.wz:" + v.wz + " must be different of clone.tz:" + clone.wz ) ;
        }
        
        public function testEquals():void
        {
            var ve:Vector3 = new Vertex(10, 20, 30, 100, 200, 300) ;
            assertTrue( v.equals(ve) , "equals method failed.") ;
        }
    }
}