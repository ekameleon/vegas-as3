﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2012
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package graphics.text 
{
    import library.ASTUce.framework.TestCase;

    import flash.text.StyleSheet;

    public class mergeStyleSheetTest extends TestCase 
    {
        public function mergeStyleSheetTest(name:String = "")
        {
            super( name );
        }
        
        public function testMergeStyleSheet():void
        {
            var style1:StyleSheet = new StyleSheet() ;
            var style2:StyleSheet = new StyleSheet() ;
            
            style1.parseCSS
            (<![CDATA[
            p { color: #00FF00; }
            .version { color: #FFFFFF; font-family: Arial; font-weight: normal; }
            .title { color: #FF0000; font-family: Arial; font-size: 18px; text-align: left; }
            ]]>);
            
            style2.parseCSS
            (<![CDATA[
            .version { color: #000000; font-size: 13px; }
            .title { color: #DE7721; font-family: Gotham Bold; }
            .test { color: #FF00FF; }
            ]]>);
            
            mergeStyleSheet( style1 , style2 ) ;
            
            var names:Array = style1.styleNames ;
            
            assertTrue( names.indexOf("p")        > -1 , "#1-1" ) ;
            assertTrue( names.indexOf(".version") > -1 , "#1-2" ) ;
            assertTrue( names.indexOf(".title")   > -1 , "#1-3" ) ;
            assertTrue( names.indexOf(".test")    > -1 , "#1-4" ) ;
            
            assertTrue( names.indexOf(".unknow")    == -1 , "#2" ) ;
        }
    }
}
