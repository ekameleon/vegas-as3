﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2011
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.proto.screens
{
    import graphics.easings.backOut;
    import graphics.transitions.TweenTo;

    import molecule.logger;
    import molecule.proto.display.atom;
    import molecule.render.starling.display.dragger;

    import starling.display.Quad;
    import starling.display.Sprite;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;

    import flash.geom.Rectangle;
    
    /**
     * The main screen of the application.
     */
    public class Body extends Sprite
    {
        public function Body()
        {
            dragger.target = atom ;
            
            var background:Quad = new Quad(1,40,0x000000);
            
            background.x = atom.x ;
            background.y = atom.y ;
            
            addChild( background ) ;
            addChild( atom ) ;
            
            atom.useHandCursor = false ;
            
            atom.addEventListener( TouchEvent.TOUCH , _touch ) ;
            
            var tween:TweenTo = new TweenTo( background , { width : 400 } , backOut , 24 ) ;
            
            tween.run() ;
        }
        
        protected function touch( e:TouchEvent ):void
        {
            var touch:Touch   = e.getTouch( atom );
            switch( touch.phase )
            {
                case TouchPhase.BEGAN :
                {
                    //dragger.startDrag(false, new Rectangle(25,25,360,0)) ;
                    break ;
                }
                case TouchPhase.ENDED :
                {
                    //dragger.stopDrag() ;
                    break ;
                }
            }
        }
        
        protected var isOver:Boolean ;
        
        protected var isPress:Boolean ;
        
        /**
         * @private
         */
        private function _touch( e:TouchEvent ):void 
        {
            var touch:Touch = e.getTouch( atom ) ;
            
            switch( touch.phase )
            {
                case TouchPhase.BEGAN :
                {
                    isPress = true ;
                    if( isOver )
                    {
                        stage.removeEventListener( TouchEvent.TOUCH , _checkIsOut ) ;
                        isOver = false ;
                    }
                    logger.info( this + " press" ) ;
                    break ;
                }
                
                case TouchPhase.HOVER :
                {
                    if( !isOver )
                    {
                        logger.info( this + " rollover" ) ;
                        isOver = true ;
                        stage.addEventListener( TouchEvent.TOUCH , _checkIsOut ) ;
                    }
                    break ;
                }
                case TouchPhase.ENDED :
                {
                    if( isPress ) 
                    {
                        isPress = false ;
                        if ( _isOut( touch.globalX , touch.globalY ) ) 
                        {
                            logger.info( this + " releaseOutside" ) ;
                        }
                        else
                        {
                            logger.info( this + " release" ) ;
                        }
                    }
                    break ;
                }
            }
        }
        
        
        /**
         * @private
         */
        private function _isOut( x:Number , y:Number ):Boolean
        {
            var b:Rectangle = atom.getBounds(stage);
            return (x < b.x) || (x > b.x + b.width) || (y < b.y) || (y > b.y + b.height) ; 
        }
        
        /**
         * @private
         */
        private function _checkIsOut( e:TouchEvent ):void 
        {
            var touch:Touch = e.getTouch( stage , TouchPhase.HOVER ) ;
            if( touch )
            {
                if( stage )
                {
                    if ( _isOut( touch.globalX , touch.globalY ) ) 
                    {
                        stage.removeEventListener( TouchEvent.TOUCH , _checkIsOut ) ;
                        isOver = false;
                        logger.info( this + " rollout" ) ;
                    }
                }
            }
        }
    }
}
