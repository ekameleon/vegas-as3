﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.buttons 
{
    import graphics.FillGradientStyle;
    
    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.MouseEvent;
    import flash.filters.DropShadowFilter;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#666666")]
    
    public class BackgroundButtonExample extends Sprite 
    {
        public function BackgroundButtonExample()
        {
            //////////
            
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            
            //////////
            
            var groupName:String = "background_button_group" ;
            
            bt1           = new BackgroundButton(200,20) ;
            bt1.x         = 25 ;
            bt1.y         = 50 ;
            bt1.groupName = groupName ;
            bt1.toggle    = true ;
            
            addChild(bt1) ;
            
            //////////
            
            bt2           = new BackgroundButton(200,20) ;
            bt2.x         = 25 ;
            bt2.y         = 80 ;
            bt2.groupName = groupName ;
            bt2.toggle    = true ;
            
            addChild(bt2) ;
            
            //////////
            
            var init:Object = 
            { 
                theme          : new graphics.FillGradientStyle( "linear" , [0x000000, 0xFF0000] , [1, 1], [0, 255] , null, "pad" , "rgb" , 0 )  ,
                themeFilters   : [ new DropShadowFilter(4,45,0,0.7) ] , 
                useGradientBox : true ,
                useLight       : false
            } ;
            
            bt3           = new BackgroundButton(200,40) ;
            bt3.x         =  25 ;
            bt3.y         = 110 ;
            bt3.groupName = groupName ;
            bt3.toggle    = true ;
            bt3.style     = new BackgroundButtonStyle( init ) ;
            
            addChild(bt3) ;
            
            //////////
            
            bt1.addEventListener( MouseEvent.CLICK , click ) ;
            bt2.addEventListener( MouseEvent.CLICK , click ) ;
            bt3.addEventListener( MouseEvent.CLICK , click ) ;
            
            ////////// Test singleton style by default
            
            bt1.setStyle( "topLeftRadius" , 4 ) ; 
            bt2.setStyle( "bottomLeftRadius" , 4 ) ;
            
            // Note: the style is a singleton by default in all default components
        }
        
        public var bt1:BackgroundButton ;
        public var bt2:BackgroundButton ;
        public var bt3:BackgroundButton ;
        
        public function click( e:MouseEvent ):void
        {
            trace( e ) ;
        }
    }
}
