﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.buttons 
{
    import molecule.events.ButtonEvent;

    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#666666")]
    
    public class FrameLabelButtonExample extends Sprite 
    {
        public function FrameLabelButtonExample()
        {
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            
            bt1 = new FrameLabelButton() ;
            bt2 = new FrameLabelButton() ;
            
            bt1.toggle = true ;
            bt1.groupName = "myGroup" ;
            bt1.states = new QuestionButton() ; // see the {project}/lib-swc/question-button.swc library
            bt1.x = 50 ;
            bt1.y = 50 ;
            
            // only if toggle is true and selected is true
            bt1.registerType( ButtonEvent.OVER_SELECTED ) ;
            bt1.registerType( ButtonEvent.OUT_SELECTED ) ;
            
            bt2.toggle = true ;
            bt2.groupName = "myGroup" ;
            bt2.states = new QuestionButton() ; // load from the library
            bt2.x = 120 ;
            bt2.y = 50 ;
            
            addChild( bt1 ) ;
            addChild( bt2 ) ;
            
            bt1.addEventListener( ButtonEvent.CLICK         , debug ) ;
            bt1.addEventListener( ButtonEvent.DESELECT      , debug ) ;
            bt1.addEventListener( ButtonEvent.DISABLED      , debug ) ;
            bt1.addEventListener( ButtonEvent.DOWN          , debug ) ;
            bt1.addEventListener( ButtonEvent.OUT           , debug ) ;
            bt1.addEventListener( ButtonEvent.OUT_SELECTED  , debug ) ;
            bt1.addEventListener( ButtonEvent.OVER          , debug ) ;
            bt1.addEventListener( ButtonEvent.OVER_SELECTED , debug ) ;
            bt1.addEventListener( ButtonEvent.SELECT        , debug ) ;
            bt1.addEventListener( ButtonEvent.UNSELECT      , debug ) ;
            bt1.addEventListener( ButtonEvent.UP            , debug ) ;
            
            stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
        } 
        
        public var bt1:FrameLabelButton ;
        public var bt2:FrameLabelButton ;
        
        public function debug( e:Event ):void
        {
            trace( "toggle:" + bt1.toggle + " selected:" + bt1.selected + " type:" + e.type + " enabled:" + bt1.enabled ) ;
        }
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            switch( code )
            {
                case Keyboard.SPACE :
                {
                    bt1.enabled = !bt1.enabled ;
                    break ;
                }
                default :
                {
                    bt1.toggle = !bt1.toggle ;
                }
            }
        }
    }
}
