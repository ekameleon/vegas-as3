﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.buttons 
{
    import graphics.Align;
    import graphics.drawing.DashRectanglePen;
    import graphics.geom.EdgeMetrics;

    import molecule.render.flash.components.buttons.display.Dutch;
    import molecule.render.flash.components.buttons.display.Girl;
    import molecule.render.flash.components.buttons.display.None;

    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.text.StyleSheet;
    import flash.ui.Keyboard;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#666666")]
    
    public class BasicButtonExample extends Sprite 
    {
        public function BasicButtonExample()
        {
            //////////
            
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            
            stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
            
            //////////
            
            var styleSheet:StyleSheet = new StyleSheet() ;
            
            styleSheet.parseCSS
            (<![CDATA[
            p
            {
                color: #FFFFFF;
                fontFamily: Droid Serif, Arial;
                size: 12px;
            }
            ]]>);
            
            //////////
            
            var style1:BasicButtonStyle = new BasicButtonStyle() ;
            
            style1.align      = Align.TOP_LEFT ;
            style1.iconMargin = new EdgeMetrics(4,4,4,4) ;
            style1.padding    = new EdgeMetrics(4,4,4,4) ;
            style1.styleSheet = styleSheet ;
            
            button1           = new BasicButton() ;
            button1.w         = 260 ;
            button1.h         = 26 ;
            button1.x         = 150 ;
            button1.y         = 50 ;
            button1.style     = style1 ;
            button1.groupName = "button_group" ;
            button1.toggle    = true ;
            
            button1.label = "<p>button 01</p>" ;
            button1.icon  = new None() ;
            
            button1.enabled = false ;
            button1.enabled = true ;
            
            //////////
            
            var style2:BasicButtonStyle = new BasicButtonStyle
            ({
                align               : Align.TOP_LEFT ,
                iconAlign           : Align.RIGHT,
                iconMargin          : new EdgeMetrics(4,4,10,4) ,
                padding             : new EdgeMetrics(4,4,4,4) ,
                themeBorder         : null ,
                themeBorderRollOver : null ,
                themeBorderSelected : null 
            }) ;
            
            style2.styleSheet = styleSheet ;
            
            button2           = new BasicButton() ;
            button2.w         = 260 ;
            button2.h         = 26 ;
            button2.x         = 150 ;
            button2.y         = 86 ;
            button2.style     = style2 ;
            button2.groupName = "button_group" ;
            button2.toggle    = true ;
            
            button2.label = "<p>button 02</p>" ;
            button2.icon  = new Dutch() ;
            
            //////////
            
            var style3:BasicButtonStyle = new BasicButtonStyle
            ({
                iconAutoSize : false ,
                iconMargin   : new EdgeMetrics(4,4,4,4) ,
                padding      : new EdgeMetrics(4,4,4,4) ,
                textBorder   : false ,
                textVerticalAlign : Align.CENTER
            }) ;
            
            style3.styleSheet = styleSheet ;
            
            var pen:DashRectanglePen = new DashRectanglePen() ;
            
            pen.length  = 4 ;
            pen.overage = new EdgeMetrics(4,4,4,4) ;
            pen.spacing = 4 ;
            
            button3           = new BasicButton() ;
            button3.pen       = pen ;
            button3.w         = 260 ;
            button3.h         = 60 ;
            button3.x         = 150 ;
            button3.y         = 122 ;
            button3.style     = style3 ;
            button3.groupName = "button_group" ;
            button3.toggle    = true ;
            
            //button3.enabled = false ;
            
            button3.label = "<p>button 03</p>" ;
            button3.icon  = new Girl() ;
            
            //////////
            
            addChild( button1 ) ;
            addChild( button2 ) ;
            addChild( button3 ) ;
            
            //////////
            
            button1.addEventListener( MouseEvent.CLICK , click ) ;
            button2.addEventListener( MouseEvent.CLICK , click ) ;
            button3.addEventListener( MouseEvent.CLICK , click ) ;
        }
        
        /**
         * The valid alignment of the icon in the area.
         * @private
         */
        protected const alignments:Vector.<uint> = Vector.<uint>
        ([
            Align.TOP, 
            Align.TOP_RIGHT,
            Align.RIGHT, 
            Align.BOTTOM_RIGHT,
            Align.BOTTOM,
            Align.BOTTOM_LEFT , 
            Align.LEFT,
            Align.CENTER ,
            Align.TOP_LEFT 
        ]) ;
        
        /**
         * The valid alignment of the icon in the area.
         * @private
         */
        protected const iconAlignments:Vector.<uint> = Vector.<uint>
        ([
            Align.TOP_LEFT, 
            Align.TOP_RIGHT,
            Align.RIGHT, 
            Align.BOTTOM_RIGHT,
            Align.BOTTOM_LEFT , 
            Align.LEFT
        ]) ;
        
        protected var button1:BasicButton ;
        
        protected var button2:BasicButton ;
        
        protected var button3:BasicButton ;
        
        protected var current:uint ;
        
        protected function click( e:MouseEvent ):void
        {
            trace( e ) ;
        }
        
        
        protected function keyDown( e:KeyboardEvent ):void
        {
            switch( e.keyCode )
            {
                case Keyboard.SPACE :
                {
                    button3.setStyle( "iconAlign" , iconAlignments[current++ % iconAlignments.length] ) ;
                    break ;
                }
                case Keyboard.UP :
                {
                    button3.setStyle( "align" , alignments[current++ % alignments.length] ) ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    button1.icon = button1.icon ? null : new Dutch() ;
                    break ;
                }
                case Keyboard.LEFT :
                {
                    button3.setStyle( "textVerticalAlign" , Align.BOTTOM ) ;
                    break ;
                }
                case Keyboard.RIGHT :
                {
                    button3.setStyle( "iconAutoSize" , !(button3.style as BasicButtonStyle).iconAutoSize ) ;
                    break ;
                }
            }
        }
    }
}
