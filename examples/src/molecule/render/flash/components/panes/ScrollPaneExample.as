﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.flash.components.panes
{
    import graphics.Direction;
    import graphics.FillStyle;
    import graphics.Position;
    import graphics.geom.EdgeMetrics;
    
    import molecule.render.flash.components.bars.ScrollIndicatorStyle;
    import molecule.render.flash.layouts.BoxLayout;
    
    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.text.TextField;
    import flash.text.TextFormat;
    import flash.ui.Keyboard;
    import flash.utils.setTimeout;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#666666")]
    
    public final class ScrollPaneExample extends Sprite 
    {
        public function ScrollPaneExample()
        {
            ////////
            
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown   ) ;
            stage.addEventListener( MouseEvent.MOUSE_DOWN  , mouseDown ) ;
            
            ////////
            
            container = new Sprite() ;
            
            container.x = 5 ;
            container.y = 5 ;
            
            ////////
            
            var style:ScrollPaneStyle = new ScrollPaneStyle() ;
            
            style.padding         = padding ;
            style.position        = Position.STATIC ;
            style.scrollDirection = Direction.VERTICAL ; // HORIZONTAL, VERTICAL, BOTH, NONE
            
            style.vScrollBarStyle = new ScrollIndicatorStyle
            ({
                barFill      : new FillStyle( 0xFFFFFF , 0.3 ) ,
                cornerRadius : 2 ,
                thumbFill    : new FillStyle( 0x333333 ) 
            }) ;
            
            ////////
            
            scrollpane = new ScrollPane() ;
            
            addChild( scrollpane ) ;
            
            scrollpane.lock() ;
            
            scrollpane.style = style ;
            scrollpane.fill = new FillStyle( 0x000000 , 0.3 ) ;
            
            scrollpane.w = 250 ;
            scrollpane.h = 350 ;
            scrollpane.x = 25 ;
            scrollpane.y = 25 ;
            
            scrollpane.unlock() ;
            
            scrollpane.content = container ;
            
            ////////
            
            layout = new BoxLayout( container );
            layout.direction   = Direction.VERTICAL ;
            layout.verticalGap = 2 ;
            
            var i:int ;
            var loop:int = 155 ;
            
            for( i = 0 ; i<loop ; i++ )
            {
                container.addChild( createCell( i ) ) ;
            }
            
            layout.run() ;
            
            ////////
            
            scrollpane.update() ;
            
            ////////
        }
        
        private var container:Sprite ;
        
        private var layout:BoxLayout ;
        
        private var padding:EdgeMetrics = new EdgeMetrics(6,6,6,6) ;
        
        private var scrollpane:ScrollPane ;
        
        private function createCell( i:int = 0 ):Sprite
        {
            var w:Number    = scrollpane.w - padding.horizontal ;
            var cell:Sprite = new Sprite() ;
            
            cell.mouseChildren = false ;
            
            cell.addEventListener( MouseEvent.CLICK , debug ) ;
            
            cell.buttonMode = true ;
            cell.useHandCursor = true ;
            cell.mouseEnabled = true ;
            
            cell.graphics.beginFill( 0x666666 , 0.6 ) ;
            cell.graphics.lineStyle( 2 , 0xFFFFFF , 0.6 ) ;
            cell.graphics.drawRect(0,0,w+100,22) ;
            
            var field:TextField = new TextField() ;
            
            field.defaultTextFormat = new TextFormat( "Verdana" , 12 , 0xFFFFFF ) ;
            field.x                 = 1 ;
            field.y                 = 2 ;
            field.width             = cell.width - 2;
            field.height            = cell.height - 4 ;
            field.text              = "cell " + (i+1) ;
            
            cell.addChild( field ) ;
            
            return cell ;
        }
        
        //////////////
        
        private function debug( e:MouseEvent ):void
        {
            trace( "#debug type:" + e.type + " target:" + e.currentTarget + " target:" + e.target + " touching:" + scrollpane.touching ) ;
        }
        
        //////////////
        
        private function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            switch( code )
            {
                case Keyboard.UP :
                {
                    scrollpane.scrollV -= 60 ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    scrollpane.scrollV += 60 ;
                    break ;
                }
                case Keyboard.LEFT :
                {
                    scrollpane.scrollV = 0 ;
                    break ;
                }
                case Keyboard.RIGHT :
                {
                    scrollpane.scrollV = scrollpane.maxScrollV;
                    break ;
                }
                case Keyboard.SPACE :
                {
                    var i:int = container.numChildren ;
                    var loop:int = i+50 ;
                    for( i ; i<loop ; i++ )
                    {
                        container.addChild( createCell( i ) ) ;
                    }
                    
                    layout.run() ;
                    
                    setTimeout( scrollpane.update , 150 ) ;
                    break ;
                }
            }
        }
        
        //////////////
        
        private var count:int ;
        
        private const counts:Vector.<int> = Vector.<int>([0,150,50,10,100]) ;
        
        private function mouseDown( e:MouseEvent ):void
        {
            if( e.shiftKey )
            {
                var loop:int = counts[count++%counts.length] ;
                
                var size:int = container.numChildren ;
                while(--size > -1)
                {
                    container.removeChildAt( size ) ; 
                }
                
                for( var i:int = 0 ; i<loop ; i++ )
                {
                    container.addChild( createCell( i ) ) ;
                }
                
                layout.run() ;
                
                setTimeout( scrollpane.update , 150 ) ;
            }
        }
    }
}
