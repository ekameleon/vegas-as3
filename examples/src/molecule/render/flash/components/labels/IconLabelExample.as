﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2011
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels 
{
    import graphics.Align;
    import graphics.FillStyle;
    import graphics.geom.EdgeMetrics;

    import molecule.render.flash.components.labels.display.Dutch;
    import molecule.render.flash.components.labels.display.None;

    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.net.URLRequest;
    import flash.ui.Keyboard;
    
    [SWF(width="740", height="480", frameRate="24", backgroundColor="#666666")]
    
    public class IconLabelExample extends Sprite 
    {
        public function IconLabelExample()
        {
            ///////
            
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
            
            ///////
            
            var style:IconLabelStyle = new IconLabelStyle() ;
            
            style.color   = 0xFFFFFF ;
            style.padding = new EdgeMetrics(2,0,2,0) ;
            
            ///////
            
            element       = new IconLabel() ;
            element.icon  = new Dutch() ;
            element.label = "Hello world" ;
            element.fill  = new FillStyle( 0x000000, 0.4 ) ;
            element.style = style ;
            element.x     = 370 ;
            element.y     = 240 ;
            
            addChild( element ) ;
        }
        
        public var element:IconLabel ;
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:Number = e.keyCode ;
            switch(code)
            {
                case Keyboard.UP :
                {
                    element.icon = new URLRequest("library/de.png") ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    element.icon = new None() ;
                    break ;
                }
                case Keyboard.SPACE :
                {
                    element.icon = new Dutch() ;
                    break ;
                }
                case Keyboard.LEFT :
                {
                    element.align = Align.RIGHT ;
                    break ;
                }
                case Keyboard.RIGHT :
                {
                    element.align = Align.LEFT ;
                    break ;
                }
            }
        }
    }
}