﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2011
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels 
{
    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;

    
    [SWF(width="740", height="480", frameRate="24", backgroundColor="#666666")]
    
    public class PaginationLabelExample extends Sprite 
    {
        public function PaginationLabelExample()
        {
            ///////
            
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
            
            ///////
            
            element = new PaginationLabel() ;
            
            element.selected.connect( select ) ;
            
            element.set( 15, 30 , 4 ) ; // current,limit,total
            
            element.firstLabel    = "Premier" ;
            element.lastLabel     = "Dernier" ;
            element.nextLabel     = "Suivant" ;
            element.previousLabel = "Précédent" ;
            
            element.w = 480  ;
            element.h = 40  ;
            element.x = 25  ;
            element.y = 25  ;
            
            addChild( element ) ;
        }
        
        public var element:PaginationLabel ;
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:Number = e.keyCode ;
            switch(code)
            {
                case Keyboard.UP :
                {
                    element.set( 1, 30, 5 ) ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    element.set( 30, 30, 5 ) ;
                    break ;
                }
            }
        }
        
        public function select( component:PaginationLabel ):void 
        {
            trace("select : " + component.current ) ;
        }
    }
}