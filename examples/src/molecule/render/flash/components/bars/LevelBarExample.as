﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.bars 
{
    import molecule.events.ButtonEvent;

    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.ui.Keyboard;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#222222")]
    
    public class LevelBarExample extends Sprite 
    {
        public function LevelBarExample()
        {
            /////////////
            
            bar = new LevelBar(250,10) ;
            
            bar.addEventListener( ButtonEvent.PRESS           , debug ) ;
            bar.addEventListener( ButtonEvent.RELEASE         , debug ) ;
            bar.addEventListener( ButtonEvent.RELEASE_OUTSIDE , debug ) ;
            
            bar.addEventListener( MouseEvent.MOUSE_MOVE , move ) ;
            
            bar.changed.connect( changed ) ;
            bar.leveled.connect( leveled  ) ;
            
            bar.minimumLevel = 0    ;
            bar.maximumLevel = 2000 ; // example the duration of the media 2000 ms 
            
            bar.x        = 50 ;
            bar.y        = 50 ;
            bar.position = 50 ; // % to loading the media
            
            addChild(bar) ;
            
            /////////////
            
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
        }
        
        public var bar:LevelBar ;
        
        protected function changed( bar:LevelBar ):void
        {
            trace( "changed position:" + bar.position + " level:" + bar.level ) ;
        }
        
        protected function leveled( bar:LevelBar ):void
        {
            trace( "leveled position:" + bar.position + " level:" + bar.level ) ;
        }
        
        protected function debug( e:ButtonEvent ):void
        {
            trace( e.type ) ;
        }
        
        protected function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            switch( code )
            {
                case Keyboard.LEFT :
                {
                    bar.position -= 10 ;
                    break ;
                }
                case Keyboard.RIGHT :
                {
                    bar.position += 10 ;
                    break ;
                }
                case Keyboard.UP :
                {
                    bar.level -= 100 ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    bar.level += 100 ;
                    break ;
                }
                case Keyboard.SPACE :
                {
                    bar.position = 0 ;
                    break ;
                }
            }
        }
        
        protected function move( e:MouseEvent ):void
        {
            trace( e.type + " current level : " + bar.getCurrentLevel() ) ;
        }
    }
}
