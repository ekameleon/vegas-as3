﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.bars 
{
    import graphics.Align;
    import graphics.Direction;
    import graphics.FillStyle;
    import graphics.LineStyle;
    import graphics.geom.EdgeMetrics;
    
    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.filters.DropShadowFilter;
    import flash.ui.Keyboard;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#999999")]
    
    public class SimpleProgressbarExample extends Sprite 
    {
        public function SimpleProgressbarExample()
        {
            /////////////
            
            bar = new SimpleProgressbar() ;
            
            bar.changed.connect( change ) ;
            
            bar.direction = "horizontal" ;
            bar.w = 150 ;
            bar.h = 6 ;
            bar.x = 50 ;
            bar.y = 50 ;
            bar.position = 50 ;
            
            addChild(bar) ;
            
            var shadow:DropShadowFilter = new DropShadowFilter( 1, 45, 0x000000, 0.6, 6, 6, 1, 6) ;
            bar.filters = [ shadow  ] ;
            
            /////////////
            
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
            
            trace("Press Keyboard.LEFT or Keyboard.RIGHT or Keyboard.SPACE  or other keyboard touch to test this example, or Keyboard.INSERT to reset the example.") ;
        }
        public var bar:SimpleProgressbar ;
        
        public function change( bar:SimpleProgressbar ):void
        {
            trace( "change position : " + bar.position ) ;
        }
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            
            switch( code )
            {
                case Keyboard.LEFT :
                {
                    bar.position -= 10 ;
                    break ;
                }
                case Keyboard.RIGHT :
                {
                    bar.position += 10 ;
                    break ;
                }
                case Keyboard.UP :
                {
                    bar.minimum = 20  ;
                    bar.maximum = 200 ;
                    break ;
                }
                case Keyboard.SPACE :
                {
                    bar.lock() ;
                    bar.align               = Align.CENTER ; // Align.LEFT or Align.RIGHT or Align.CENTER
                    bar.direction           = Direction.HORIZONTAL ;
                    bar.backgroundFillStyle = new FillStyle( 0xB5C7CA , 1 ) ;
                    bar.backgroundLineStyle = new LineStyle( 2 , 0xFFFFFF , 1 ) ;
                    bar.barFillStyle        = new FillStyle( 0x921085 , 1 ) ;
                    bar.barLineStyle        = new LineStyle( 1 , 0x6A9195, 1 ) ;
                    bar.border              = new EdgeMetrics(2, 2, 2, 2) ;
                    bar.unlock() ;
                    bar.setSize( 200, 8 ) ;
                    break ;
                }
                case Keyboard.INSERT : 
                {
                    bar.lock() ;
                    bar.align               = Align.LEFT ; // Align.LEFT or Align.RIGHT or Align.CENTER
                    bar.direction           = Direction.HORIZONTAL ;
                    bar.backgroundFillStyle = new FillStyle( 0xFFFFFF , 1 ) ;
                    bar.backgroundLineStyle = new LineStyle( 1 , 0xA2A2A2 , 1 ) ;
                    bar.barFillStyle        = new FillStyle( 0xFF0000 , 1 ) ;
                    bar.barLineStyle        = null ;
                    bar.border              = new EdgeMetrics() ;
                    bar.unlock() ;
                    bar.setSize( 150, 6 ) ;
                    break ;
                }
                default :
                {
                    bar.direction = ( bar.direction == Direction.VERTICAL ) ? Direction.HORIZONTAL : Direction.VERTICAL ;
                    if ( bar.direction == Direction.VERTICAL )
                    {
                        bar.setSize( 8, 500 ) ;
                    }
                    else
                    {
                        bar.setSize( 500, 8 ) ;
                    }
                }
            }
        }
    }
}
