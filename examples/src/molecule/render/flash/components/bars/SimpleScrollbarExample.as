﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.bars  
{
    import graphics.Direction;
    import graphics.FillStyle;
    import graphics.LineStyle;
    import graphics.geom.EdgeMetrics;

    import molecule.render.flash.components.buttons.FrameLabelButton;

    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.filters.DropShadowFilter;
    import flash.ui.Keyboard;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#222222")]
    
    public class SimpleScrollbarExample extends Sprite 
    {
        public function SimpleScrollbarExample()
        {
            ////////////
            
            bar = new SimpleScrollbar() ;
            
            bar.direction = Direction.VERTICAL  ;
            bar.w = 15 ;
            bar.h = 250 ;
            
            // optionals arrow buttons from the library.
            
            bar.bottomButton = new FrameLabelButton( new ScrollbarBottom() ) ;
            bar.leftButton   = new FrameLabelButton( new ScrollbarLeft() ) ;
            bar.rightButton  = new FrameLabelButton( new ScrollbarRight() ) ;
            bar.topButton    = new FrameLabelButton( new ScrollbarTop() ) ;
            
            // behaviours of the scrollbar
            
            // bar.enabled = false ;
            
            bar.lineScrollSize =  10 ;
            bar.pageSize       =  20 ;
            bar.position       =  50 ;
            bar.thumbSize      =  60 ;
            bar.x              =  50 ;
            bar.y              =  50 ;
            
            // initialize style of the scrollbar
            
            bar.barFillStyle    = new FillStyle( 0x921085 , 0.2 ) ;
            bar.barLineStyle    = new LineStyle( 1.5 , 0x6A9195 ) ;
            bar.thumbFillStyle  = new FillStyle( 0xB5C7CA ) ;
            bar.thumbLineStyle  = new LineStyle( 1.5 , 0xFFFFFF ) ;
            
            bar.padding = new EdgeMetrics(3,3,3,3) ;
            
            bar.filters = [ new DropShadowFilter( 1, 45, 0x000000, 0.6, 6, 6, 1, 6) ] ;
            
            bar.setStyle
            ( { 
                barFilters   : [ new DropShadowFilter( 1, 45, 0x000000, 0.6, 6, 6, 1, 6)  ] , 
                thumbFilters : [ new DropShadowFilter( 1, 45, 0x000000, 0.6, 6, 6, 1, 6)  ] 
            } ) ;
            
            bar.changed.connect( change ) ;
            
            addChild( bar ) ;
            
            ////////////
            
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            
            stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
            
            ////////////
        }
        
        public var bar:SimpleScrollbar ;
        
        public function change( bar:SimpleScrollbar ):void
        {
            trace( "change position : " + bar.position ) ;
        }
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            switch( code )
            {
                case Keyboard.LEFT :
                {
                    bar.position -= 10 ;
                    break ;
                }
                case Keyboard.RIGHT :
                {
                    bar.position += 10 ;
                    break ;
                }
                case Keyboard.UP :
                {
                    bar.position = bar.minimum ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    bar.position = bar.maximum ;
                    break ;
                }
                case Keyboard.SPACE :
                {
                    bar.thumbSize    = 80 ;
                    bar.cornerRadius =  8 ;
                    bar.direction = ( bar.direction == Direction.VERTICAL ) ? Direction.HORIZONTAL : Direction.VERTICAL ;
                    
                    bar.leftButton  = new FrameLabelButton( new RoundLeftButton() ) ;
                    bar.rightButton = new FrameLabelButton( new RoundRightButton() ) ;
                    bar.padding     = new EdgeMetrics(-6,3,-6,3) ;
                    
                    if ( bar.direction == Direction.VERTICAL )
                    {
                        bar.w = 15  ;
                        bar.h = 200 ;
                    }
                    else
                    {
                        bar.setSize( 350, 15 ) ;
                    }
                    break ;
                }
                default :
                {
                    bar.minimum = 20 ; // change the minimum value of the bar
                    bar.maximum = 80 ; // change the maximum value of the bar
                    bar.invert  = !bar.invert ; // invert the positive direction of the bar scroll.
                }
            }
        }
    }
}
