﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.bars 
{
    import graphics.Direction;
    import graphics.FillStyle;
    import graphics.LineStyle;
    import graphics.geom.EdgeMetrics;

    import flash.display.Sprite;
    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#222222")]
    
    public class ScrollIndicatorExample extends Sprite 
    {
        public function ScrollIndicatorExample()
        {
            ////////////
            
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            
            stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
            
            ////////////
            
            bar = new ScrollIndicator() ;
            
            bar.lock() ;
            
            bar.changed.connect( change ) ;
            
            addChild( bar ) ;
            
            bar.direction = Direction.VERTICAL ;
            bar.border    = new EdgeMetrics(2,2,2,2);
            bar.position  =  50 ;
            bar.thumbSize =  60 ;
            bar.w         =   8 ;
            bar.h         = 220 ; 
            bar.x         = 150 ;
            bar.y         =  50 ;
            
            bar.style = style ;
            
            bar.unlock() ;
            
            bar.update();
        }
        
        protected var bar:ScrollIndicator ;
        
        protected const style:ScrollIndicatorStyle = new ScrollIndicatorStyle
        ({
            barFill   : new FillStyle( 0x921085 ) ,
            thumbFill : new FillStyle( 0x000000 ) 
        }) ;
        
        public function change( bar:ScrollIndicator ):void
        {
            trace( "change position : " + bar.position ) ;
        }
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            switch( code )
            {
                case Keyboard.LEFT :
                {
                    bar.position -= 10 ;
                    break ;
                }
                case Keyboard.RIGHT :
                {
                    bar.position += 10 ;
                    break ;
                }
                case Keyboard.UP :
                {
                    bar.position = bar.minimum ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    bar.position = bar.maximum ;
                    break ;
                }
                case Keyboard.SPACE :
                {
                    bar.lock() ;
                    bar.direction = ( bar.direction == Direction.VERTICAL ) ? Direction.HORIZONTAL : Direction.VERTICAL ;
                    bar.style = new ScrollIndicatorStyle
                    ({
                        barLine      : new LineStyle( 2 , 0xCCCCCC ) ,
                        cornerRadius : 4
                    }) ;
                    if ( bar.direction == Direction.VERTICAL )
                    {
                        bar.setSize( 10 , 200 ) ;
                    }
                    else
                    {
                        bar.setSize( 350, 10 ) ;
                    }
                    bar.unlock() ;
                    bar.update() ;
                    break ;
                }
                default :
                {
                    bar.minimum = 20 ; // change the minimum value of the bar
                    bar.maximum = 80 ; // change the maximum value of the bar
                    bar.invert  = !bar.invert ; // invert the positive direction of the bar scroll.
                }
            }
        }
    }
}
