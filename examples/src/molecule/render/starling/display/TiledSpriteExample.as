﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.display 
{
    import graphics.easings.backOut;
    import graphics.transitions.TweenTo;

    import molecule.logger;
    import molecule.render.starling.display.views.TiledView;
    import molecule.render.starling.display.views.pictures.Tile;

    import starling.core.Starling;
    import starling.events.Event;
    import starling.events.ResizeEvent;
    import starling.textures.Texture;

    import system.logging.LoggerLevel;
    import system.logging.targets.SOSTarget;

    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.events.UncaughtErrorEvent;
    import flash.geom.Rectangle;
    
    [SWF(width="2048", height="1536", frameRate="24", backgroundColor="#666666")]
    
    public class TiledSpriteExample extends Sprite 
    {
        public function TiledSpriteExample()
        {
            /////////
            
            var sos:SOSTarget = new SOSTarget( "TiledSprite example" , 0xA2A2A2 , true  ) ;
            
            sos.filters       = ["*"] ;
            sos.level         = LoggerLevel.ALL ;
            sos.includeLines  = true  ;
            sos.includeTime   = true  ;
            
            loaderInfo.uncaughtErrorEvents.addEventListener( UncaughtErrorEvent.UNCAUGHT_ERROR, uncaughtError );
            
            addEventListener( Event.ADDED_TO_STAGE , addedToStage ) ;
        }
        
        protected var sprite:TiledSprite ;
        
        protected var tween:TweenTo ;
        
        /**
         * Invoked when the sprite is added to the stage.
         */
        protected function addedToStage( e:flash.events.Event = null ):void
        {
            starling = new Starling( TiledView , stage , new Rectangle(0,0,stage.fullScreenWidth, stage.fullScreenHeight) ) ;
            
            starling.antiAliasing = 1;
            starling.enableErrorChecking = false;
            
            starling.addEventListener( starling.events.Event.ROOT_CREATED , created ) ;
            
            starling.start();
            
            logger.debug( this + " addedToStage fullScreenSourceRect:" + stage.fullScreenSourceRect ) ;
            
            stage.addEventListener( flash.events.MouseEvent.MOUSE_DOWN , mouseDown ) ;
            stage.addEventListener( flash.events.Event.RESIZE , resize ) ;
        }
        
        protected function created( e:starling.events.Event ):void
        {
            logger.debug( this + " " + e.type + " root:" + starling.root + " stage:" + starling.stage ) ;
            
            updateViewport(stage.fullScreenWidth, stage.fullScreenHeight );
            
            starling.stage.addEventListener( ResizeEvent.RESIZE , resizeStage ) ;
            
            var view:TiledView = starling.root as TiledView ;
            
            if( view )
            {
                var sprite:TiledSprite = new TiledSprite( Texture.fromBitmap( new Tile() ) ) ;
                
                sprite.x = 0 ;
                sprite.y = 0 ;
                
                sprite.width  = stage.stageWidth ;
                sprite.height = stage.stageHeight  ;
                
                //sprite.smoothing = TextureSmoothing.TRILINEAR ;
                
                view.addChild( sprite ) ;
                
                tween = new TweenTo( sprite , null , backOut, 12 ) ;
            }
        }
        
        protected function mouseDown( e:MouseEvent ):void
        {
            if( tween )
            {
                tween.stop() ;
                tween.to = 
                { 
                    width  : 100 , 
                    height : 100 
                } ;
                tween.run() ;
            }
        }
        
        protected function resize( e:flash.events.Event ):void
        {
            logger.debug( this + " resize " + stage.stageWidth + " : " + stage.stageHeight ) ;
            updateViewport( stage.fullScreenWidth, stage.fullScreenHeight );
            if( sprite )
            {
                sprite.width  = stage.stageWidth ; 
                sprite.height = stage.stageHeight ;
            }
        }
        
        
        protected function resizeStage( e:ResizeEvent ):void
        {
            logger.debug( this + " resizeStage " + e.width + "x" + e.height ) ;
            Starling.current.stage.stageWidth = e.width   ;
            Starling.current.stage.stageHeight = e.height ;
            Starling.current.viewPort = new Rectangle( 0, 0, e.width , e.height ) ;
        }
        
        protected function updateViewport( width:Number, height:Number ):void
        {
            if ( !Starling.current && !Starling.current.stage ) 
            {
                return;
            }
            Starling.current.stage.stageWidth = width   ;
            Starling.current.stage.stageHeight = height ;
            Starling.current.viewPort = new Rectangle( 0, 0, width , height ) ;
        }
        
        /**
         * Invoked when an error is thrown outside of any try..catch blocks or when an ErrorEvent object is dispatched with no registered listeners. 
         * The uncaught error event functionality is often described as a "global error handler."
         */
        protected function uncaughtError( e:UncaughtErrorEvent ):void
        {
            logger.error( "## " + this + " uncaughtError " + e.error ) ;
        }
    }
}
