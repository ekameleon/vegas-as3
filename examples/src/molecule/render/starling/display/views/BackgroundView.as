﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [VEGAS framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.display.views
{
    import graphics.Direction;
    import graphics.FillGradientStyle;
    import graphics.FillStyle;
    import graphics.LineStyle;

    import molecule.render.starling.display.Background;

    import starling.display.Sprite;

    import flash.display.GradientType;
    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;

    /**
     * The main view of the application.
     */
    public class BackgroundView extends Sprite
    {
        public function BackgroundView()
        {
            // clipRect = new Rectangle( 20 , 20 , 400 , 400 ) ;

            background = new Background() ;

            background.lock() ; // lock the update method

            background.fill = new FillStyle( 0xD97BD0  ) ;
            background.line = new LineStyle( 2, 0xFFFFFF ) ;

            background.w  = 1024 ;
            background.h  = 760 ;

            background.unlock() ; // unlock the update method

            background.update() ; // force update

            addChild( background ) ;


            var background2:Background = new Background() ;

            background2.lock() ; // lock the update method

            background2.fill = new FillStyle( 0x000000  ) ;
            background2.line = new LineStyle( 2, 0xFF0000 ) ;

            background2.w = 100 ;
            background2.h  = 100 ;
            background2.x = 25 ;
            background2.y = 25 ;


            background2.unlock() ; // unlock the update method

            background2.update() ; // force update

            background.addChild( background2 ) ;
        }

        protected var background:Background ;

        public function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            background.lock() ;
            switch( code )
            {
                case Keyboard.SPACE :
                {
                    if( background.fullscreen )
                    {
                        background.autoSize   = false ;
                        background.fill       = new FillStyle( 0xD97BD0 ) ;
                        background.fullscreen = false ;
                    }
                    else
                    {
                        background.autoSize         = true ;
                        background.gradientRotation = 90 ;
                        background.useGradientBox   = true ;
                        background.fill             = new FillGradientStyle( GradientType.LINEAR, [0x071E2C,0x81C2ED], [1,1], [0,255] ) ;
                        background.fullscreen       = true ;
                        background.direction        = null ;
                    }
                    break ;
                }
                case Keyboard.UP :
                {
                    background.autoSize   = true ;
                    background.fill       = new FillStyle( 0x000000 ) ;
                    background.fullscreen = true ;
                    background.direction  = Direction.HORIZONTAL ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    background.autoSize   = true ;
                    background.fill       = new FillStyle( 0xFFFFFF ) ;
                    background.fullscreen = true ;
                    background.direction  = Direction.VERTICAL ;
                    break ;
                }
            }
            background.unlock() ;
            background.update() ;
        }
    }
}
