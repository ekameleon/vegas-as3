﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.display.views
{
    import graphics.Direction;
    import graphics.easings.backOut;
    import graphics.geom.EdgeMetrics;
    import graphics.transitions.TweenTo;
    
    import molecule.logger;
    import molecule.render.starling.display.Scale3Sprite;
    import molecule.render.starling.display.views.pictures.Square;
    
    import starling.display.Sprite;
    import starling.textures.Texture;
    
    import system.process.Action;
    
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    
    /**
     * The main view of the application.
     */
    public class Scale3View extends Sprite
    {
        public function Scale3View()
        {
            var square:Texture = Texture.fromBitmap( new Square() ) ;
            
            sprite = new Scale3Sprite( square , new EdgeMetrics(12,12,20,20) , Direction.HORIZONTAL ) ;
            
            logger.info( this + " sprite width:" + sprite.width + " height:" + sprite.height) ; // 60x60
            
            sprite.x = 25 ;
            sprite.y = 25 ;
            
            addChild( sprite ) ;
            
            tween = new TweenTo( sprite , null , backOut, 0.5 , true ) ;
            
            tween.finishIt.connect( finish ) ;
        }
        
        protected var sprite:Scale3Sprite ;
        
        protected var tween:TweenTo ;
        
        public function keyDown( e:KeyboardEvent ):void
        {
            if( tween )
            {
                tween.stop() ;
            }
            
            if( sprite )
            {
                sprite.direction = sprite.direction == Direction.HORIZONTAL ? Direction.VERTICAL : Direction.HORIZONTAL ;
            }
        }
        
        public function mouseDown( e:MouseEvent ):void
        {
            if( tween )
            {
                tween.stop() ;
            }
            
            if( sprite.direction == Direction.HORIZONTAL )
            {
                tween.to = { width : uint(Math.random() * 140 + 60) } ;
            }
            else
            {
                tween.to = { height : uint(Math.random() * 140 + 60) } ;
            }
            
            tween.run() ;
        }
        
        public function finish( action:Action ):void
        {
            logger.info( this + " sprite width:" + sprite.width + " height:" + sprite.height) ; 
        }
    }
}

/////////////////
//
//var textureH:HScale3Texture = new HScale3Texture( Texture.fromBitmap( new Square() ) , new EdgeMetrics(12,0,20,0) ) ;
//var textureV:VScale3Texture = new VScale3Texture( Texture.fromBitmap( new Square() ) , new EdgeMetrics(0,12,0,20) ) ;
//
//var image1:Image = new Image( textureH.left ) ;
//var image2:Image = new Image( textureH.center ) ;
//var image3:Image = new Image( textureH.right ) ;
//
//image1.x = 25 ;
//image1.y = 25 ;
//
//image2.x = image1.x + image1.width ;
//image2.y = image1.y ;
//
//image3.x = image2.x + image2.width ;
//image3.y = image2.y ;
//
//addChild( image1 ) ;
//addChild( image2 ) ;
//addChild( image3 ) ;
//
//var image4:Image = new Image( textureV.top ) ;
//var image5:Image = new Image( textureV.middle ) ;
//var image6:Image = new Image( textureV.bottom ) ;
//
//image4.x = 25 ;
//image4.y = 100 ;
//
//image5.x = image4.x ;
//image5.y = image4.y + image4.height ; 
//
//image6.x = image5.x ;
//image6.y = image5.y + image5.height  ;
//
//addChild( image4 ) ;
//addChild( image5 ) ;
//addChild( image6 ) ;
//
/////////////////
