﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2011
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling.layouts.display
{
    import graphics.Align;
    import graphics.Direction;
    import graphics.DirectionOrder;
    import graphics.easings.backOut;
    import graphics.geom.EdgeMetrics;

    import molecule.Layout;
    import molecule.LayoutBufferMode;
    import molecule.logger;
    import molecule.render.starling.layouts.ScrollBoxLayout;

    import starling.display.Quad;
    import starling.display.Sprite;

    import system.data.Iterator;
    import system.data.iterators.ArrayIterator;

    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.geom.Rectangle;
    import flash.ui.Keyboard;
    
    /**
     * The main screen of the application.
     */
    public class BodyScrollBoxLayout extends Sprite
    {
        public function BodyScrollBoxLayout()
        {
            ///////////
            
            canvas = new Sprite() ;
            
            canvas.x = 980/2 ;
            canvas.y = 760/2 ;
            
            addChild( canvas) ;
            
            background = new Quad(10,10,0x999999) ;
            
            canvas.addChild( background) ;
            
            container   = new Sprite() ;
            
            canvas.addChild( container ) ;
            
            ///////////
            
            var colors:Array = [ 0xFF0000 , 0xDD0000 , 0xBB0000 , 0x990000 , 0x770000 , 0x550000 ] ;
            
            var quad:Quad ;
            for ( var i:uint = 0 ; i<16 ; i++ ) 
            {
                quad = new Quad( 60 , 40 , colors[i%colors.length] ) ;
                
                container.addChild( quad ) ;
            }
            
            ///////////
            
            layout = new ScrollBoxLayout() ;
            
            logger.debug( this + " #1 layout.numChildren:" + layout.numChildren + " maxscroll" + layout.maxscroll ) ;
            
            layout.updater.connect( update ) ;
            
            layout.lock() ;
            
            layout.easing        = backOut ;
            layout.easingMode    = true  ;
            layout.duration      = 12 ;
            layout.childCount    = 6 ;
            layout.bufferMode    = LayoutBufferMode.NORMAL ;
            layout.direction     = Direction.HORIZONTAL ;
            layout.padding       = new EdgeMetrics( 10 , 10 , 10 , 10 ) ;
            layout.horizontalGap = 10 ;
            layout.verticalGap   = 10 ;
            
            layout.align = Align.CENTER ;
            
            layout.unlock() ;
            
            layout.initialize( container ) ;
            
            layout.run() ;
            
            logger.info ( this + " maxscroll::" + layout.maxscroll + " scroll:" + layout.scroll + " bottomScroll:" + layout.bottomScroll ) ;
        }
        
        public var background:Quad ;
        
        public var canvas:Sprite ;
        
        public var container:Sprite ;
        
        public var layout:ScrollBoxLayout ;
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            switch( code )
            {
                case Keyboard.SPACE :
                {
                    // logger.debug( this + " change the direction" ) ;
                    layout.direction = layout.direction == Direction.HORIZONTAL ? Direction.VERTICAL : Direction.HORIZONTAL ;
                    break ;
                }
                case Keyboard.UP :
                {
                    // logger.debug( this + " scroll--" ) ;
                    layout.scroll-- ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    // logger.debug( this + " scroll++" ) ;
                    layout.scroll++ ;
                    break ;
                }
                case Keyboard.LEFT :
                {
                    // logger.debug( this + " change the order" ) ;
                    layout.order = layout.order == DirectionOrder.NORMAL ? DirectionOrder.REVERSE : DirectionOrder.NORMAL ;
                    break ;
                }
            }
            layout.run() ;
        }
        
        public function mouseDown( e:MouseEvent ):void
        {
            if ( alignIterator.hasNext() )
            {
                layout.align = alignIterator.next() ;
                layout.run() ;
            }
            if ( !alignIterator.hasNext() )
            {
                alignIterator.reset() ;
            }
        }
        
        protected const aligns:Array = 
        [ 
            Align.TOP_LEFT    , Align.TOP          , Align.TOP_RIGHT ,
            Align.RIGHT       , Align.BOTTOM_RIGHT , Align.BOTTOM    ,
            Align.BOTTOM_LEFT , Align.LEFT          
        ] ;
        
        protected const alignIterator:Iterator = new ArrayIterator( aligns ) ;
        
        /**
         * Invoked when the layout is updated.
         */
        protected function update( layout:Layout ):void
        {
            var bounds:Rectangle = layout.bounds ;
            
            background.x      = bounds.x ;
            background.y      = bounds.y ; 
            background.width  = bounds.width ;
            background.height = bounds.height ;
            
            canvas.clipRect   = bounds ;
        }
    }
}
