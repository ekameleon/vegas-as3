﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2011
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling.layouts 
{
    import molecule.logger;
    import molecule.render.flash.display.MOB;
    import molecule.render.starling.layouts.display.BodyScrollBoxLayout;

    import starling.core.Starling;
    import starling.events.Event;

    import system.logging.LoggerLevel;
    import system.logging.targets.SOSTarget;

    import flash.display.StageScaleMode;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#666666")]
    
    public class ScrollBoxLayoutExample extends MOB 
    {
        public function ScrollBoxLayoutExample()
        {
            /////////
            
            var sos:SOSTarget = new SOSTarget( "hello" , 0xA2A2A2 , true  ) ;
            
            sos.filters       = ["*"] ;
            sos.level         = LoggerLevel.ALL ;
            sos.includeLines  = true  ;
            sos.includeTime   = true  ;
            
            logger.info( "## " + this + " start" ) ;
            
            /////////
            
            stage.align     = "" ;
            stage.scaleMode = StageScaleMode.NO_SCALE ;
            
            starling = new Starling( BodyScrollBoxLayout , stage ) ;
            starling.antiAliasing = 1;
            
            starling.addEventListener( Event.ROOT_CREATED , complete ) ;
            
            starling.start();
        }
        
        protected function complete( e:Event ):void
        {
            logger.debug( this + " " + e.type + " " + starling.root ) ;
            stage.addEventListener( KeyboardEvent.KEY_DOWN , (starling.root as BodyScrollBoxLayout).keyDown   ) ;
            stage.addEventListener( MouseEvent.MOUSE_DOWN  , (starling.root as BodyScrollBoxLayout).mouseDown ) ;
        }
    }
}
