﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.components.panes.views
{
    import graphics.Direction;
    import graphics.Position;
    import graphics.geom.EdgeMetrics;

    import molecule.render.starling.components.bars.ScrollIndicatorStyle;
    import molecule.render.starling.components.panes.ScrollPane;
    import molecule.render.starling.components.panes.ScrollPaneStyle;
    import molecule.render.starling.layouts.BoxLayout;

    import starling.display.Quad;
    import starling.display.Sprite;
    import starling.text.TextField;

    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;
    
    public class ScrollPaneView extends Sprite 
    {
        public function ScrollPaneView()
        {
            var background:Quad = new Quad(250,350,0xDDDDDD) ;
            
            background.x = 25 ;
            background.y = 25 ;
            
            var style:ScrollPaneStyle = new ScrollPaneStyle() ;
            
            style.padding          = padding ;
            style.position         = Position.STATIC ;
            style.scrollDirection  = Direction.VERTICAL ; // HORIZONTAL, VERTICAL, BOTH, NONE
            style.vScrollBarStyle  = new ScrollIndicatorStyle( { barColor:0x000000 , thumbColor:0x0099CC }) ;
            
            ////////
            
            scrollpane = new ScrollPane() ;
            
            scrollpane.enabled = false ;
            
            scrollpane.lock() ;
            
            scrollpane.style = style ;
            
            scrollpane.w = 250 ;
            scrollpane.h = 350 ;
            
            scrollpane.x = 25 ;
            scrollpane.y = 25 ;
            
            scrollpane.unlock() ;
            
            scrollpane.update() ;
            
            addChild( background ) ;
            addChild(scrollpane);
        }
        
        private var container:Sprite ;
        
        private var layout:BoxLayout ;
        
        private var padding:EdgeMetrics = new EdgeMetrics(6,6,6,6) ;
        
        private var scrollpane:ScrollPane ;
        
        private function createCell( i:int = 0 ):Sprite
        {
            var w:Number = scrollpane.w - padding.horizontal ;
            
            var background:Quad = new Quad( w, 22 , 0x006666 ) ;
            
            var field:TextField = new TextField( background.width - 2, background.height - 4 , "cell " + (i+1) ) ;
            
            field.fontName = "Verdana" ; 
            field.fontSize = 12 ;
            field.color    = 0xFFFFFF ;
            field.hAlign   = "left" ;
            field.x        = 1 ;
            field.y        = 2 ;
            
            var cell:Sprite = new Sprite() ;
            cell.addChild( background ) ;
            cell.addChild( field ) ;
            
            return cell ;
        }
        
        public function initialize():void
        {
            container = new Sprite() ;
            
            container.x = 5 ;
            container.y = 5 ;
            
            ////////
            
            layout = new BoxLayout( container );
            layout.direction   = Direction.VERTICAL ;
            layout.verticalGap = 2 ;
            
            var i:int ;
            var loop:int = 155 ;
            
            for( i = 0 ; i<loop ; i++ )
            {
                container.addChild( createCell( i ) ) ;
            }
            
            layout.run() ;
            
            ////////
            
            scrollpane.content = container ;
            
            scrollpane.update() ;
        }
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            switch( code )
            {
                case Keyboard.SPACE :
                {
                    break ;
                }
                case Keyboard.DOWN :
                {
                    break ;
                }
                case Keyboard.UP :
                {
                    break ;
                }
            }
        }
    }
}
