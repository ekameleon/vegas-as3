﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.components.buttons.views
{
    import molecule.logger;
    import molecule.render.starling.components.buttons.SimpleButton;
    import molecule.render.starling.components.buttons.display.CardDisabled;
    import molecule.render.starling.components.buttons.display.CardDown;
    import molecule.render.starling.components.buttons.display.CardOver;
    import molecule.render.starling.components.buttons.display.CardUp;

    import starling.display.Image;
    import starling.display.Quad;
    import starling.display.Sprite;
    import starling.textures.Texture;

    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;
    
    public class SimpleButtonView extends Sprite 
    {
        public function SimpleButtonView()
        {
            ////////////
            
            button1 = createButton(25,25,50,50) ;
            button2 = createButton(150,25,50,50) ;
            
            button1.groupName = "simple-button-group" ;
            button2.groupName = "simple-button-group" ;
            
            button1.toggle = true ;
            button2.toggle = true ;
            
            addChild( button1 ) ;
            addChild( button2 ) ;
        }
        
        public var button1:SimpleButton ;
        
        public var button2:SimpleButton ;
        
        public function createButton( x:Number = 0 , y:Number = 0 , w:Number = 0 , h:Number = 0 ):SimpleButton
        {
            var button:SimpleButton = new SimpleButton() ;
            
            button.useHandCursor = true ;
            
            button.down.connect( down ) ;
            button.select.connect( select ) ;
            button.up.connect( up ) ;
            button.unselect.connect( unselect ) ;
            
            ////////////
            
            button.lock() ;
            
            button.upState       = new Quad(w,h,0xFF0000);
            button.overState     = new Quad(w,h,0xFFFFFF);
            button.downState     = new Quad(w,h,0x000000);
            button.disabledState = new Quad(w,h,0xCCCCCC);
            
            button.unlock() ;
            
            button.update() ;
            
            ////////////
            
            button.x = x ;
            button.y = y ;
            
            return button ;
        }
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            switch( code )
            {
                case Keyboard.SPACE :
                {
                    button1.enabled = !button1.enabled ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    button1.toggle = !button1.toggle ;
                    break ;
                }
                case Keyboard.UP :
                {
                    button1.lock() ;
                    
                    button1.upState       = new Image( Texture.fromBitmap( new CardUp() ) ) ;
                    button1.overState     = new Image( Texture.fromBitmap( new CardOver() ) ) ;
                    button1.downState     = new Image( Texture.fromBitmap( new CardDown() ) ) ;
                    button1.disabledState = new Image( Texture.fromBitmap( new CardDisabled() ) ) ;
                    
                    button1.unlock() ;
                    
                    button1.update() ;
                    
                    break ;
                }
            }
        }
        
        ////////////
        
        protected function up( who:SimpleButton ):void
        {
            logger.info( this + " up") ;
        }
        
        protected function down( who:SimpleButton ):void
        {
            logger.info( this + " down") ;
        }
        
        protected function select( who:SimpleButton ):void
        {
            logger.info( this + " select") ;
        }
        
        protected function unselect( who:SimpleButton ):void
        {
            logger.info( this + " unselect") ;
        }
    }
}
