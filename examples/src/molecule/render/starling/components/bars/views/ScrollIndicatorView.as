﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [VEGAS framework].
  
  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2013
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling.components.bars.views
{
    import graphics.Direction;
    import graphics.geom.EdgeMetrics;

    import molecule.logger;
    import molecule.render.starling.components.bars.ScrollIndicator;
    import molecule.render.starling.components.bars.ScrollIndicatorStyle;

    import starling.display.Sprite;

    import flash.events.KeyboardEvent;
    import flash.ui.Keyboard;
    
    [SWF(width="980", height="760", frameRate="24", backgroundColor="#333333")]
    
    public class ScrollIndicatorView extends Sprite 
    {
        public function ScrollIndicatorView()
        {
            ////////////
            
            indicator = new ScrollIndicator() ;
            
            indicator.lock() ;
            
            indicator.changed.connect( change ) ;
            
            indicator.direction = Direction.VERTICAL ;
            indicator.border    = new EdgeMetrics(2,2,2,2);
            indicator.position  =  50 ;
            indicator.thumbSize =  60 ;
            indicator.w         = 8 ;
            indicator.h         = 220 ; 
            indicator.x         = 150 ;
            indicator.y         =  50 ;
            
            indicator.style = style ;
            
            indicator.unlock() ;
            
            indicator.update();
            
            addChild( indicator ) ;
        }
        
        public var indicator:ScrollIndicator ;
        
        protected const style:ScrollIndicatorStyle = new ScrollIndicatorStyle({ barColor : 0x921085 , thumbColor : 0x000000 }) ;
        
        public function change( bar:ScrollIndicator ):void
        {
            logger.debug( "change position : " + bar.position ) ;
        }
        
        public function keyDown( e:KeyboardEvent ):void
        {
            var code:uint = e.keyCode ;
            switch( code )
            {
                case Keyboard.LEFT :
                {
                    indicator.position -= 10 ;
                    break ;
                }
                case Keyboard.RIGHT :
                {
                    indicator.position += 10 ;
                    break ;
                }
                case Keyboard.UP :
                {
                    indicator.position = indicator.minimum ;
                    break ;
                }
                case Keyboard.DOWN :
                {
                    indicator.position = indicator.maximum ;
                    break ;
                }
                case Keyboard.SPACE :
                {
                    indicator.lock() ;
                    indicator.direction = ( indicator.direction == Direction.VERTICAL ) ? Direction.HORIZONTAL : Direction.VERTICAL ;
                    indicator.style = new ScrollIndicatorStyle() ;
                    if ( indicator.direction == Direction.VERTICAL )
                    {
                        indicator.setPreferredSize( 8 , 200 ) ;
                    }
                    else
                    {
                        indicator.setPreferredSize( 350, 8 ) ;
                    }
                    indicator.unlock() ;
                    indicator.update() ;
                    break ;
                }
                default :
                {
                    indicator.minimum = 20 ; // change the minimum value of the bar
                    indicator.maximum = 80 ; // change the maximum value of the bar
                    indicator.invert  = !indicator.invert ; // invert the positive direction of the bar scroll.
                }
            }
        }
    }
}
