# VEGAS ActionScript OpenSource Framework - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [2.0.0]
### Changed
* New version

## [1.0.0] - 2016-08-31
### Added
* Old version

