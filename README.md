# VEGAS #

Vegas is an opensource framework based on ECMAScript and ActionScript.

In progress : 
* 2016-09 : Implements the **molecule** package with the new Starling version 2.1 
* 2016-08 : Fix the compatibility between the JS version of VEGAS and this AS3 version (core + system)


### Licences ###

Under tree licences : **MPL 2.0/GPL 2.+/LGPL 2.1+**

 * Licence MPL  2.0 : https://www.mozilla.org/en-US/MPL/2.0/
 * Licence GPL  2.0 : http://www.gnu.org/licenses/gpl-2.0.html
 * Licence LGPL 2.1 : http://www.gnu.org/licenses/lgpl-2.1.html

### About ###

 * Author : Marc ALCARAZ (aka eKameleon)
 * Mail : ekameleon@gmail.com
 * Link : http://www.ooopener.com

### Slack Community ###

![slack-logo-vector-download.jpg](https://bitbucket.org/repo/AEbB9b/images/3509366499-slack-logo-vector-download.jpg)

Send us your email to join the VEGAS community on Slack !

### Editor ###

FDT, the best ActionScript editor Forever !

![FDT_Supported.png](https://bitbucket.org/repo/AEbB9b/images/525527175-FDT_Supported.png) 

### History ###

 * 1998 : Flash 
 * 2000 : First framework concept and first libraries (components, tools, design patterns)
 * 2004 : First official SVN repository 
 * 2007 : Fusion with the Maashaack framework (eden, etc.)
 * 2015 : Google Code must die - **VEGAS** move from an old Google Code SVN repository to this Bitbucket GIT repository and REBOOT this source code.