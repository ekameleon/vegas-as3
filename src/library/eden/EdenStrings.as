﻿/*
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is [eden: ECMAScript data exchange notation AS3]. 
  
  The Initial Developer of the Original Code is
  Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2004-2016
  the Initial Developer. All Rights Reserved.
  
  Contributor(s):
  Marc Alcaraz <ekameleon@gmail.com>.
  
*/

package library.eden
{
    /**
    * Ressource strings for the eden library.
    */
    public class EdenStrings
    {
        public function EdenStrings() {}
        
        public var separator:String = "----------------------------------------------------------------";
        public var requirePairValue:String = "multiSerialize require pairs of values";
        public var pairIsIgnored:String = "name \"{0}\" is not a string, pair[{1},{2}] is ignored";
        public var reservedKeyword:String = "\"{0}\" is a reserved keyword";
        public var futureReservedKeyword:String = "\"{0}\" is a future reserved keyword";
        public var notValidPath:String = "\"{0}\" is not a valid path";
        public var unterminatedComment:String = "unterminated comment";
        public var notValidConstructor:String = "\"{0}\" is not a valid constructor";
        public var doesNotExist:String = "\"{0}\" does not exists";
        public var errorArray:String = "bad array (unterminated array)";
        public var errorComment:String = "syntax error (comment)";
        public var errorConstructor:String = "bad constructor";
        public var errorIdentifier:String = "bad identifier";
        public var errorLineTerminator:String = "bad string (found line terminator in string)";
        public var errorNumber:String = "bad number (not finite)";
        public var errorObject:String = "bad object (unterminated object)";
        public var errorString:String = "bad string (unterminated string)";
        public var malformedHexadecimal:String = "bad number (malformed hexadecimal)";
        public var extRefDoesNotExist:String = "external reference \"{0}\" does not exists";
        public var errorKeyword:String = "syntax error";
        public var notAuthorizedConstructor:String = "\"{0}\" is not an authorized constructor";
        public var notAuthorizedExternalReference:String = "\"{0}\" is not an authorized external reference";
        public var notAuthorizedPath:String = "\"{0}\" is not an authorized path";
        public var notValidFunction:String = "\"{0}\" is not a valid function";
        public var errorFunction:String = "bad function";
        public var notFunctionCallAllowed:String = "function call \"{0}( {1} )\"is not allowed";
        public var notAuthorizedFunction:String = "\"{0}()\" is not an authorized function on type \"{1}\"";
        public var RHSmissing:String = "RHS is missing";
        public var assignWithoutRHS:String = "equal assignement without RHS";
        public var unterminatedParenthesis:String = "unterminated parenthesis, check your function/constructor \"{0}\"";
        public var malformedCtor:String = "malformed constructor \"{0}( {1} )\": {2}";
        public var notFoundInMemory:String = "definition \"{0}\" not found in memory (both global and local pool)";
        public var addAuthorizedFailed:String = "addAuthorized failed with a null 'authorized' Array to configurate the eden parser.";
    }	
}
