﻿/*
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at 
  http://www.mozilla.org/MPL/ 
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the License. 
  
  The Original Code is [eden: ECMAScript data exchange notation AS3]. 
  
  The Initial Developer of the Original Code is
  Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developer are Copyright (C) 2004-2016
  the Initial Developer. All Rights Reserved.
  
  Contributor(s):
  Marc Alcaraz <ekameleon@gmail.com>.
  
*/

package library.eden
{
    import core.output;
    import core.strings.format;
    
    /**
     * Stores static metadata about the project.
     */
    public class metadata
    {
        /** Name of the project. */ 
        public static var name:String = "eden";
        
        /** Full name of the project. */
        public static var fullname:String = "ECMAScript data exchange notation AS3";
        
        /** Copyright of the project. */
        public static var copyright:String = "Copyright © 2004-2015 Zwetan Kjukov, All right reserved.";
        
        /** Origin of the project. */
        public static var origin:String = "Made in the EU.";
        
        /**
        * Prints the informations of the package.
        * 
        * @param verbose (optional) display more informations, default is <code>false</code>
        * @param showConfig (optional) display the pretty printing of the config object, default is <code>false</code>
        */
        public static function about( verbose:Boolean = false, showConfig:Boolean = false ):void
        {
            output( info( verbose, showConfig ) );
        }
        
        /**
        * Returns the informations of the package.
        * 
        * @param verbose (optional) add more informations, default is <code>false</code>
        * @param showConfig (optional) add the pretty printing of the config object, default is <code>false</code>
        */
        public static function info( verbose:Boolean = false, showConfig:Boolean = false ):String
        {
            var CRLF:String = "\n";
        
            var str:String = "";
                if( ! verbose && config.verbose )
                {
                    verbose = true;
                }
                
                if( verbose ) 
                {
                str += "{sep}{crlf}";
                str += "{name}: {fullname} v{version}{crlf}";
                str += "{sep}";
                } else {
                str += "{name} v{version}"; 
                }
                
                if( showConfig ) {
                str += "{crlf}config:";
                str += "{config}{crlf}";
                str += "{sep}";
                }
            
            return format
            ( 
                str,
               {
                   sep: strings.separator,
                   crlf: CRLF,
                   name: metadata.name,
                   fullname: metadata.fullname,
                   version: version,
                   config: config.toSource()
               }
            );
        }
        
        
        /**
         * Stores the configuration options of the package.
         */
        public static var config:EdenConfigurator = new EdenConfigurator( {
                            compress: true,
                            copyObjectByValue: false,
                            strictMode: true,
                            undefineable: undefined,
                            verbose: false,
                            security: false,
                            allowAliases: true,
                            allowFunctionCall: true,
                            arrayIndexAsBracket: false,
                            authorized: 
                            [
                                "Array.*",
                                "Boolean.*",
                                "Date.*",
                                "Error.*",
                                "Math.*",
                                "Number.*",
                                "Object.*",
                                "String.*",
                                "Infinity" 
                            ],
                            reserved:
                            [
                                "break", 
                                "case", "catch", "continue", 
                                "default", "delete", "do", 
                                "else", 
                                "finally", "for", "function", 
                                "if", "in", "instanceof", "is",
                                "new", 
                                "return", 
                                "switch", 
                                "this", "throw", "try", "typeof", 
                                "var", "void", 
                                "while", "with"
                            ],
                            reservedFuture:
                            [
                                "abstract" ,
                                "boolean",  "byte",
                                "char",  "class",  "const",
                                "debugger", "double",
                                "enum", "export", "extends",
                                "final", "float",
                                "goto",
                                "implements", "import", "int", "interface",
                                "long",
                                "native",
                                "package", "private", "protected", "public",
                                "short", "static", "super", "synchronized",
                                "throws", "transient",
                                "volatile"
                            ]
                                                                        } );
        
        /**
         * Stores the string resources of the package. 
         */
        public static var strings:EdenStrings = new EdenStrings();
        
    }
}
