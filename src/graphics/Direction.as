﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

package graphics 
{
    /**
     * The most common relative directions are horizontal, vertical, both, left, right, forward, backward, none, up, and down.
     */
    public final class Direction 
    {
        /**
         * Specifies the "backward" value to change the orientation of a Display or a component.
         */
        public static const BACKWARD:String = "backward" ;
        
        /**
         * Specifies the "both" value to represent both horizontal and vertical scrolling.
         */
        public static const BOTH:String = "both" ;
        
        /**
         * Specifies the "down" value to change the orientation of a Display or a component.
         */
        public static const DOWN:String = "down" ;
        
        /**
         * Specifies the "forward" value to change the direction or scrolling of a Display or a component.
         */
        public static const FORWARD:String = "forward" ;
        
        /**
          * Specifies the "horizontal" value to change the orientation of a Display or a component.
          */
        public static const HORIZONTAL:String = "horizontal" ;
        
        /**
         * Specifies the "left" value to change the orientation of a Display or a component.
         */
        public static const LEFT:String = "left" ;
        
        /**
         * Specifies the "none" value to represent no scrolling or an object without direction.
         */
        public static const NONE:String = "none" ;
        
        /**
         * Specifies the "right" value to change the orientation of a Display or a component.
         */
        public static const RIGHT:String = "right" ;
        
        /**
         * Specifies the "up" value to change the orientation of a Display or a component.
         */
        public static const UP:String = "up" ;
        
        /**
         * Specifies the "vertical" value to change the orientation of a Display or a component.
         */
        public static const VERTICAL:String = "vertical" ;
    }
}
