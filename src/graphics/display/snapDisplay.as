﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

package graphics.display
{
    import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.geom.Matrix;
    import flash.geom.Rectangle;
    
    /**
     * Creates a new BitmapData who snap the specific display object.
     * @param display The DisplayObject reference to snap.
     * @param quality Any of one of the StageQuality values. Selects the antialiasing quality to be used when drawing vectors graphics.
     */
    public function snapDisplay( display:DisplayObject , scale:Number = 1, quality:String = null, margin:Number = .5, transparent:Boolean = true  ):BitmapData
    {
        if( isNaN( scale ) )
        {
            scale = 1 ;
        }
        
        var bounds:Rectangle = display.getBounds( null ) ;
        
        var bmp:BitmapData ;
        
        var matrix:Matrix ;
        
        if( scale != 1 )
        {
            bmp = new BitmapData( int( bounds.width * scale + margin ) , int( bounds.height * scale + margin ) , transparent , 0 ) ;
            matrix = new Matrix( 1 , 0 , 0 , 1 , -bounds.x , -bounds.y ) ;
            matrix.scale( scale , scale ) ;
        }
        else
        {
            bmp = new BitmapData( int( bounds.width * display.scaleX + margin ) , int( bounds.height * display.scaleY + margin ) , transparent , 0 ) ;
            matrix = new Matrix( 1 , 0 , 0 , 1 , -bounds.x , -bounds.y ) ;
            matrix.scale( display.scaleX , display.scaleY ) ;
        }
        
        if( quality != null )
        {
            bmp.drawWithQuality( display , matrix , null, null, null,  scale != 1 , quality ) ;
        }
        else
        {
            bmp.draw( display , matrix , null, null, null, !isNaN(scale) && (scale != 1) ) ;
        }
        
        return bmp ;
    }
}
