﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [maashaack framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.date 
{
    /**
     * The DayOfWeek strings configuration.
     */
    public class DayOfWeekStrings
    {
        /**
         * Creates a new DayOfWeekStrings instance.
         */
        public function DayOfWeekStrings()
        {

        }

        /**
         * The 'all' expression.
         */
        public var all:String = DayOfWeek.ALL ;

        /**
         * The 'days' expressions.
         */
        public var days:Array =
        [
            { id : DayOfWeek.MO , name : DayOfWeek.MONDAY    , short : DayOfWeek.MO } ,
            { id : DayOfWeek.TU , name : DayOfWeek.TUESDAY   , short : DayOfWeek.TU } ,
            { id : DayOfWeek.WE , name : DayOfWeek.WEDNESDAY , short : DayOfWeek.WE } ,
            { id : DayOfWeek.TH , name : DayOfWeek.THURSDAY  , short : DayOfWeek.TH } ,
            { id : DayOfWeek.FR , name : DayOfWeek.FRIDAY    , short : DayOfWeek.FR } ,
            { id : DayOfWeek.SA , name : DayOfWeek.SATURDAY  , short : DayOfWeek.SA } ,
            { id : DayOfWeek.SU , name : DayOfWeek.SUNDAY    , short : DayOfWeek.SU } ,
            { id : DayOfWeek.PH , name : DayOfWeek.PUBLIC    , short : DayOfWeek.PH } ,
            { id : DayOfWeek.SH , name : DayOfWeek.SCHOOL    , short : DayOfWeek.SH } 
        ];

        /**
         * The 'none' expressions.
         */
        public var none:String = DayOfWeek.NONE ;

        /**
         * The 'specials' expressions.
         */
        public var specials:String = DayOfWeek.SPECIALS ;

        /**
         * The 'week' expressions.
         */
        public var week:String = DayOfWeek.WEEK ;

        /**
         * The 'weekend' expressions.
         */
        public var weekend:String = DayOfWeek.WEEKEND ;

        /**
         * Initialize the object with a i18n object.
         * @example
         * <code>
         * var fr =
         * {
         *     all      : ALL ,
         *     none     : NONE ,
         *     specials : SPECIALS ,
         *     week     : WEEK ,
         *     weekend  : WEEKEND ,
         *     days     :
         *     {
         *         'mo' : { name : 'lundi'    , short : 'lun' } ,
         *         'tu' : { name : 'mardi'    , short : 'mar' } ,
         *         'we' : { name : 'mercredi' , short : 'mer' } ,
         *         'th' : { name : 'jeudi'    , short : 'jeu' } ,
         *         'fr' : { name : 'vendredi' , short : 'ven' } ,
         *         'sa' : { name : 'samedi'   , short : 'sam' } ,
         *         'su' : { name : 'dimanche' , short : 'dim' } ,
         *         'ph' : { name : 'vacances' , short : 'vac' } ,
         *         'sh' : { name : 'scolaire' , short : 'sco' } 
         *     }
         * } ;
         * var dayOfWeek = new DayOfWeek() ;
         * dayOfWeek.locale( fr ) ;
         * trace( dayOfWeek.format( dayOfWeek.parse('Mo-Th,Fr,Sa-Su,Ph,Sh') , true ) ) ;
         * trace( dayOfWeek.format( dayOfWeek.parse('Mo-Su') , true ) ) ;
         * trace( dayOfWeek.format( dayOfWeek.parse('Sa-Su') , true ) ) ;
         * </code>
         */
        public function locale( init:Object = null ):void
        {
            if( !init )
            {
                all      = DayOfWeek.ALL ;
                none     = DayOfWeek.NONE ;
                specials = DayOfWeek.SPECIALS ;
                week     = DayOfWeek.WEEK ;
                weekend  = DayOfWeek.WEEKEND ;
                days     =
                [
                    { id : MO , name : MONDAY    , short : MO } ,
                    { id : TU , name : TUESDAY   , short : TU } ,
                    { id : WE , name : WEDNESDAY , short : WE } ,
                    { id : TH , name : THURSDAY  , short : TH } ,
                    { id : FR , name : FRIDAY    , short : FR } ,
                    { id : SA , name : SATURDAY  , short : SA } ,
                    { id : SU , name : SUNDAY    , short : SU } ,
                    { id : PH , name : PUBLIC    , short : PH } ,
                    { id : SH , name : SCHOOL    , short : SH } 
                ];
            }
            else
            {
                var props:Vector.<String> = Vector.<String>
                ([
                    'all','none','specials','week','weekend'
                ]);

                for each( var prop:String in props )
                {
                    if( (prop in init) && (init[prop] is String) )
                    {
                        this[prop] = init[prop] ;
                    }
                }

                if( ('days' in init) )
                {
                    var cur:Object ;
                    for each( var day:Object in days )
                    {
                        if( day.id in init['days'] )
                        {
                            cur = init['days'][day.id] ;
                            if( 'name' in cur && (cur.name is String) )
                            {
                                day.name = cur.name ;
                            }
                            if( 'short' in cur && (cur.short is String) )
                            {
                                day.short = cur.short ;
                            }
                        }

                    }
                }
            }
        }

        /**
         * Returns the custom object to stringify in the JSON.stringify() method.
         * @return the custom object to stringify in the JSON.stringify() method.
         */
        public function toJSON( expression:String ):*
        {
            var object:Object =
            {
                all      : all ,
                none     : none ,
                specials : specials ,
                week     : week ,
                weekend  : weekend ,
                days     :
                {
                    'mo' : { name : days[0].name , short : days[0].short } ,
                    'tu' : { name : days[1].name , short : days[1].short } ,
                    'we' : { name : days[2].name , short : days[2].short } ,
                    'th' : { name : days[3].name , short : days[3].short } ,
                    'fr' : { name : days[4].name , short : days[4].short } ,
                    'sa' : { name : days[5].name , short : days[5].short } ,
                    'su' : { name : days[6].name , short : days[6].short } ,
                    'ph' : { name : days[7].name , short : days[7].short } ,
                    'sh' : { name : days[8].name , short : days[8].short }
                }
            } ;
            return object ;
        }
    }
}
