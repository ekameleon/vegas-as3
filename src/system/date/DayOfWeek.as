﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [maashaack framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.date
{
    import core.strings.fastformat ;

    /**
     * Format and parse the dayOfWeek objects.
     * @example
     * <pre>
     * import system.date.DayOfWeek ;
     *
     * var dayOfWeek = new DayOfWeek() ;
     *
     * trace( dayOfWeek.parse('Mo') ) ;
     * trace( dayOfWeek.parse('Mo,Th,Ph') ) ;
     * trace( dayOfWeek.parse('Mo-Th,Fr,Sa-Su,Ph,Sh') ) ;
     * trace( dayOfWeek.format(['Ph','Sh'],true) ) ;
     * </pre>
     */
    public class DayOfWeek
    {
        /**
         * Creates a new DayOfWeek instance.
         */
        public function DayOfWeek( init:Object = null )
        {
            if( init )
            {
                for( var prop:String in init )
                {
                    this[prop] = init[prop] ;
                }
            }
        }

        // ---------

        public static const ALL:String      = 'Everyday' ;
        public static const NONE:String     = 'none' ;
        public static const SPECIALS:String = '{0} / {1}' ;
        public static const WEEK:String     = 'Working days' ;
        public static const WEEKEND:String  = 'Week end' ;

        public static const MONDAY:String    = 'Monday' ;
        public static const TUESDAY:String   = 'Tuesday' ;
        public static const WEDNESDAY:String = 'Wednesday' ;
        public static const THURSDAY:String  = 'Thursday' ;
        public static const FRIDAY:String    = 'Friday' ;
        public static const SATURDAY:String  = 'Saturday' ;
        public static const SUNDAY:String    = 'Sunday' ;
        public static const PUBLIC:String    = 'Public holidays' ;
        public static const SCHOOL:String    = 'School holidays' ;

        public static const MO:String = 'Mo' ;
        public static const TU:String = 'Tu' ;
        public static const WE:String = 'We' ;
        public static const TH:String = 'Th' ;
        public static const FR:String = 'Fr' ;
        public static const SA:String = 'Sa' ;
        public static const SU:String = 'Su' ;
        public static const PH:String = 'Ph' ;
        public static const SH:String = 'Sh' ;

        // ---------

        /**
         * The enumeration of all the day labels.
         */
        public const days:Vector.<String> = Vector.<String>([ 'Mo','Tu','We','Th','Fr','Sa','Su']) ;

        /**
         * The enumeration of all the special day labels.
         */
        public const specials:Vector.<String> = Vector.<String>([ 'Ph' , 'Sh' ]) ;

        /**
         * The strings configuration of the parser.
         */
        public const strings:DayOfWeekStrings = new DayOfWeekStrings() ;

        /**
         * Formats the specific collection of days.
         */
        public function format( days:Array , humanReadble:Boolean = false ):String
        {
            var exp:String = '' ;

            if( !days || days.length == 0 )
            {
                return (humanReadble) ? this.strings.none : '' ;
            }

            var i:int ;

            var cur:String ;
            var pre:String ;
            var pos:int ;

            var len:int = days.length ;

            for( i = 0 ; i < len ; i++ )
            {
                if( pre )
                {
                    exp += ',' ;
                    pre = null ;
                }

                cur = days[i] ;

                if( this.days.indexOf( cur ) > -1 )
                {
                    pos  = this.days.indexOf( cur ) ;
                    exp += cur ;

                    if( days[i+1] )
                    {
                        if( this.days.indexOf(days[i+1]) == (pos+1) )
                        {
                            do
                            {
                                cur = days[i+1] ;
                                i++ ;
                                pos++ ;
                            }
                            while ( this.days.indexOf(days[i+1]) == (pos+1) );

                            exp += '-' + cur ;
                        }
                    }
                }
                else if( this.specials.indexOf( cur ) > -1 )
                {
                    pos = this.specials.indexOf( cur ) ;
                    exp += cur ;
                }

                pre = cur ;
            }

            if( humanReadble )
            {
                var items:Array ;

                switch( exp )
                {
                    case '' :
                    {
                        return this.strings.none ;
                    }
                    case 'Mo-Fr' :
                    {
                        return this.strings.week ;
                    }
                    case 'Sa-Su' :
                    {
                        return this.strings.weekend ;
                    }
                    case 'Mo-Su' :
                    {
                        return this.strings.all ;
                    }
                    case 'Ph,Sh' :
                    case 'Ph,Sh' :
                    {
                        items = this.strings.days.filter(function( value:* , index:int , array:Array ):*
                        {
                            return value && (value.id == 'Ph' || value.id == 'Sh') ;
                        }) ;
                        return fastformat
                        (
                            strings.specials,
                            items[0].name ,
                            items[1].name
                        );
                    }
                    default :
                    {
                        if( this.days.indexOf(exp) > -1 )
                        {
                            items = this.strings.days.filter(function( value:* , index:int , array:Array ):*
                            {
                                return value && value.id == exp ;
                            }) ;
                            return items[0].name ;
                        }

                        len = this.days.length ;
                        for( i = 0 ; i<len ; i++ )
                        {
                            cur = this.days[i] ;
                            items = this.strings.days.filter(function( value:* , index:int , array:Array ):*
                            {
                                return value && value.id == cur ;
                            }) ;
                            if( items && items.length > 0 )
                            {
                                exp = exp.replace( cur , items[0].short ) ;
                            }
                        }

                        len = this.specials.length ;
                        for( i = 0 ; i<len ; i++ )
                        {
                            cur = this.specials[i] ;
                            items = this.strings.days.filter(function( value:* , index:int , array:Array ):*
                            {
                                return value && value.id == cur ;
                            }) ;
                            if( items && items.length > 0 )
                            {
                                exp = exp.replace( cur , items[0].name ) ;
                            }
                        }
                    }
                }
            }

            return exp ;
        }

        /**
         * Parse the passed-in expression.
         */
        public function parse( expression:String ):Array
        {
            var i     :int ,
                j     :int ,
                count :int ,
                len   :int ;

            var collector:Object = {} ;
            var days:Array       = [] ;

            if( expression && expression != ''  )
            {
                var item:String ;
                var items:Array = expression.split(',') ;

                len = items.length ;

                for( i = 0 ; i<len ; i++ )
                {
                    item = items[i] ;

                    switch( true )
                    {
                        case     this.days.indexOf( item ) > -1  :
                        case this.specials.indexOf( item ) > -1 :
                        {
                            collector[item] = true ;
                            break ;
                        }

                        case ( item.indexOf('-') > -1 ) :
                        {
                            var ar:Array = item.split('-') ;
                            if( ar.length == 2 )
                            {
                                var first:int = this.days.indexOf(ar[0]) ;
                                var last:int  = this.days.indexOf(ar[1]) ;
                                if( (first > -1) && (last > -1) && (last > first) )
                                {
                                    var subdays:Vector.<String> = this.days.slice( first , last+1 ) ;
                                    count = subdays.length ;
                                    for( j = 0 ; j<count ; j++ )
                                    {
                                        collector[subdays[j]] = true ;
                                    }
                                }
                            }
                            break ;
                        }
                    }
                }
            }

            len = this.days.length ;
            for( i = 0 ; i<len ; i++ )
            {
                if( collector[this.days[i]] === true )
                {
                    days.push(this.days[i]) ;
                }
            }

            len = this.specials.length ;
            for( i = 0 ; i<len ; i++ )
            {
                if( collector[this.specials[i]] === true )
                {
                    days.push(this.specials[i]) ;
                }
            }

            return days ;
        }
    }
}
