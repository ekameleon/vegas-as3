﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [maashaack framework].

  The Initial Developers of the Original Code are
  Marc Alcaraz <ekameleon@gmail.com> and Zwetan Kjukov <zwetan@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package system.date
{
    /**
     * Tool class to transform ISO8601 to Date.
     */
    public class ISO8601
    {
        /**
         * Creates a new ISO8601 instance.
         */
        public function ISO8601()
        {

        }

        public const COLON:String = ":";

        public const DASH:String  = "-";

        public const T:String     = "T";

        public const ZERO:String  = "0";

        public const ZULU:String  = "Z";

        /**
         * Converts an AS3 Date object into an ISO-8601 UTC extended date and time String (YYYY-MM-HHTHH:MM:SSZ).
         * The zulu designation (Z) at the end of the string indicates the time is UTC (Coordinated Universal Time).
         */
        public function formatExtendedDateTime( date:Date ):String
        {
            return formatExtendedDate( date )
                   + T
                   + formatExtendedTime( date )
                   + ZULU;
        }

        /**
         * Converts an AS3 Date object into an ISO-8601 UTC basic date and time String. The Basic format has no hyphens or
         * colons, but does have a UTC zulu designation at the end.
         */
        public function formatBasicDateTime( date:Date ):String
        {
            return formatBasicDate( date )
                 + T
                 + formatBasicTime( date )
                 + ZULU;
        }

        /**
         * Converts an ISO-8601 date + time (UTC) string of format "2009-02-21T09:00:00Z" to an AS3 Date Object.
         * The zulu designation (Z) at the end of the string indicates the time is UTC (Coordinated Universal Time).
         * Even if the zulu designation is missing UTC will be assumed.
         */
        public function parseDateTimeString( val:String ):Date
        {
            //first strip all non-numerals from the String ( convert all extended dates to basic)
            val = val.replace( /-|:|T|Z/g, "" );

            var date:Date = parseBasicDate( val.substr( 0, 8 ) );
                     date = parseBasicTime( val.substr( 8, 6 ) , date );

            return date;
        }


        /**
         * Parse the basic date expression in Date.
         */
        public function parseBasicDate( val:String, date:Date = null ):Date
        {
            if ( date == null )
            {
                date = new Date();
            }

            date.setUTCFullYear( convertYear( val ), convertMonth( val ), convertDate( val ) );

            return date;
        }

        /**
         * Parse the basic time expression in Date.
         */
        public function parseBasicTime( val:String, date:Date = null ):Date
        {
            if ( date == null )
            {
                date = new Date();
            }

            date.setUTCHours( convertHours( val ), convertMinutes( val ), convertSeconds( val ) );

            return date;
        }

        /**
         * Formats the specified Date in an extended date.
         */
        public function formatExtendedDate( date:Date ):String
        {
            return formatYear( date.getUTCFullYear() )
                   + DASH
                   + formatMonth( date.getUTCMonth() )
                   + DASH
                   + formatDate( date.getUTCDate() ) ;
        }

        /**
         * Formats the specified Date in a basic date.
         */
        public function formatBasicDate( date:Date ):String
        {
            return formatYear( date.getUTCFullYear() )
                 + formatMonth( date.getUTCMonth() )
                 + formatDate( date.getUTCDate() );
        }

        /**
         * Formats the specified Date in an extended time.
         */
        public function formatExtendedTime( date:Date ):String
        {
            return formatTimeChunk( date.getUTCHours() )
                 + COLON
                 + formatTimeChunk( date.getUTCMinutes() )
                 + COLON
                 + formatTimeChunk( date.getUTCSeconds() );
        }

        /**
         * Formats the specified Date in a basic time.
         */
        public function formatBasicTime( date:Date ):String
        {
            return formatTimeChunk( date.getUTCHours() )
                 + formatTimeChunk( date.getUTCMinutes() )
                 + formatTimeChunk( date.getUTCSeconds() );
        }

        /**
         * Assumes an 8601 basic date string (8 characters YYYYMMDD)
         */
        private function convertYear( val:String ):int
        {
            val = val.substr( 0, 4 );
            return parseInt( val );
        }

        /**
         * assumes an 8601 basic date string (8 characters YYYYMMDD)
         */
        private function convertMonth( val:String ):int
        {
            val = val.substr( 4, 2 );
            var y:int = parseInt( val ) - 1; // months are zero indexed in Date objects so we need to decrement
            return y;
        }

        /**
         * Assumes an 8601 basic date string (8 characters YYYYMMDD)
         * @private
         */
        private function convertDate( val:String ):int
        {
            return parseInt( val.substr( 6, 2 ) );
        }

        /**
         * Assumes a 8601 basic UTC time string (6 characters HHMMSS).
         * @private
         */
        private function convertHours( value:* ):int
        {
            return parseInt( value.slice(0,2) );
        }

        /**
         * Assumes a 8601 basic UTC time string (6 characters HHMMSS).
         * @private
         */
        private function convertMinutes( val:String ):int
        {
            return parseInt( val.substr( 2, 2 ) );
        }

        /**
         * assumes a 8601 basic UTC time string (6 characters HHMMSS)
         */
        private function convertSeconds( val:String ):int
        {
            return parseInt( val.substr( 4, 2 ) );
        }

        /**
         * @private
         */
        private function formatYear( year:int ):String
        {

            var y:String = year.toString(); // doesn't handle BC dates

            if ( year < 10 )
            {
                y = ZERO + ZERO + ZERO + y; // 0009 0010 0099 0100
            }
            else if ( year < 100 )
            {
                y = ZERO + ZERO + y;
            }
            else if ( year < 1000 )
            {
                y = ZERO + y;
            }

            return y;
        }

        /**
         * @private
         */
        private function formatMonth( month:int ):String
        {
            month++ ; // Date object months are zero indexed so always increment the month up by one

            var m:String = month.toString();

            if ( month < 10 )
            {
                m = ZERO + m;
            }

            return m;
        }

        /**
         * @private
         */
        private function formatDate( date:int ):String
        {
            var d:String = date.toString() ;

            if ( date < 10 )
            {
                d = ZERO + d;
            }

            return d;
        }

        /**
         * @private
         */
        private function formatTimeChunk( val:int ):String
        {
            var t:String = val.toString();

            if ( val < 10 )
            {
                t = ZERO + t;
            }

            return t;
        }
    }
}
