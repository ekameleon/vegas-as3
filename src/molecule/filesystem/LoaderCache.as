﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

package molecule.filesystem 
{
    import system.process.Resumable;
    import system.process.Stoppable;
    import system.process.Task;
    import system.process.TaskPhase;
    import system.signals.Signal;

    import molecule.logger;

    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.JPEGEncoderOptions;
    import flash.display.JPEGXREncoderOptions;
    import flash.display.Loader;
    import flash.display.LoaderInfo;
    import flash.display.PNGEncoderOptions;
    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.HTTPStatusEvent;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.events.SecurityErrorEvent;
    import flash.filesystem.File;
    import flash.filesystem.FileMode;
    import flash.filesystem.FileStream;
    import flash.geom.Rectangle;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.system.LoaderContext;
    import flash.utils.ByteArray;
    
    /**
     * The LoaderCache class is used to load SWF files or image (JPG, PNG, or GIF) files and cache it on a specific application loc&l storage directory with a custom name.
     */
    public class LoaderCache extends Task implements Resumable, Stoppable
    {
        /**
         * Creates a new LoaderCache instance.
         * @param url The url to load the external file and cache it.
         * @param name The name of the file in the local storage.
         * @param path The path's name to cache the file on the local storage.
         * @param root The optional root directory File reference to target, by default the loader save the files in the application storage directory.
         */
        public function LoaderCache( url:String = null , name:String = null , path:String = "" , root:File = null )
        {
            _path = path || "" ;
            
            _name = name ;
            _url  = url ;
            
            if( root && root.isDirectory )
            {
                _rootDirectory = root ;
            }
        }
        
        /**
         * The signal invoked when a file is cached.
         */
        public const cached:Signal = new Signal() ;
        
        /**
         * Returns a LoaderInfo object corresponding to the object being loaded. 
         * LoaderInfo objects are shared between the Loader object and the loaded content object. 
         * The LoaderInfo object supplies loading progress information and statistics about the loaded file. 
         */
        public function get contentLoaderInfo():LoaderInfo
        {
            return (_loader != null) ? (_loader as Loader).contentLoaderInfo : null ;
        }
        
        /**
         * The LoaderContext class provides options for loading SWF files and other media by using the Loader class. 
         * The LoaderContext class is used as the context parameter in the load() and loadBytes() methods of the Loader class.
         */
        public var context:LoaderContext ;
        
        /**
         * Indicates if the loading content is cached or not.
         */
        public function get enabled():Boolean
        {
            return _enabled ;
        }
        
        /**
         * @private
         */
        public function set enabled( value:Boolean ):void
        {
            _enabled = value ;
            if( !_enabled && exists )
            {
                clear() ;
            }
        }
        
        /**
         * Indicates the bitmap encoder used to compress the file on the local application storage. By default use a new JPEGEncoderOptions(70) ;
         */
        public function get encoder():Object
        {
            return _encoder ;
        }
        
        /**
         * @private
         */
        public function set encoder( option:Object ):void
        {
            switch( true )
            {
                case option is JPEGEncoderOptions :
                {
                    _encoder = option as JPEGEncoderOptions ; 
                    break ;
                }
                case option is PNGEncoderOptions :
                {
                    _encoder = option as PNGEncoderOptions ; 
                    break ;
                }
                case option is JPEGXREncoderOptions :
                {
                    _encoder = option as JPEGXREncoderOptions ; 
                    break ;
                }
                default :
                {
                    _encoder = new JPEGEncoderOptions(70) ; 
                }
            }
        }
        
        /**
         * The signal invoked when an error is emitted.
         */
        public const error:Signal = new Signal() ;
        
        /**
         * Indicates if a file is caching with the specific name.
         */
        public function get exists():Boolean
        {
            try
            {
                return _rootDirectory.resolvePath( _path ).resolvePath( name ).exists ;
            }
            catch( e:Error )
            {
                //
            }
            return false ;
        }
        
        /**
         * Returns the specific File item if is caching with the specific name.
         * @return the specific File item if is caching with the specific name.
         */
        public function get file():File
        {
            return _rootDirectory.resolvePath( _path ).resolvePath( _name );
        }
        
        /**
         * The collection of the URLRequestHeaders to send when the cache is loading.
         */
        public var headers:Array ;
        
        /**
         * Returns the internal Loader reference.
         * @return the internal Loader reference.
         */
        public function get loader():Loader
        {
            return _loader ;
        }
        
        /**
         * The method of the request.
         */
        public var method:String = URLRequestMethod.GET ;
        
        /**
         * Indicates the name of the file cached on the local storage.
         */
        public function get name():String
        {
            return _name ;
        }
        
        /**
         * Indicates the path name of the cache loader object.
         */
        public function get path():String
        {
            return _path ;
        }
        
        /**
         * This signal emit when the process is in progress. 
         */
        public const progressIt:Signal = new Signal() ;
        
        /**
         * The signal invoked when a file is ready.
         */
        public const ready:Signal = new Signal() ;
        
        /**
         * This signal emit when the process is resumed.
         */
        public const resumeIt:Signal = new Signal() ;
        
        /**
         * Indicates the root directory of the cache loader.
         */
        public function get rootDirectory():File
        {
            return _rootDirectory ;
        }
        
        /**
         * Indicates the storage directory of this cache object based on the current path of it.
         */
        public function get storageDirectory():File
        {
            return _rootDirectory.resolvePath( _path );
        }
        
        /**
         * This signal emit when the process is stoped.
         */
        public const stopIt:Signal = new Signal() ;
        
        /**
         * Indicates the url to load the external file before caching it.
         */
        public function get url():String
        {
            return _url;
        }
        
        /**
         * Purges the file and deletes it from the disk.
         */
        public function clear():void
        {
            try
            {
                var file:File = _rootDirectory.resolvePath( _path ).resolvePath( _name ); 
                if( file.exists )
                {
                    file.deleteFile() ;
                }
            }
            catch( er:Error  )
            {
                //
            }
        }
        
        /**
         * Dispose the object.
         */
        public function dispose():void
        {
            if( running )
            {
                _isRunning = false ;

            }
            _unregisterLoader(true) ;
            _name = null ;
            _url  = null ;
        }
        
        /**
         * Resumes the process.
         */
        public function resume() : void 
        {
            if( !_isRunning )
            {
                run() ;
            }
        }
        
        /**
         * Run the process.
         */
        public override function run( ...args:Array ):void
        {
            if( _isRunning )
            {
                return ;
            }
            
            if( _phase == TaskPhase.STOPPED )
            {
                _phase = TaskPhase.RUNNING ;
                if ( !isLocked() )
                {
                    resumeIt.emit( this ) ;
                }
            }
            else
            {
                notifyStarted();
            }
            
            _cachePhase = false ;
            _registerLoader() ;
            
            
            if( args.length > 0 )
            {
                _url  = args[0] as String ;
                _name = args[1] as String ;
            }
            
            if( _loader )
            {
                var file:File ;
                if( _url && _url.length > 0   )
                {
                    if( _name && _name.length > 0 )
                    {
                        file = _rootDirectory.resolvePath( _path ).resolvePath( _name ) ;
                    } 
                    
                    _cachePhase = !file || !file.exists ;
                    
                    // logger.log( this + " run cachePhase:" + _cachePhase + " name:" + name + " url:" + url ) ;
                    
                    var request:URLRequest = new URLRequest( _cachePhase ? _url : file.url ) ;
                    
                    if( headers && headers.length > 0 )
                    {
                        request.requestHeaders = [].concat( headers ) ;
                    }
                    
                    request.method = ( method && method.length > 0 ) ? method : URLRequestMethod.GET ;
                    
                    _loader.load( request , context ) ;
                }
            }
            else
            {
                notifyFinished() ;
            }
        }
        
        /**
         * Stops the process.
         */
        public function stop() : void 
        {
            if( running )
            {
                 _unregisterLoader() ;
                _isRunning = false ;
                _phase = TaskPhase.STOPPED ;
                if ( !isLocked() )
                {
                    stopIt.emit( this ) ;
                }
            }
        }
        
        /**
         * @private
         */
        private var _cachePhase:Boolean ;
        
        /**
         * @private
         */
        private var _enabled:Boolean = true ;
        
        /**
         * @private
         */
        private var _encoder:Object = new JPEGEncoderOptions(70) ; 
        
        /**
         * @private
         */
        private var _loader:Loader ;
        
        /**
         * @private
         */
        private var _name:String ;
        
        /**
         * @private
         */
        private var _path:String ;
        
        /**
         * @private
         */
        private var _rootDirectory:File = File.applicationStorageDirectory ;
        
        /**
         * @private
         */
        private var _url:String ;
        
        /**
         * @private
         */
        private function _complete( event:Event ):void
        {
            if( _loader ) 
            {
                //logger.log( this + " complete cachePhase:" + _cachePhase ) ;
                
                var file:File = _rootDirectory.resolvePath( _path ).resolvePath( _name ) ;
                
                //logger.debug( this + " complete file:" + file + " url:" + file.url + " content:" + _loader.content ) ;
                
                if( _cachePhase && _enabled )
                {
                    var bitmap:Bitmap = _loader.content as Bitmap ;
                    if( bitmap )
                    {
                        var area:Rectangle    = bitmap.getBounds(null) ;
                        var bmp:BitmapData    = bitmap.bitmapData ;
                        //var type:String       = _loader.contentLoaderInfo.contentType ;
                        
                        //logger.info( this + " complete #0 contentType:" + type + " area:" + area  ) ;
                        
                        if( bmp )
                        {
                            var data:ByteArray = bmp.encode( area , _encoder ) ;
                            
                            /////////////
                            
                            var stream:FileStream = new FileStream();
                                stream.open( file, FileMode.WRITE );
                                stream.writeBytes( data );
                                stream.close();
                            
                            _cachePhase = false ;
                            
                            cached.emit( this, file , _name ) ;
                        }
                    }
                }
                
                //logger.info( this + " complete #1 content:" + _loader.content ) ;
                //logger.info( this + " complete #2 ready:" + ready ) ;
                //logger.info( this + " complete #3 name:" + _name  ) ;
                //logger.info( this + " complete #4 file:" + file ) ;
                
                if ( !isLocked() )
                {
                    ready.emit( this, _loader.content , _name , file ) ;
                }
                
                _unregisterLoader( true ) ;
            }
            else
            {
                logger.warn( this + " complete failed, the loader reference not must be null.") ;
            }
            
            notifyFinished() ;
        }
        
        /**
         * @private
         */
        private function _error( event:ErrorEvent ):void
        {
            if ( !isLocked() )
            {
                error.emit( this , event.text ) ;
            }
            
            _unregisterLoader( true ) ;
            
            notifyFinished() ;
        }
        
        /**
         * @private
         */
        private function _httpResponseStatus( event:HTTPStatusEvent ):void
        {
            /*
            trace( "URLCache.onHTTPResponseStatus()" );
            trace( "status: " + event.status );
            trace( "response URL: " + event.responseURL );
            trace( "headers:");
            for( var i:uint = 0; i < event.responseHeaders.length; i++ )
            {
                trace( "    " + event.responseHeaders[i].name + " : " + event.responseHeaders[i].value );
            }
            */
        }
        
        /**
         * @private
         */
        private function _progress( event:ProgressEvent ):void
        {
            //warn( this + " progress url:" + url + " event:" + event ) ;
            if ( !isLocked() )
            {
                progressIt.emit( this , event.bytesLoaded , event.bytesTotal ) ;
            }
        }
        
        ///////////
        
        /**
         * @private
         */
        private function _registerLoader():void
        {
            if( !_loader )
            {
                _loader = new Loader() ;
            }
            
            var info:LoaderInfo = _loader.contentLoaderInfo ;
            if( info )
            {
                if( !info.hasEventListener( Event.COMPLETE ) ) 
                {
                    info.addEventListener( Event.COMPLETE  , _complete , false, 0 , true );
                }
                
                if( !info.hasEventListener( HTTPStatusEvent.HTTP_RESPONSE_STATUS ) ) 
                {
                    info.addEventListener( HTTPStatusEvent.HTTP_RESPONSE_STATUS , _httpResponseStatus , false, 0 , true );
                }
                
                if( !info.hasEventListener( IOErrorEvent.IO_ERROR ) ) 
                {
                    info.addEventListener( IOErrorEvent.IO_ERROR , _error , false, 0 , true );
                }
                
                if( !info.hasEventListener( ProgressEvent.PROGRESS ) ) 
                {
                    info.addEventListener( ProgressEvent.PROGRESS , _progress , false, 0 , true );
                }
                
                if( !info.hasEventListener( SecurityErrorEvent.SECURITY_ERROR ) ) 
                {
                    info.addEventListener( SecurityErrorEvent.SECURITY_ERROR  , _error , false, 0 , true );
                }
            }
        }
        
        /**
         * @private
         */
        private function _unregisterLoader( remove:Boolean = false ):void
        {
            if( _loader )
            {
                _loader.contentLoaderInfo.removeEventListener( Event.COMPLETE                       , _complete           );
                _loader.contentLoaderInfo.removeEventListener( HTTPStatusEvent.HTTP_RESPONSE_STATUS , _httpResponseStatus );
                _loader.contentLoaderInfo.removeEventListener( IOErrorEvent.IO_ERROR                , _error              );
                _loader.contentLoaderInfo.removeEventListener( ProgressEvent.PROGRESS               , _progress           );
                _loader.contentLoaderInfo.removeEventListener( SecurityErrorEvent.SECURITY_ERROR    , _error              ) ;
                
                try
                {
                    _loader.close() ;
                    _loader.unload() ;
                }
                catch( e:Error )
                {
                    //
                }
                
                if( remove )
                {
                    _loader = null ;
                }
            }
        }
    }
}
