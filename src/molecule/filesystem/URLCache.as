﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

package molecule.filesystem 
{
    import system.process.Task;
    import system.signals.Signal;

    import flash.events.ErrorEvent;
    import flash.events.Event;
    import flash.events.HTTPStatusEvent;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.filesystem.File;
    import flash.filesystem.FileMode;
    import flash.filesystem.FileStream;
    import flash.net.URLLoader;
    import flash.net.URLLoaderDataFormat;
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;
    import flash.utils.ByteArray;
    import flash.utils.Dictionary;
    
    /**
     * Load a file and try to caching it in a specific application storage directory.
     */
    public class URLCache extends Task
    {
        /**
         * Creates a new URLCache instance.
         * @param path The name of the path to cache the file.
         * @param root The optional root directory File reference to target, by default the loader save the files in the application storage directory.
         */
        public function URLCache( path:String = "" , url:String = null , name:String = null , root:File = null )
        {
            _path = path ;
            
            this.name = name ;
            this.url  = url ;
            
            if( root && root.isDirectory )
            {
                _rootDirectory = root ;
            }
        }
        
        /**
         * The signal invoked when a file is cached.
         */
        public const cached:Signal = new Signal() ;
        
        /**
         * The signal invoked when an error is emitted.
         */
        public const error:Signal = new Signal() ;
        
        /**
         * The collection of the URLRequestHeaders to send when the cache is loading.
         */
        public var headers:Array ;
        
        /**
         * The method of the request.
         */
        public var method:String = URLRequestMethod.GET ;
        
        /**
         * The optional name of the file.
         */
        public var name:String ;
        
        /**
         * Indicates the path name of the cache loader object.
         */
        public function get path():String
        {
            return _path ;
        }
        
        /**
         * The signal invoked when a file loading is in progress.
         */
        public const progress:Signal = new Signal() ;
        
        /**
         * The signal invoked when a file is ready.
         */
        public const ready:Signal = new Signal() ;
        
        /**
         * Indicates the root directory of the cache loader.
         */
        public function get rootDirectory():File
        {
            return _rootDirectory ;
        }
        
        /**
         * Indicates the storage directory of this cache object based on the current path of it.
         */
        public function get storageDirectory():File
        {
            return _rootDirectory.resolvePath( _path );
        }
        
        /**
         * The url to load the file to caching.
         */
        public var url:String ;
        
        /**
         * Clear all files in the current cache directory or a specific collection of files.
         * @param ...args The name of all the files to delete (optional).
         */
        public function clear( ...args:Array ):void
        {
            if( args.length > 0 )
            {
                for each( var name:String in args )
                {
                    var file:File = _rootDirectory.resolvePath( _path ).resolvePath( name ); 
                    if( file.exists )
                    {
                        file.deleteFile() ;
                    }
                }
            }
            else
            {
                storageDirectory.deleteDirectory( true );
            }
        }
        
        /**
         * Returns true if a file is caching with the specific name.
         * @return true if a file is caching with the specific name.
         */
        public function containsFile( name:String ):Boolean
        {
            try
            {
                return _rootDirectory.resolvePath( _path ).resolvePath( name ).exists ;
            }
            catch( e:Error )
            {
                //
            }
            return false ;
        }
        
        /**
         * Returns the specific File item if is caching with the specific name.
         * @return the specific File item if is caching with the specific name.
         */
        public function getFile( name:String ):File
        {
            return _rootDirectory.resolvePath( _path ).resolvePath( name );
        }
        
        /**
         * Run the process.
         */
        public override function run( ...args:Array ):void
        {
            notifyStarted();
            
            if( args.length > 0 )
            {
                url  = args[0] as String ;
                name = args[1] as String ;
            }
            
            var file:File = _rootDirectory.resolvePath( _path ).resolvePath( name ); 
            
            if( file && file.exists )
            {
                ready.emit( this, file , name ) ;
                notifyFinished() ;
            }
            
            var loader:URLLoader = _registerLoader( file , name ) ;
            
            var request:URLRequest = new URLRequest( url ) ;
            
            if( headers && headers.length > 0 )
            {
                request.requestHeaders = [].concat( headers ) ;
            }
            
            request.method = ( method && method.length > 0 ) ? method : URLRequestMethod.GET ;
            
            loader.load( request ) ;
        }
        
        /**
         * @private
         */
        private var _loaders:Dictionary = new Dictionary() ;
        
        /**
         * @private
         */
        private var _path:String ;
        
        /**
         * @private
         */
        private var _rootDirectory:File = File.applicationStorageDirectory ;
        
        /**
         * @private
         */
        private function _complete( event:Event ):void
        {
            var loader:URLLoader = event.target as URLLoader ;
            
            var info:Object = _loaders[ loader ] ;
            if( info )
            {
                var file:File      = info.file as File ;
                var name:String    = info.name as String ;
                var data:ByteArray = loader.data as ByteArray;
                
                /////////////
                
                var stream:FileStream = new FileStream();
                    stream.open( file, FileMode.WRITE );
                    stream.writeBytes( data );
                    stream.close();
                
                cached.emit( this, file , name ) ;
                
                /////////////
                
                ready.emit( this, file , name ) ;
            }
            
            _unregisterLoader( loader ) ;
            
            notifyFinished() ;
        }
        
        /**
         * @private
         */
        private function _error( event:ErrorEvent ):void
        {
            var loader:URLLoader = event.target as URLLoader ;
            if( loader )
            {
                _unregisterLoader( event.target as URLLoader ) ;
            }
            error.emit( this , event.text ) ;
            notifyFinished() ;
        }
        
        /**
         * @private
         */
        private function _httpResponseStatus( event:HTTPStatusEvent ):void
        {
            /*
            trace( "URLCache.onHTTPResponseStatus()" );
            trace( "status: " + event.status );
            trace( "response URL: " + event.responseURL );
            trace( "headers:");
            for( var i:uint = 0; i < event.responseHeaders.length; i++ )
            {
                trace( "    " + event.responseHeaders[i].name + " : " + event.responseHeaders[i].value );
            }
            */
        }
        
        /**
         * @private
         */
        private function _progress( event:ProgressEvent ):void
        {
            var loader:URLLoader = event.target as URLLoader ;
            var info:Object = _loaders[loader] ;
            if( info )
            {
                progress.emit( this , event.bytesLoaded , event.bytesTotal , info ) ;
            }
        }
        
        /**
         * @private
         */
        private function _registerLoader( file:File = null , name:String = null ):URLLoader
        {
            var loader:URLLoader = new URLLoader() ;
            
            loader.dataFormat = URLLoaderDataFormat.BINARY;
            
            loader.addEventListener( Event.COMPLETE                       , _complete           );
            loader.addEventListener( HTTPStatusEvent.HTTP_RESPONSE_STATUS , _httpResponseStatus );
            loader.addEventListener( IOErrorEvent.IO_ERROR                , _error              );
            loader.addEventListener( ProgressEvent.PROGRESS               , _progress           );
            
            _loaders[loader] = { file:file , name:name } ;
            
            return loader ;
        }
        
        /**
         * @private
         */
        private function _unregisterLoader( loader:URLLoader ):void
        {
            if( loader )
            {
                loader.removeEventListener( Event.COMPLETE                       , _complete           );
                loader.removeEventListener( HTTPStatusEvent.HTTP_RESPONSE_STATUS , _httpResponseStatus );
                loader.removeEventListener( IOErrorEvent.IO_ERROR                , _error              );
                loader.removeEventListener( ProgressEvent.PROGRESS               , _progress           );
                
                try
                {
                    loader.close() ;
                }
                catch( e:Error )
                {
                    //
                }
                
                loader.data = null ; 
                _loaders[loader] = undefined ;
            }
        }
    }
}
