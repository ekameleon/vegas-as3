﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule 
{
    import system.signals.Signal;
    import system.signals.Signaler;

    import flash.text.StyleSheet;
    
    /**
     * This class provides a skeletal implementation of the <code class="prettyprint">Style</code> interface, to minimize the effort required to implement this interface.
     */
    public dynamic class Stylus implements Style
    {
        /**
         * Creates a new Stylus instance.
         * @param init An object that contains properties with which to populate the newly Style object. If init is not an object, it is ignored.
        */
        public function Stylus( init:*=null )
        {
            _styleChanged = new Signal() ;
            _styleSheetChanged = new Signal() ;
            initialize() ;
            map( init ) ;
        }
        
        /**
         * Emits a message when the style in changed.
         */
        public function get styleChanged():Signaler
        {
            return _styleChanged ;
        }
        
        /**
         * Indicates the style sheet reference of this object.
         */
        public function get styleSheet():StyleSheet
        {
            return _styleSheet ;
        }
        
        /**
         * Emits a message when the styleSheet in changed property.
         */
        public function get styleSheetChanged():Signaler
        {
            return _styleSheetChanged ;
        }
        
        /**
         * @private
         */
        public function set styleSheet( styleSheet:StyleSheet ):void
        {
            _styleSheet = styleSheet ;
            _styleSheetChanged.emit( this ) ;
        }
        
        /**
         * Returns the value of the specified property if it's exist in the object, else returns null.
         * @return the value of the specified property if it's exist in the object or <code class="prettyprint">null</code>.
         */
        public function getStyle( prop:String ):*
        {
            if ( prop in this )
            {
                return this[prop] ;
            }
            else
            {
                return null ;
            }
        }
        
        /**
         * Invoked in the constructor of the <code class="prettyprint">IStyle</code> instance.
         */
        public function initialize():void
        {
            // overrides this method.
        }
        
        /**
         * Copy all properties in the specified passed-in object in the internal style.
         */
        public function map( init:Object ):void
        {
            for( var member:String in init )
            {
                this[member] = init[member] ;
            }
            _styleChanged.emit( this ) ;
        }
        
        /**
         * This method is invoked to change a style attribute in this IStyle object with a generic object or a key(String)/value pair of arguments.
         */
        public function setStyle( ...args:Array ):void
        {
            if ( args.length == 0 ) 
            {
                return ;
            } 
            if ( args.length == 2 && args[0] is String ) 
            {
                if ( args[0] in this ) 
                {
                    this[ args[0] ] = args[1] ;
                    _styleChanged.emit( this ) ;
                }
            }
            else if ( args.length == 1 && args[0] is Object ) 
            {
                map( args[0] as Object ) ;
            }
        }
        
        /**
         * @private
         */
        protected var _styleChanged:Signaler ; 
        
        /**
         * @private
         */
        protected var _styleSheet:StyleSheet ;
        
        /**
         * @private
         */
        protected var _styleSheetChanged:Signaler ;
    }
}
