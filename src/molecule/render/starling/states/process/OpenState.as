﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.states.process 
{
    import molecule.Screen;
    import molecule.logger;
    import molecule.render.starling.process.display.AddChild;
    import molecule.states.State;
    import molecule.states.StateTask;
    
    import starling.display.DisplayObject;
    import starling.display.DisplayObjectContainer;
    
    import system.ioc.ObjectFactory;
    import system.process.Action;
    import system.process.BindTask;
    import system.process.Chain;
    
    /**
     * This process open a state.
     */
    public class OpenState extends StateTask 
    {
        /**
         * Creates a new OpenState instance.
         * @param state The State reference of this process.
         * @param factory The IoC factory reference.
         * @param auto Indicates if the screen of the current state is automatically attached when the state is opened (default true).
         * @param verbose Indicates the verbose mode of the task.
         */ 
        public function OpenState( state:State = null , factory:ObjectFactory = null , auto:Boolean = true  , verbose:Boolean = true )
        {
            super( state , factory , verbose );
            this.auto = auto ;
            _chain = new Chain() ;
            _chain.mode = Chain.TRANSIENT ;
            _chain.finishIt.connect( finish ) ;
        }
        
        /**
         * Indicates if the screen of the current state is automatically attached when the state is opened (default true).
         */
        public var auto:Boolean = true ;

        /**
         * Run the process.
         */
        public override function run( ...arguments:Array ):void 
        {
            logger.debug(this + " run : " + state ) ;
            
            notifyStarted() ;
            
            if ( state == null )
            {
                logger.warn( this + " failed, the State of this process not must be 'null'." ) ;
                notifyFinished() ;
                return ;
            }
            
            ///////////////// owner
            
            var owner:Screen ;
            
            if ( state.owner )
            {
                if( state.owner is Screen )
                {
                    owner = state.owner as Screen ;
                }
                else if ( state.owner is String && factory )
                {
                    owner = factory.getObject( state.owner ) as Screen ;
                }
            }
            
            if ( !owner )
            {
                logger.warn(this + " failed, the State of the current state of this process not must be 'null'.") ;
                notifyFinished() ;
                return ;
            }
            
            ///////////////// container
            
            var container:DisplayObjectContainer = owner.container ;
            if ( !container )
            {
                logger.warn(this + " failed, no exist a state container in the application.") ;
                notifyFinished() ;
                return ;
            }
            
            ///////////////// screen
            
            var screen:Screen ;
            
            if ( state.screen is Screen )
            {
                screen = state.screen as Screen ;
            }
            else if ( state.screen is String && factory && factory.containsObjectDefinition( state.screen ) )
            {
                screen = factory.getObject( state.screen ) as Screen ;
            }
            else
            {
                logger.warn(this + " failed, the screen of the state:" + state + " isn't register in the display factory of the application with the screen id : " + state.screen  ) ;
            }
            
            if ( screen )
            {
                if ( screen.openBefore )
                {
                    _chain.addAction( screen.openBefore ) ;
                }
                
                if( auto )
                {
                    _chain.addAction( new AddChild( container , screen as DisplayObject ) ) ;
                }

                _chain.addAction( new BindTask( screen.open , screen ) ) ;
                
                if ( screen.openAfter )
                {
                    _chain.addAction( screen.openAfter ) ;
                }
            }
            else
            {
                logger.warn( this + " failed, no Screen reference find in the State with the id : " + state.id ) ;
            }
            
            if ( ( _chain.length > 0 ) && !_chain.running )
            {
                _chain.run() ;
            }
            else
            {
                notifyFinished() ;
            }
        }
        
        /**
         * @private
         */   
        protected function finish( action:Action = null ):void
        {
            notifyFinished() ;
        }
        
        /**
         * @private
         */
        private var _chain:Chain ;
    }
}
