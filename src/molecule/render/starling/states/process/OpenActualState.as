﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling.states.process 
{
    import molecule.logger;
    import molecule.states.State;

    import system.data.ValueObject;
    import system.ioc.ObjectFactory;
    import system.models.maps.InitMapModel;
    import system.models.maps.MapModel;
    import system.process.Action;
    import system.process.Task;
    
    /**
     * Open the current state display in the specified state model.
     */
    public class OpenActualState extends Task 
    {
        /**
         * Creates a new OpenActualState instance.
         * @param model The state model reference.
         * @param init An optional action to initialize the state.
         * @param factory The optional IoC factory reference.
         */
        public function OpenActualState( model:MapModel = null , init:InitMapModel = null , factory:ObjectFactory = null )
        {
            this.factory = factory ;
            this.model   = model ;
            this.init    = init  ;
        }
        
        /**
         * The optional factory reference.
         */
        public var factory:ObjectFactory ;
        
        /**
         * An optional action to initialize the state.
         */
        public var init:InitMapModel ;
        
        /**
         * The state model reference.
         */
        public var model:MapModel ;
        
        /**
         * Run the process.
         */
        public override function run( ...arguments:Array ):void 
        {
            notifyStarted() ;
            if ( init )
            {
                var menu:MapModel = init.model ;
                if ( menu == null )
                {
                    logger.warn(this + " failed, the menu model reference not must be null.") ; 
                    return ;
                }
                var first:* = init.first  ;
                if ( first )
                {
                    if ( first is ValueObject )
                    {
                        menu.current = first ;
                    }
                    else if ( menu.containsKey( first ) )
                    {
                        menu.current = menu.get( first ) ;
                    }
                }
                else
                {
                    logger.warn(this + " failed, the first value is null.") ; 
                }
            }
            else if ( model )
            {
                var current:State = model.current as State ;
                if ( current )
                {
                    var open:OpenState = new OpenState( current , factory ) ;
                    open.finishIt.connect( finish , 0 , true ) ; 
                    open.run() ;
                    return ;
                }
                else
                {
                    logger.warn(this + " failed, the state model isnt selected with a current State reference.") ;
                }
            }
            init = null ; // destroy the init reference
            notifyFinished() ;
        }
        
        /**
         * Invoked when an Action finish this process.
         */
        protected function finish( action:Action = null ):void
        {
            notifyFinished() ;
        }
    }
}
