﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling.textures
{
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.Sprite;
    
    /**
     * A basic texture item representation.
     */
    public class TextureItem extends Sprite
    {
        /**
         * Creates a new TextureItem instance.
         * @param graphic The graphic representation of the texture.
         * @param textureName The texture name.
         * @param frameName The name component of the frame.
         * @param frameX The x component of the frame (default 0).
         * @param frameY The y component of the frame (default 0).
         * @param frameWidth The width component of the frame (default 0).
         * @param frameHeight The height component of the frame (default 0).
         */
        public function TextureItem( graphic:BitmapData, textureName:String, frameName:String, frameX:int = 0, frameY:int = 0, frameWidth:int = 0, frameHeight:int = 0 )
        {
            super();
            
            this.graphic = graphic;
            this.textureName = textureName;
            this.frameName = frameName;
            
            this.frameWidth = frameWidth;
            this.frameHeight = frameHeight;
            this.frameX = frameX;
            this.frameY = frameY;
            
            addChild( new Bitmap( graphic , "auto" , false ) );
        }
        
        /**
         * The height component of the frame.
         */
        public var frameHeight:int = 0;
        
        /**
         * The name component of the frame.
         */
        public var frameName:String = "";
        
        /**
         * The width component of the frame.
         */
        public var frameWidth:int = 0;
        
        /**
         * The x component of the frame.
         */
        public var frameX:int = 0;
        
        /**
         * The y component of the frame.
         */
        public var frameY:int = 0;
        
        /**
         * The graphic representation of the texture.
         */
        public var graphic:BitmapData;
        
        /**
         * The texture name.
         */
        public var textureName:String = "";
    }
}
