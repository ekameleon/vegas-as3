﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling.textures
{
    import core.maths.replaceNaN;

    import graphics.geom.EdgeMetrics;

    import starling.textures.Texture;

    import flash.geom.Rectangle;
    
    /**
     * A set of 3 textures.
     */
    public final class HScale3Texture
    {
        /**
         * Creates a new HScale3Texture instance.
         * @param texture The texture to cut in tree areas horizontally.
         * @param margin The EdgeMetrics reference to defines the top and bottom sizes.
         */
        public function HScale3Texture( texture:Texture, margin:EdgeMetrics )
        {
            _texture   = texture ;
            _margin    = margin ;
            initialize() ;
        }
        
        /**
         * The texture for the second region.
         */
        public function get center():Texture
        {
            return _center;
        }
        
        /**
         * The texture for the first region.
         */
        public function get left():Texture
        {
            return _left;
        }
        
        
        /**
         * The texture for the first region.
         */
        public function get margin():EdgeMetrics
        {
            return _margin;
        }
        
        /**
         * The size of the first region, in pixels.
         */
        public function set margin( em:EdgeMetrics ):void
        {
            _margin.left   = replaceNaN( em.left   ) ;
            _margin.top    = replaceNaN( em.top    ) ;
            _margin.right  = replaceNaN( em.right  ) ;
            _margin.bottom = replaceNaN( em.bottom ) ;
        }
        
        /**
         * The texture for the third region.
         */
        public function get right():Texture
        {
            return _right;
        }
        
        /**
         * The original texture.
         */
        public function get texture():Texture
        {
            return _texture;
        }
        
        /**
         * @private
         */
        private var _center:Texture;
        
        /**
         * @private
         */
        private var _left:Texture;
        
        /**
         * @private
         */
        private var _margin:EdgeMetrics = new EdgeMetrics() ;
        
        /**
         * @private
         */
        private var _right:Texture;
        
        /**
         * @private
         */
        private var _texture:Texture;
        
        /**
         * @private
         */
        private function initialize():void
        {
            const frame:Rectangle = _texture.frame ;
            
            var region:Rectangle = new Rectangle() ;
            var area:Rectangle  = new Rectangle() ;
            
            var hasFrame:Boolean ;
            
            const middle:Number = frame.width - _margin.left - _margin.right ; 
            
            
            const left:Number   = _margin.left + frame.x ;
            const right:Number  = _margin.right - (frame.width - texture.width) - frame.x;
            
            var hasTopFrame:Boolean    = frame.y != 0;
            var hasRightFrame:Boolean  = right != _margin.right ;
            var hasBottomFrame:Boolean = (frame.height - frame.y) != texture.height;
            var hasLeftFrame:Boolean   = left != _margin.left ;
            
            // first
            
            hasFrame = (hasLeftFrame || hasTopFrame || hasBottomFrame)  ;
            
            region.x      = 0 ; 
            region.y      = 0 ; 
            region.width  = left ; 
            region.height = _texture.height ;
            
            if( hasFrame )
            {
                area.x      = frame.x ;
                area.y      = frame.y ; 
                area.width  = _margin.left ;
                area.height = frame.height ;
            }
            
            _left = Texture.fromTexture( texture, region , hasFrame ? area : null );
            
            // second
            
            hasFrame = (hasTopFrame || hasBottomFrame)  ;
            
            region.x      = left ; 
            region.y      = 0 ; 
            region.width  = middle ; 
            region.height = _texture.height ;
            
            if( hasFrame )
            {
                area.x      = 0 ;
                area.y      = frame.y ; 
                area.width  = middle ;
                area.height = frame.height ;
            }
            
            _center = Texture.fromTexture( texture, region , hasFrame ? area : null ) ;
            
            // thirst
            
            hasFrame = (hasTopFrame || hasBottomFrame || hasRightFrame) ;
            
            region.x      = left + middle ; 
            region.y      = 0 ; 
            region.width  = right ; 
            region.height = _texture.height ;
            
            if( hasFrame )
            {
                area.x      = 0 ;
                area.y      = frame.y ; 
                area.width  = right ;
                area.height = frame.height ;
            }
            
            _right = Texture.fromTexture( texture, region , hasFrame ? area : null ) ;
        }
    }
}
