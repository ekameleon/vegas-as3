﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling.display
{
    import core.maths.clamp;

    import starling.display.DisplayObject;
    import starling.display.Stage;
    import starling.events.Event;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;
    
    import flash.geom.Point;
    import flash.geom.Rectangle;
    
    /**
     * The internal helper class to enable a startDrag and a stopDrag method with a starling display object. 
     * You must use the public "dragger" singleton to register a display and drag it.
     */
    internal class DragDisplay
    {
        /**
         * Creates a new DragDisplay instance.
         */
        public function DragDisplay()
        {
            //
        }
        
        /**
         * The display object to drag.
         */
        public function get target():DisplayObject
        {
            return _target ;
        }
        
        /**
         * @private
         */
        public function set target( sprite:DisplayObject ):void
        {
            if( _target )
            {
                stopDrag();
            }
            _target = sprite ;
        }
        
        /**
         * Lets the user drag the specified display. The display remains draggable until explicitly stopped through a call to the 
         * stopDrag() method, or until another display is made draggable. Only one display is draggable at a time.
         * @param lockCenter Specifies whether the draggable display is locked to the center of the pointer position (true), or locked to the point where the user first clicked the display (false).
         * @param bounds Value relative to the coordinates of the display's parent that specify a constraint rectangle for the Sprite.
         */
        public function startDrag( lockCenter:Boolean=false , bounds:Rectangle = null ):void
        {
            _lockCenter = lockCenter;
            _bounds     = bounds ;
            _position.x = 0 ;
            _position.y = 0 ;
            if( _target )
            {
                if( _target.stage ) 
                {
                    _target.stage.addEventListener( TouchEvent.TOUCH , touch );
                    _target.addEventListener( Event.REMOVED_FROM_STAGE , removedFromStage ) ;
                }
            }
        }
        
        /**
         * Ends the startDrag() method. A display that was made draggable with the startDrag() method remains draggable until 
         * a stopDrag() method is added, or until another display becomes draggable. Only one sprite is draggable at a time.
         */
        public function stopDrag():void
        {
            if( _target )
            {
                if( _target.hasEventListener( Event.REMOVED_FROM_STAGE ) )
                {
                    _target.removeEventListener( Event.REMOVED_FROM_STAGE , removedFromStage );
                }
                _target.stage.removeEventListener( TouchEvent.TOUCH , touch );
            }
        }
        
        /**
         * @private
         */
        private var _bounds:Rectangle;
        
        /**
         * @private
         */
        private var _lockCenter:Boolean;
        
        /**
         * @private
         */
        private var _position:Point = new Point();
        
        /**
         * @private
         */
        private var _target:DisplayObject;
        
        /**
         * @private
         */
        private function removedFromStage( e:Event = null ):void
        {
            stopDrag();
        }
        
        /**
         * @private
         */
        private function touch( e:TouchEvent ):void
        {
            var stage:Stage          = _target.stage ;
            var parent:DisplayObject = _target.parent ;
            var touch:Touch          = e.getTouch( stage ) ;
            
            if( touch.phase != TouchPhase.MOVED && touch.phase != TouchPhase.HOVER )
            {
                return;
            }
            
            var gm:Point = touch.getLocation( stage ) ;
            var cm:Point = parent.globalToLocal( gm ) ;
            
            if( !_lockCenter )
            {
                _position   = _target.globalToLocal( gm ) ;
                _lockCenter = true;
            }
            
            cm = cm.subtract( _position ) ;
            
            _target.x = cm.x ;
            _target.y = cm.y ;
            
            if( _bounds ) 
            {
                _target.x = clamp( _target.x , _bounds.x , _bounds.right ) ;
                _target.y = clamp( _target.y , _bounds.y , _bounds.bottom ) ;
            }
        }
    }
}
