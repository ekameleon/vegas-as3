﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1

  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/

  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.

  The Original Code is [maashaack framework].

  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.

  Contributor(s):

  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.display
{
    import core.maths.clamp;
    import core.maths.degreesToRadians;

    import graphics.Direction;
    import graphics.FillGradientStyle;
    import graphics.IFillStyle;
    import graphics.ILineStyle;
    import graphics.drawing.RectanglePen;

    import molecule.render.starling.display.Element;

    import starling.display.Image;
    import starling.display.Mesh;
    import starling.events.Event;
    import starling.events.ResizeEvent;
    import starling.rendering.Painter;
    import starling.textures.Texture;

    import system.hack;

    import flash.display.BitmapData;
    import flash.display.Shape;
    import flash.geom.Matrix;
    import flash.geom.Point;

    use namespace hack;

    /**
     * This display is used to create a background in your application or in an other display of the application.
     * <p><b>Example :</b></p>
     * <pre class="prettyprint">
     * import flash.display.GradientType ;
     * import flash.display.StageAlign ;
     * import flash.display.StageScaleMode ;
     *
     * import graphics.Direction ;
     * import graphics.FillStyle ;
     * import graphics.FillGradientStyle ;
     * import graphics.LineStyle ;
     *
     * import molecule.render.starling.display.Background ;
     *
     * stage.align     = StageAlign.TOP_LEFT ;
     * stage.scaleMode = StageScaleMode.NO_SCALE ;
     *
     * var area:Background = new Background() ;
     *
     * area.lock() ; // lock the update method
     *
     * area.fill = new FillStyle( 0xD97BD0  ) ;
     * area.line = new LineStyle( 2, 0xFFFFFF ) ;
     *
     * area.w    = 400 ;
     * area.h    = 300 ;
     *
     * area.unlock() ; // unlock the update method
     *
     * area.update() ; // force update
     *
     * addChild( area ) ;
     *
     * /////////////////////////////////
     *
     * function keyDown( e:KeyboardEvent ):void
     * {
     *     var code:uint = e.keyCode ;
     *     switch( code )
     *     {
     *         case Keyboard.SPACE :
     *         {
     *             if( area.fullscreen )
     *             {
     *                 area.autoSize   = false ;
     *                 area.fill       = new FillStyle( 0xD97BD0 ) ;
     *                 area.fullscreen = false ;
     *             }
     *             else
     *             {
     *                 area.autoSize         = true ;
     *                 area.gradientRotation = 90 ;
     *                 area.useGradientBox   = true ;
     *                 area.fill             = new FillGradientStyle( GradientType.LINEAR, [0x071E2C,0x81C2ED], [1,1], [0,255] ) ;
     *                 area.fullscreen       = true ;
     *                 area.direction        = null ;
     *             }
     *             break ;
     *         }
     *         case Keyboard.UP :
     *         {
     *             area.autoSize   = true ;
     *             area.fill       = new FillStyle( 0x000000 ) ;
     *             area.fullscreen = true ;
     *             area.direction  = Direction.HORIZONTAL ;
     *             break ;
     *         }
     *         case Keyboard.DOWN :
     *         {
     *             area.autoSize   = true ;
     *             area.fill       = new FillStyle( 0xFFFFFF ) ;
     *             area.fullscreen = true ;
     *             area.direction  = Direction.VERTICAL ;
     *             break ;
     *         }
     *     }
     * }
     *
     * stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
     * </pre>
     */
    public class Background extends Element
    {
        use namespace hack ;

        /**
         * Creates a new Background instance.
         * @param init An object that contains properties with which to populate the newly instance. If init is not an object, it is ignored.
         * @param pen An optional RectanglePen reference to use to draw the background area.
         * @param locked An optional boolean to defines if the constructor must be locked, the update method is not invoked in the constructor if this flag is true.
         */
        public function Background( init:Object = null , pen:RectanglePen = null , locked:Boolean = false )
        {
            initializePen( pen ) ;
            super( init , locked ) ;
            addEventListener( Event.ADDED_TO_STAGE , addedToStageResize ) ;
        }

        /**
         * Indicates if the background is resizing when the stage resize event is invoked.
         */
        public function get autoSize():Boolean
        {
            return _autoSize ;
        }

        /**
         * @private
         */
        public function set autoSize( b:Boolean ):void
        {
            if( _autoSize == b )
            {
                return ;
            }

            if( stage && stage.hasEventListener( ResizeEvent.RESIZE ) )
            {
                stage.removeEventListener( ResizeEvent.RESIZE , resize ) ;
            }
            _autoSize = b ;
            if ( stage )
            {
                if ( _autoSize )
                {
                    stage.addEventListener( ResizeEvent.RESIZE , resize ) ;
                    resize() ;
                }
            }
        }


        /**
         * Determinates the IFillStyle reference of this display.
         */
        public function get fill():IFillStyle
        {
            return _fillStyle ;
        }

        /**
         * @private
         */
        public function set fill( style:IFillStyle ):void
        {
            _fillStyle = style ;
            if( _pen )
            {
                _pen.fill = _fillStyle ;
            }
            if ( _locked == 0 )
            {
                update() ;
            }
        }

        /**
         * Indicates if the canvas is in the fullscreen mode (use Stage.stageWidth and Stage.stageHeight to resize).
         */
        public function get fullscreen():Boolean
        {
            return _fullscreen ;
        }

        /**
         * @private
         */
        public function set fullscreen( b:Boolean ):void
        {
            _fullscreen = b ;
            if ( _locked == 0 )
            {
                update() ;
            }
        }

        /**
         * The matrix value to draw the gradient fill. This property override the gradientRotation and gradientTranslation properties.
         */
        public var gradientMatrix:Matrix ;

        /**
         * The rotation value to draw the gradient fill.
         */
        public var gradientRotation:Number = 0 ;

        /**
         * The translation vector to draw the gradient fill.
         */
        public var gradientTranslation:Point ;

        /**
         * Determinates the virtual height value of this component.
         */
        public override function get h():Number
        {
            var value:Number = ( _fullscreen && (stage != null) && (_direction != Direction.HORIZONTAL) ) ? stage.stageHeight : _h ;
            return clamp( value , _minHeight, _maxHeight) ;
        }

        /**
         * Determinates the <code class="prettyprint">ILineStyle</code> reference of this display.
         */
        public function get line():ILineStyle
        {
            return _lineStyle ;
        }

        /**
         * @private
         */
        public function set line( style:ILineStyle ):void
        {
            _lineStyle = style ;
            if( _pen )
            {
                _pen.line = style ;
            }
            if ( _locked == 0 )
            {
                update() ;
            }
        }

        /**
         * The internal RectanglePen reference of this background display to draw inside.
         * <p><b>Example :</b></p>
         * <pre class="prettyprint">
         * import graphics.Border ;
         * import graphics.FillStyle ;
         * import graphics.LineStyle ;
         *
         * import molecule.render.starling.display.Background ;
         * import graphics.drawing.DashRectanglePen ;
         * import graphics.geom.EdgeMetrics ;
         *
         * import flash.display.StageAlign ;
         * import flash.display.StageScaleMode ;
         *
         * stage.align     = StageAlign.TOP_LEFT ;
         * stage.scaleMode = StageScaleMode.NO_SCALE ;
         *
         * var pen:DashRectanglePen = new DashRectanglePen() ;
         *
         * pen.length  = 8 ;
         * pen.spacing = 6 ;
         *
         * pen.overage = new EdgeMetrics( 8 , 8 , 8 , 8 ) ;
         * pen.border  = new Border( Border.LEFT | Border.TOP | Border.RIGHT ) ;
         *
         * var area:Background = new Background() ;
         *
         * area.lock() ; // lock the update method
         *
         * area.fill = new FillStyle( 0xD97BD0  ) ;
         * area.line = new LineStyle( 2, 0xFFFFFF ) ;
         *
         * area.x = 25 ;
         * area.y = 25 ;
         * area.w = 400 ;
         * area.h = 300 ;
         *
         * area.pen = pen ;
         *
         * area.unlock() ; // unlock the update method
         *
         * area.update() ; // force update
         *
         * addChild( area ) ;
         * </pre>
         */
        public function get pen():RectanglePen
        {
            return _pen ;
        }

        /**
         * @private
         */
        public function set pen( pen:RectanglePen ):void
        {
            initializePen( pen ) ;
        }

        /**
         * Indicates if the IFillStyle of this display use gradient box matrix (only if the IFillStyle is a FillGradientStyle).
         */
        public var useGradientBox:Boolean ;

        /**
         * Determinates the virtual height value of this component.
         */
        public override function get w():Number
        {
            var value:Number = ( _fullscreen && (stage != null) && (_direction != Direction.VERTICAL) ) ? stage.stageWidth : _w ;
            return clamp( value , _minWidth, _maxWidth ) ;
        }

        /**
         * Draw the display.
         */
        public override function draw( ...args:Array ):void
        {
            if( _bitmapData )
            {
                _bitmapData.dispose() ;
                _bitmapData = null ;
            }

            if( _image )
            {
                _image.dispose() ;
                _image = null ;
            }

            // align

            fixArea() ;

            // gradient

            if ( _fillStyle is FillGradientStyle )
            {
                var matrix:Matrix ;

                if( gradientMatrix )
                {
                    matrix = gradientMatrix ;
                }
                else
                {
                    matrix = new Matrix() ;
                    if( useGradientBox )
                    {
                        matrix.createGradientBox( _real.width, _real.height );
                    }
                    if ( !isNaN( gradientRotation ) )
                    {
                        matrix.rotate( degreesToRadians( gradientRotation ) ) ;
                    }
                    if ( gradientTranslation != null )
                    {
                        matrix.translate( gradientTranslation.x , gradientTranslation.y ) ;
                    }
                }

                ( _fillStyle as FillGradientStyle ).matrix = matrix ;
            }

            // draw
            if( (_fillStyle || _lineStyle) && _pen )
            {
                _pen.draw( 0 , 0 , _real.width , _real.height ) ;
                if( _shape && (_real.width > 0 && _real.height > 0 ))
                {
                    _bitmapData = new BitmapData( _real.width , _real.height , true , 0x00FFFFFF ) ;
                    _bitmapData.draw( _shape ) ;
                    _image = new Image( Texture.fromBitmapData( _bitmapData ) ) ;
                    _image.x = _real.x ;
                    _image.y = _real.y ;
                }
            }
        }

        /**
         * @inheritDoc
         */
        public override function render( painter:Painter ):void
        {
            if( _image )
            {
                painter.batchMesh( _image ) ;
            }

            super.render( painter ) ;
        }

        //////////

        /**
         * Invoked when the display is removed from the stage to enable the autoSize mode.
         */
        protected function addedToStageResize( e:Event = null ):void
        {
            removeEventListener( Event.ADDED_TO_STAGE , addedToStageResize ) ;
            addEventListener( Event.REMOVED_FROM_STAGE , removedFromStageResize ) ;
            if ( stage && _autoSize )
            {
                stage.addEventListener( ResizeEvent.RESIZE , resize ) ;
                resize() ;
            }
        }

        /**
         * Invoked when the display is removed from the stage to disable the autoSize mode.
         */
        protected function removedFromStageResize( e:Event = null ):void
        {
            removeEventListener( Event.REMOVED_FROM_STAGE , removedFromStageResize ) ;
            addEventListener( Event.ADDED_TO_STAGE , addedToStageResize ) ;
            if ( stage && _autoSize )
            {
                stage.removeEventListener( ResizeEvent.RESIZE , resize ) ;
            }
        }

        //////////

        /**
         * @private
         */
        hack var _autoSize:Boolean ;

        /**
         * @private
         */
        hack var _bitmapData:BitmapData ;

        /**
         * @private
         */
        hack var _fillStyle:IFillStyle ;

        /**
         * @private
         */
        hack var _fullscreen:Boolean ;

        /**
         * @private
         */
        hack var _image:Mesh ;

        /**
         * @private
         */
        hack var _lineStyle:ILineStyle ;

        /**
         * @private
         */
        hack var _pen:RectanglePen ;

        /**
         * @private
         */
        hack var _shape:Shape = new Shape() ;

        /**
         * Invoked in the constructor to initialize the RectanglePen reference in the background.
         */
        hack function initializePen( pen:RectanglePen = null ):RectanglePen
        {
            /*FDT_IGNORE*/
            _pen = pen || new RectanglePen() ;
            /*FDT_IGNORE*/
            _pen.graphics = _shape.graphics ;
            _pen.fill = _fillStyle ;
            _pen.line = _lineStyle ;
            return _pen ;
        }

        /**
         * Resize and update the background.
         */
        hack function resize( e:ResizeEvent = null ):void
        {
            update() ;
            notifyResized() ;
        }
    }
}