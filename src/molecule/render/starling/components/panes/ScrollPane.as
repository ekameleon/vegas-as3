﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.components.panes
{
    import molecule.Builder;
    import molecule.Style;
    import molecule.render.starling.display.Element;
    
    import starling.display.DisplayObject;
    
    import system.hack;
    import system.signals.Signal;
    
    import flash.geom.Point;
    
    use namespace hack ;
    
    /**
     * The scrollpane component.
     */
    public class ScrollPane extends Element
    {
        /**
         * Creates a new ScrollPane instance.
         * @param init An object that contains properties with which to populate the newly instance. If init is not an object, it is ignored.
         * @param locked The flag to lock the new current object when is created.
         */
        public function ScrollPane( init:Object = null , locked:Boolean = false )
        {
            _manager = new ScrollPaneManager( this ) ;
            super( init , locked ) ;
        }
        
        /**
         * Determinates the ScrollPane content as a DisplayObject.
         */
        public function get content():DisplayObject
        {
            return _content ;
        }
        
        /**
         * @private
         */
        public function set content( display:DisplayObject ):void
        {
            _content = (builder as ScrollPaneBuilder).initializeContent( display ) ;
            if( _locked == 0 )
            {
                update() ;
            }
        }
        
        /**
         * The maximum value of horizontal scroll position.
         */
        public function get maxScrollH():Number
        {
            if( _content )
            {
                return Math.max( _content.getBounds(null).width * _content.scaleX + (_style as ScrollPaneStyle).padding.horizontal - w , 0 ) ;
            }
            else
            {
                return 0 ;
            }
        }
        
        /**
         * The maximum value of vertical scroll position.
         */
        public function get maxScrollV():Number
        {
            if( _content )
            {
                return Math.max( _content.getBounds(null).height * _content.scaleY + (_style as ScrollPaneStyle).padding.vertical - h , 0 ) ;
            }
            else
            {
                return 0 ;
            }
        }
        
        /**
         * Indicates the position of the content item given a pixel value.
         */
        public function get position():Point
        {
            return scroller ;
        }
        
        /**
         * @private
         */
        public function set position( position:Point ):void
        {
            scroller.x = position.x ;
            scroller.y = position.y ;
            (builder as ScrollPaneBuilder).scroll() ;
        }
        
        /**
         * Emits when scrolling occurs.
         */
        public function get scroll():Signal
        {
            return _scroll ;
        }
        
        /**
         * Emits when scrolling is finished.
         */
        public function get scrollFinished():Signal
        {
            return _scrollFinished ;
        }
        
        /**
         * The current horizontal scrolling position. Sets the x position of the content item given a pixel value.
         */
        public function get scrollH():Number
        {
            return scroller.x ;
        }
        
        /**
         * @private
         */
        public function set scrollH( value:Number ):void
        {
            scroller.x = value ;
            if( _locked == 0 )
            {
                (builder as ScrollPaneBuilder).scroll() ;
            }
        }
        
        /**
         * Emits when scrolling begins.
         */
        public function get scrollStarted():Signal
        {
            return _scrollStarted ;
        }
        
        /**
         * The current vertical scrolling position. Sets the y position of the content item given a pixel value.
         */
        public function get scrollV():Number
        {
            return scroller.y ;
        }
        
        /**
         * @private
         */
        public function set scrollV( value:Number ):void
        {
            scroller.y = value ;
            if( _locked == 0 )
            {
                (builder as ScrollPaneBuilder).scroll() ;
            }
        }
        
        /**
         * Indicates if the component is touching.
         */
        public function get touching():Boolean
        {
            return _manager.touching ;
        }
        
        /**
         * Dispose the component.
         */
        public override function dispose():void
        {
            lock() ;
            if( content )
            {
                content = null ;
            }
            if( _builder )
            {
                _builder.clear() ;
            }
            unlock() ;
            super.dispose() ;
        }
        
        /**
         * Notify a message when the scroll is changing.
         */
        public function notifyScroll():void
        {
            if( _locked == 0 && _scroll.connected() )
            {
                _scroll.emit( this ) ;
            }
        }
        
        /**
         * Notify a message when the scroll is finished.
         */
        public function notifyScrollFinished():void
        {
            if( _locked == 0 && _scrollFinished.connected() )
            {
                _scrollFinished.emit( this ) ;
            }
        }
        
        /**
         * Notify a message when the scroll is started.
         */
        public function notifyScrollStarted():void
        {
            if( _locked == 0 && _scrollStarted.connected() )
            {
                _scrollStarted.emit( this ) ;
            }
        }
        
        /**
         * Sets the x and y position of the component.
         */
        public function setPosition( x:Number, y:Number ):void
        {
            scroller.x = x ;
            scroller.y = y ;
            if( _locked == 0 )
            {
                (builder as ScrollPaneBuilder).scroll() ;
            }
        }
        
        /**
         * @private
         */
        hack var _content:DisplayObject ;
        
        /**
         * @private
         */
        hack var _manager:ScrollPaneManager ;
        
        /**
         * @private
         */
        hack const _scroll:Signal = new Signal() ;
        
        /**
         * The internal scroll position of the scrollpane.
         * @private
         */
        hack const scroller:Point = new Point() ;
        
        /**
         * @private
         */
        hack const _scrollFinished:Signal = new Signal() ;
        
        /**
         * @private
         */
        hack const _scrollStarted:Signal = new Signal() ;
        
        /**
         * Returns the Builder constructor use to initialize this component.
         * @return the Builder constructor use to initialize this component.
         */
        protected override function getBuilderRenderer():Builder 
        {
            return new ScrollPaneBuilder( this ) ;
        }
        
        /**
         * Returns the Style constructor use to initialize this component.
         * @return the Style constructor use to initialize this component.
         */
        protected override function getStyleRenderer():Style 
        {
            return new ScrollPaneStyle() ;
        }
    }
}
