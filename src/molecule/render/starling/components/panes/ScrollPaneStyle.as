﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.components.panes
{
    import graphics.Direction;
    import graphics.Position;
    import graphics.easings.expoOut;
    import graphics.geom.EdgeMetrics;
    
    import molecule.ScrollPolicy;
    import molecule.Style;
    import molecule.Stylus;
    
    /**
     * The style of the scrollpane component.
     */
    public class ScrollPaneStyle extends Stylus
    {
        /**
         * Creates a new ScrollPaneStyle instance.
         * @param init An object that contains properties with which to populate the newly Style object. If init is not an object, it is ignored.
         */
        public function ScrollPaneStyle( init:* = null )
        {
            super( init ) ;
        }
        
        /**
         * The horizontal strength ratio (default 1).
         */
        public var horizontalStrength:Number = 1 ;
        
        /**
         * Indicates the x or y postion of the horizontal scrollbar, in pixels.
         */
        public var hScrollBarOffset:int = 4 ;
        
        /**
         * Whether the horizontal scrollbar should appear on the left hand side of the panes content.
         */
        public var hScrollbarOnTop:Boolean ;
        
        /**
         * Defines the policy of the horizontal scrollbar : "auto" (default), "on", "off".
         */
        public function get hScrollbarPolicy():String
        {
            return _hScrollbarPolicy ;
        }
        
        /**
         * @private
         */
        public function set hScrollbarPolicy( value:String ):void
        {
            _hScrollbarPolicy = ( value == ScrollPolicy.OFF || value == ScrollPolicy.ON ) ? value : ScrollPolicy.AUTO ;
        }
        
        /**
         * Indicates the size of the horizontal scrollbar.
         */
        public var hScrollBarSize:uint = 4 ;
        
        /**
         * Indicates the style of the horizontal scrollbar.
         */
        public var hScrollBarStyle:Style ;
        
        /**
         * Whether you want the contents of the scroll pane to maintain it's position when you re-initialise it - so it doesn't scroll as you add more content (default true)
         */
        public var maintainPosition:Boolean = true ;
        
        /**
         * The padding between the field in the area and the background.
         */
        public function get padding():EdgeMetrics
        {
            return _padding ;
        }
        
        /**
         * @private
         */
        public function set padding( em:EdgeMetrics ):void
        {
           _padding.bottom = em ? em.bottom : 0 ;
           _padding.left   = em ? em.left   : 0 ;
           _padding.right  = em ? em.right  : 0 ;
           _padding.top    = em ? em.top    : 0 ;
        }
        
        /**
         * Defines what the position of the content should be.
         */
        public function get position():String
        {
            return _position ;
        }
        
        /**
         * Defines what the position of the content should be, by default the position is "static" but you can use the "fixed" value to positioned at the desired coordinates the content display.
         * @see graphics.Position
         */
        public function set position( value:String ):void
        {
            _position = _positions.indexOf(value) > -1 ? value : Position.STATIC ;
        }
        
        /**
         * The delay to see the scrollbars when the scroll of the component is changed (in milliseconds).
         */
        public var scrollBarDelay:uint = 500 ;
        
        /**
         * Sets a String representing the scroll direction of the scroll pane. 
         * <p>This property value can be :</p>
         * <ul>
         * <li>Direction.NONE (no scroll),</li>
         * <li>Direction.VERTICAL (only vertical scroll,</li>
         * <li>Direction.HORIZONTAL (only horizontal scroll),</li>
         * <li>Direction.BOTH (horizontal and vertical scrolling)</li>
         * </ul>
         * @see graphics.Direction
         */
        public function get scrollDirection():String
        {
            return _scrollDirection ;
        }
        
        /**
         * @private
         */
        public function set scrollDirection( value:String ):void
        {
            _scrollDirection = _scrollDirections.indexOf(value) > -1 ? value : Direction.NONE ;
        }
        
        /**
         * The minimum size to allow the drag bars to be (default to 20 px).
         */
        public var scrollDragMinSize:int = 20  ;
        
        /**
         * The maximum size to allow the drag bars to be (default to 999999).
         */
        public var scrollDragMaxSize:int = 99999;
        
        /**
         * Indicates the scroll duration when the scroll is smoothing.
         */
        public var scrollDuration:uint = 24 ;
        
        /**
         * Indicates the scroll easing function when the scroll is smoothing.
         */
        public var scrollEasing:Function = expoOut ;
        
        /**
         * Indicates how many pixels constitutes a touch to scroll the content with the finger or the mouse.
         */
        public var scrollRatio:uint = 20 ;
        
        /**
         * Controls whether or not the scrolling is smoothed when scaled.
         */
        public var smoothing:Boolean = true ;
        
        /**
         * Indicates if the scrollpane use natural scrolling or reverse scrolling (by default is natural).
         */
        public var useNaturalScrolling:Boolean = true ;
        
        /**
         * Indicates if the scrollpane use the scrollRect property to mask the elements out of the visible area.
         */
        public var useScrollRect:Boolean = true  ;
        
        /**
         * The vertical strength ratio (default 1).
         */
        public var verticalStrength:Number = 1 ;
        
        /**
         * Gets or sets the x or y postion of the scrollbar, in pixels.
         */
        public var vScrollBarOffset:int = 4 ;
        
        /**
         * Whether the vertical scrollbar should appear on the left hand side of the panes content.
         */
        public var vScrollbarOnLeft:Boolean ;
        
        /**
         * Defines the policy of the vertical scrollbar : "auto" (default), "on", "off".
         */
        public function get vScrollbarPolicy():String
        {
            return _vScrollbarPolicy ;
        }
        
        /**
         * @private
         */
        public function set vScrollbarPolicy( value:String ):void
        {
            _vScrollbarPolicy = ( value == ScrollPolicy.OFF || value == ScrollPolicy.ON ) ? value : ScrollPolicy.AUTO ;
        }
        
        /**
         * Indicates the size of the vertical scrollbar.
         */
        public var vScrollBarSize:uint = 4 ;
        
        /**
         * Indicates the style of the vertical scrollbar.
         */
        public var vScrollBarStyle:Style ;
        
        /**
         * Indicates if the scroll can be horizontal.
         */
        public function isHorizontal():Boolean
        {
            return _scrollDirection == Direction.BOTH || _scrollDirection == Direction.HORIZONTAL ;
        }
        
        /**
         * Indicates if the scroll can be vertical.
         */
        public function isVertical():Boolean
        {
            return _scrollDirection == Direction.BOTH || _scrollDirection == Direction.VERTICAL ;
        }
        
        /**
         * @private
         */
        protected var _hScrollbarPolicy:String = ScrollPolicy.AUTO ;
        
        /**
         * @private
         */
        protected const _padding:EdgeMetrics = new EdgeMetrics() ;
        
        /**
         * @private
         */
        protected var _position:String = Position.STATIC ;
        
        /**
         * The collection of all scroll directions valids in this component.
         */
        protected static const _positions:Vector.<String> = Vector.<String>([ Position.FIXED , Position.STATIC ]) ; // Position.ABSOLUTE and Position.RELATIVE not supported yet.
        
        /**
         * @private
         */
        protected var _scrollDirection:String = Direction.VERTICAL ;
        
        /**
         * The collection of all scroll directions valids in this component.
         */
        protected static const _scrollDirections:Vector.<String> = Vector.<String>([ Direction.VERTICAL , Direction.HORIZONTAL , Direction.BOTH , Direction.NONE ]) ;
        
        /**
         * @private
         */
        protected var _vScrollbarPolicy:String = ScrollPolicy.AUTO ;
    }
}
