﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling.components
{
    import graphics.Direction;
    import graphics.Measurable;

    import molecule.components.Draggable;
    import molecule.render.starling.display.dragger;

    import starling.display.DisplayObject;
    import starling.events.Touch;
    import starling.events.TouchEvent;
    import starling.events.TouchPhase;

    import system.hack;
    import system.signals.Signal;
    import system.signals.Signaler;

    import flash.geom.Rectangle;
    
    use namespace hack ;
    
    /**
     * This class provides a skeletal implementation of all the <code class="prettyprint">Scrollbar</code> display components, 
     * to minimize the effort required to implement this interface.
     */
    public class CoreDragScrollbar extends CoreScrollbar implements Draggable
    {
        /**
         * Creates a new CoreDragScrollbar instance.
         * @param init An object that contains properties with which to populate the newly instance. If init is not an object, it is ignored.
         */
        public function CoreDragScrollbar( init:Object = null )
        {
            super( init ) ;
        }
        
        /**
         * This signal is invoked when the drag phase is in progress.
         */
        public function get drag():Signaler
        {
            return _drag ;
        }
        
        /**
         * This signal is invoked when the drag phase is in started.
         */
        public function get dragStarted():Signaler
        {
            return _dragStarted ;
        }
        
        /**
         * This signal is invoked when the drag phase is in stopped.
         */
        public function get dragStopped():Signaler
        {
            return _dragStopped ;
        }
        
        /**
         * Indicates if the bar is dragging.
         */
        public function get isDragging():Boolean 
        {
            return _lockPosition ;
        }
        
        /**
         * Invoked when the bar is dragging.
         */
        public function dragging( ...args:Array ):void 
        {
            var ts:Number = thumbSize ;
            var oldPosition:Number = position ;
            var newPosition:Number ;
            if( direction == Direction.HORIZONTAL )
            {
                newPosition = _thumb.x / (((bar is Measurable) ? (bar as Measurable).w : bar.width) - ts) * (_max - _min) + _min ;
                if ( _invert )
                {
                    newPosition = (_max + _min) - newPosition ;
                }
            }
            else
            {
                var range:Number = ((bar is Measurable) ? (bar as Measurable).h : bar.height) - ts ;
                newPosition = _min + ( range - _thumb.y ) / range * (_max - _min) ;
                if ( _invert == false )
                {
                    newPosition = (_max + _min) - newPosition ;
                }
            }
            setPosition( Math.round(newPosition) ) ;
            if(newPosition != oldPosition)
            {
                notifyDrag() ;
            }
        }
        
        /**
         * Dispatchs an event when the user drag the bar.
         */
        public function notifyDrag():void 
        {
            if( _drag.connected() )
            {
                _drag.emit( this ) ;
            }
        }
        
        /**
         * Dispatchs an event when the user start to drag the bar.
         */
        public function notifyStartDrag():void
        {
            if( _dragStarted.connected() )
            {
                _dragStarted.emit( this ) ;
            }
        }
        
        /**
         * Dispatchs an event when the user stop to drag the bar.
         */
        public function notifyStopDrag():void
        {
            if( _dragStopped.connected() )
            {
                _dragStopped.emit( this ) ;
            }
        }
        
        /**
         * Invoked when the user start to drag the bar.
         */
        public function startDragging( ...args:Array ):void 
        {
            notifyStartDrag() ;
            _lockPosition = true ;
            
            if ( stage )
            {
                stage.addEventListener( TouchEvent.TOUCH, touch ) ;
            }
            
            dragger.target = _thumb ;
            
            if( _direction == Direction.HORIZONTAL )
            {
                dragger.startDrag(false, new Rectangle(0 , 0 , ((bar is Measurable) ? bar.w : bar.width) - thumbSize , 0 ) ) ;
            }
            else
            {
                dragger.startDrag(false, new Rectangle(0 , 0 , 0 , ((bar is Measurable) ? bar.h : bar.height) - thumbSize ) ) ;
            }
        }
        
        /**
         * Invoked when the user stop to drag the bar.
         */
        public function stopDragging( ...args:Array ):void 
        {
            _lockPosition = false ;
            if ( stage )
            {
                stage.removeEventListener( TouchEvent.TOUCH, touch ) ;
            }
            dragger.stopDrag();
            notifyStopDrag() ;
        }
        
        /**
         * @private
         */
        hack var _drag:Signaler = new Signal() ;
        
        /**
         * @private
         */
        hack var _dragStopped:Signaler = new Signal() ;
        
        /**
         * @private
         */
        hack var _dragStarted:Signaler = new Signal() ;
        
        
        /**
         * @private
         */
        protected function touch( e:TouchEvent ):void
        {
            if( !enabled )
            {
                if( _lockPosition )
                {
                    stopDragging() ;
                }
                return ;
            }
            var touch:Touch = e.getTouch( e.target as DisplayObject ) ;
            if( touch.phase == TouchPhase.ENDED )
            {
                stopDragging() ;
            }
            else if( touch.phase == TouchPhase.MOVED )
            {
                dragging() ;
            }
        }
    }
}
