﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
  
*/
package molecule.render.starling.components.buttons
{
    import molecule.components.ButtonPhase;
    import molecule.render.starling.components.CoreButton;

    import starling.display.DisplayObject;

    import system.hack;
    import system.signals.Receiver;

    use namespace hack ;

    /**
     * The SimpleButton class lets you control all basic instances of button in the framework.
     */
    public class SimpleButton extends CoreButton implements Receiver
    {
        /**
         * Creates a new SimpleButton instance.
         * @param init An object that contains properties with which to populate the newly instance. If init is not an object, it is ignored.
         * @param locked The flag to lock the new current object when is created.
         */
        public function SimpleButton( init:Object = null , locked:Boolean = false )
        {
            super( init , locked );
            
            disable.connect( this ) ;
            down.connect( this ) ;
            out.connect( this ) ;
            over.connect( this ) ;
            up.connect( this ) ;
        }
        
        /**
         * Specifies a display object that is used as the visual object for the "disabled" state.
         */
        public function get disabledState():DisplayObject 
        {
            return _disabledState ;
        }
        
        /**
         * @private
         */
        public function set disabledState( display:DisplayObject ):void
        {
            _disabledState = display ;
            update() ;
        }
        
        /**
         * Specifies a display object that is used as the visual object for the "down" state.
         */
        public function get downState():DisplayObject 
        {
            return _downState ;
        }
        
        /**
         * @private
         */
        public function set downState( display:DisplayObject ):void
        {
            _downState = display ;
            update() ;
        }
        
        /**
         * Specifies a display object that is used as the visual object for the "over" state — the state that the button is in when the pointer is positioned over the button.
         */
        public function get overState():DisplayObject 
        {
            return _overState ;
        }
        
        /**
         * @private
         */
        public function set overState( display:DisplayObject ):void
        {
            _overState = display ;
            update() ;
        }
        
        /**
         * Specifies a display object that is used as the visual object for the "up" state — the state that the button is in when the pointer is not positioned over the button.
         */
        public function get upState():DisplayObject 
        {
            return _upState ;
        }
        
        /**
         * @private
         */
        public function set upState( display:DisplayObject ):void
        {
            _upState = display ;
            update() ;
        }
        
        /**
         * The receiver method.
         */
        public function receive( ...args:Array ):void
        {
            update() ;
        }
        
        /**
         * @private
         */
        protected var _current:DisplayObject ;
        
        /**
         * @private
         */
        protected var _downState:DisplayObject ;
        
        /**
         * @private
         */
        protected var _disabledState:DisplayObject ;
        
        /**
         * @private
         */
        protected var _overState:DisplayObject ;
        
        /**
         * @private
         */
        protected var _upState:DisplayObject ;
        
        /**
         * Invoked when the view is changed.
         */
        protected override function viewChanged():void
        {
            if( _current )
            {
                if( contains( _current ) )
                {
                    removeChild( _current ) ; 
                }
                _current = null ;
            }
            
            switch( _phase ) 
            {
                case ButtonPhase.DISABLE :
                {
                    _current = _disabledState ;
                    break ;
                }
                case ButtonPhase.DOWN :
                {
                    _current = _downState ;
                    break ;
                }
                case ButtonPhase.OVER :
                {
                    _current = _overState ;
                    break ;
                }
                default :
                case ButtonPhase.UP :
                {
                    _current = _upState ;
                    break ;
                }
            }
            
            if( _current && !contains(_current) )
            {
                if( numChildren > 0 )
                {
                    addChildAt( _current , 0 ) ;
                }
                else
                {
                    addChild( _current ) ;
                }
            }
        }
    }
}
