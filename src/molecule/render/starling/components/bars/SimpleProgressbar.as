﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.starling.components.bars
{
    import core.maths.map;
    import core.maths.replaceNaN;
    
    import graphics.Align;
    import graphics.Direction;
    
    import molecule.render.starling.components.CoreProgressbar;
    
    import starling.display.Quad;
    
    import system.hack;
    
    use namespace hack ;
    
    /**
     * The SimpleProgressbar component.
     * <p><b>Example :</b></p>
     * <pre class="prettyprint">
     * import graphics.Align ;
     * import graphics.Direction ;
     * import graphics.geom.EdgeMetrics ;
     * 
     * import molecule.render.starling.components.bars.SimpleProgressbar ;
     * 
     * stage.scaleMode = StageScaleMode.NO_SCALE ;
     * 
     * var change:Function = function( bar:SimpleProgressBar ):void
     * {
     *     trace( "change position: " + bar.position ) ;
     * }
     * 
     * var bar:SimpleProgressbar = new SimpleProgressbar() ;
     * 
     * bar.changed.connect( change ) ;
     * 
     * bar.x = 25 ;
     * bar.y = 25 ;
     * 
     * bar.w = 350 ;
     * bar.h = 12 ;
     * 
     * bar.w = 12 ;
     * bar.h = 350 ;
     * 
     * bar.position = 50  ;
     * bar.border = new EdgeMetrics(2,2,2,2)  ;
     * 
     * bar.direction = Direction.VERTICAL ;
     * 
     * bar.align = Align.RIGHT ;
     * 
     * body.addChild( bar ) ;
     * 
     * var keyDown:Function = function( e:KeyboardEvent ):void
     * {
     *     var code:uint = e.keyCode ;
     *     switch( code )
     *     {
     *         case Keyboard.LEFT :
     *         {
     *             bar.position -= 10 ;
     *             break ;
     *         }
     *         case Keyboard.RIGHT :
     *         {
     *             bar.position += 10 ;
     *             break ;
     *         }
     *         case Keyboard.SPACE :
     *         {
     *             bar.lock() ;
     *             bar.align               = Align.CENTER ; // Align.LEFT or Align.RIGHT or Align.CENTER
     *             bar.direction           = Direction.HORIZONTAL ;
     *             bar.backgroundFillStyle = new FillStyle( 0xB5C7CA , 1 ) ;
     *             bar.backgroundLineStyle = new LineStyle( 2 , 0xFFFFFF , 1 ) ;
     *             bar.barFillStyle        = new FillStyle( 0x921085 , 1 ) ;
     *             bar.barLineStyle        = new LineStyle( 1 , 0x6A9195, 1 ) ;
     *             bar.border              = new EdgeMetrics(2, 2, 2, 2) ;
     *             bar.unlock() ;
     *             bar.setPreferredSize( 200, 8 ) ;
     *             break ;
     *         }
     *         case Keyboard.UP :
     *         {
     *             bar.minimum = 20  ;
     *             bar.maximum = 200 ;
     *             break ;
     *         }
     *         default :
     *         {
     *             bar.direction = ( bar.direction == Direction.VERTICAL ) ? Direction.HORIZONTAL : Direction.VERTICAL ;
     *             if ( bar.direction == Direction.VERTICAL )
     *             {
     *                 bar.setPreferredSize( 8, 200 ) ;
     *             }
     *             else
     *             {
     *                 bar.setPreferredSize( 200, 8 ) ;
     *             }
     *         }
     *     }
     * }
     * 
     * stage.addEventListener( KeyboardEvent.KEY_DOWN , keyDown ) ;
     * 
     * trace("Press Keyboard.LEFT or Keyboard.RIGHT or Keyboard.SPACE or other keyboard touch to test this example.") ;
     * </pre>
     */
    public class SimpleProgressbar extends CoreProgressbar
    {
        /**
         * Creates a new SimpleProgressbar instance.
         */
        public function SimpleProgressbar( w:Number = 200 , h:Number = 10 )
        {
            addChild( _background ) ;
            addChild( _bar ) ;
            setPreferredSize(w, h) ;
        }
        
        /**
         * @private
         */
        public override function set align( align:uint ):void
        {
            super.align = ( _alignments.indexOf(align) > -1 ) ? align : Align.LEFT ;
            update() ;
        }
        
        /**
         * Defines the alpha of the background.
         */
        public function get backgroundAlpha():Number
        {
            return _background.alpha ;
        }
        
        /**
         * @private
         */
        public function set backgroundAlpha( value:Number ):void
        {
            _background.alpha = value ;
        }
        
        /**
         * Defines the color of the background.
         */
        public function get backgroundColor():Number
        {
            return _background.color ;
        }
        
        /**
         * @private
         */
        public function set backgroundColor( value:Number ):void
        {
            _background.color = value ;
        }
        
        /**
         * Defines the alpha component of the bar.
         */
        public function get barAlpha():Number
        {
            return _bar.alpha ;
        }
        
        /**
         * @private
         */
        public function set barAlpha( value:Number ):void
        {
            _bar.alpha = value ;
        }
        
        /**
         * Defines the color component of the bar.
         */
        public function get barColor():Number
        {
            return _bar.color ;
        }
        
        /**
         * @private
         */
        public function set barColor( value:Number ):void
        {
            _bar.color = value ;
        }
        
        /**
         * Draws the view of the component.
         */
        public override function draw( ...arguments:Array ):void
        {
            _background.width  = w ;
            _background.height = h ;
        }
        
        /**
         * @private
         */
        protected const _alignments:Array = [ Align.LEFT  , Align.CENTER , Align.RIGHT ] ;
        
        /**
         * @private
         */
        protected var _background:Quad = new Quad(240,10,0x000000);
        
        /**
         * The thumb reference of this bar.
         */
        protected var _bar:Quad = new Quad(10,10,0xFFFFFF);
        
        /**
         * Invoked when the position of the bar is changed.
         * @param flag (optional) An optional boolean. By default this flag is passed-in the setPosition method.
         */
        protected override function viewPositionChanged( flag:Boolean = false ):void 
        {
            var isVertical:Boolean = direction == Direction.VERTICAL ;
            
            var horizontal:Number  = replaceNaN( border.horizontal ) ;
            var vertical:Number    = replaceNaN( border.vertical   ) ;
            
            var margin:Number      = isVertical ? vertical : horizontal ;
            var max:Number         = isVertical ? h : w ;
            
            var size:Number        =  map( position , minimum, maximum , 0 , (max - margin) ) ;
            
            var $b:Number          = replaceNaN( border.bottom ) ;
            var $l:Number          = replaceNaN( border.left ) ;
            var $r:Number          = replaceNaN( border.right ) ;
            var $t:Number          = replaceNaN( border.top ) ;
            
            var $w:Number          = isVertical ? ( w - horizontal ) : size  ;
            var $h:Number          = isVertical ? size : ( h - vertical ) ;
            
            _bar.width   = $w ;
            _bar.height  = $h ;
            _bar.visible = position > 0 ;
            
            if ( align == Align.RIGHT )
            {
                _bar.x = isVertical ? $l                   : w - _bar.width - $r ;
                _bar.y = isVertical ? h - _bar.height - $b : $t ;
            }
            else if ( align == Align.CENTER )
            {
                _bar.x = isVertical ? $l                   : ( w - _bar.width) * .5;
                _bar.y = isVertical ? (h-_bar.height) * .5 : $t ;
            }
            else // Align.LEFT (default)
            {
                _bar.x = $l ;
                _bar.y = $t ;
            }
        }
    }
}
