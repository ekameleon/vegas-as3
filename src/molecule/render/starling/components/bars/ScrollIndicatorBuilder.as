﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling.components.bars
{
    import graphics.Direction;
    import graphics.Measurable;
    import graphics.geom.EdgeMetrics;

    import molecule.render.starling.CoreBuilder;

    import starling.display.DisplayObject;
    import starling.display.Quad;
    import starling.events.Event;

    import system.hack;
    
    use namespace hack ;
    
    /**
     * The builder of the ScrollIndicator component.
     */
    public class ScrollIndicatorBuilder extends CoreBuilder
    {
        /**
         * Creates a new ScrollIndicatorBuilder instance.
         * @param target The component to build and update.
         */
        public function ScrollIndicatorBuilder( component:ScrollIndicator = null )
        {
            super( component ) ;
        }
        
        /**
         * Clear the component.
         */
        public override function clear():void
        {
            var comp:ScrollIndicator = _target as ScrollIndicator ;
            if( comp )
            {
                if( comp._bar )
                {
                    if( (comp._bar is DisplayObject) && comp.contains( comp._bar as DisplayObject ) )
                    {
                        comp.removeChild( comp._bar as DisplayObject ) ;
                    }
                    comp._bar.dispose() ;
                    comp._bar = null ;
                }
                
                if( (comp._thumb is DisplayObject) && comp._thumb )
                {
                    if( comp.contains( comp._thumb as DisplayObject ) )
                    {
                        comp.removeChild( comp._thumb as DisplayObject ) ;
                    }
                    comp._thumb.dispose() ;
                    comp._thumb = null ;
                }
            }
        }
        
        /**
         * Run the component building.
         */
        public override function run(...args:*):void
        {
            var comp:ScrollIndicator = _target as ScrollIndicator ;
            if( comp )
            {
                comp._bar   = new Quad(1,1,0x000000) ;
                comp._thumb = new Quad(1,1,0x000000) ;
                if( comp.stage )
                {
                    addedToStage() ;
                }
                else
                {
                    comp.addEventListener( Event.ADDED_TO_STAGE , addedToStage) ; // Need use the addedToStage event tofix a TypeError #1009
                }
            }
        }
        
        /**
         * @private
         */
        protected function addedToStage( e:Event = null ):void
        {
            if( _target is ScrollIndicator )
            {
                _target.removeEventListener( Event.ADDED_TO_STAGE , addedToStage) ;
                if( (_target as ScrollIndicator).bar )
                {
                    _target.addChild( (_target as ScrollIndicator)._bar ) ;
                }
                if( (_target as ScrollIndicator).thumb )
                {
                    _target.addChild( (_target as ScrollIndicator)._thumb ) ;
                }
            }
        }
        
        /**
         * Update the component.
         */
        public override function update():void
        {
            var comp:ScrollIndicator = _target as ScrollIndicator ;
            if( comp )
            {
                var border:EdgeMetrics  = comp._border ;
                var bar:DisplayObject   = comp.bar ;
                var thumb:DisplayObject = comp.thumb ;
                
                ////////////////
                
                var style:ScrollIndicatorStyle = comp.style as ScrollIndicatorStyle ;
                if( style )
                {
                    if( bar is Quad )
                    {
                        ( bar as Quad ).alpha = style.barAlpha ;
                        ( bar as Quad ).color = style.barColor ;
                    }
                    
                    if( thumb is Quad )
                    {
                        ( thumb as Quad ).alpha = style.thumbAlpha ;
                        ( thumb as Quad ).color = style.thumbColor ;
                    }
                }
                
                ////////////////
                
                var $w:Number = comp.w ;
                var $h:Number = comp.h ;
                
                if( bar is Measurable )
                {
                    (bar as Measurable).setPreferredSize( $w , $h ) ;
                }
                else
                {
                    bar.width  = $w ;
                    bar.height = $h ;
                }
                
                ////////////////
                
                $w = ( comp.direction == Direction.HORIZONTAL ) ? comp.thumbSize : comp.w - border.horizontal ;
                $h = ( comp.direction == Direction.HORIZONTAL ) ? comp.h - border.vertical : comp.thumbSize ;
                
                if( thumb is Measurable )
                {
                    (thumb as Measurable).setPreferredSize( $w , $h ) ;
                }
                else
                {
                    thumb.width  = $w ;
                    thumb.height = $h ;
                }
                
                thumb.x = border.top ;
                thumb.y = border.left ;
            }
        }
    }
}
