﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.starling 
{
    import starling.events.Event;
    import graphics.FillStyle;
    import graphics.IFillStyle;

    import molecule.Screen;
    import molecule.render.starling.display.Element;

    import starling.display.DisplayObjectContainer;
    import starling.display.Quad;

    import system.hack;
    import system.process.Action;
    
    use namespace hack ;
    
    /**
     * The basic representation of a Screen object.
     */
    public class KernelScreen extends Element implements Screen
    {
        /**
         * Creates a new KernelScreen instance.
         */
        public function KernelScreen( init:Object = null , locked:Boolean = false )
        {
             super( init , locked ) ;
        }
        
        /**
         * The container of the state if this state contains sub states.
         */
        public function get container():* 
        {
            return _scope ;
        }
        
        /**
         * @private
         */
        public function set container( target:* ):void 
        {
            scope = target as DisplayObjectContainer ;
        }
        
        /**
         * Invoked when the state is close (after the close).
         */
        public function get closeAfter():Action
        {
            return _closeAfter ;
        }
        
        /**
         * @private
         */
        public function set closeAfter( action:Action ):void
        {
            _closeAfter = action ;
        }
        
        /**
         * Invoked when the state is close (before the close).
         */
        public function get closeBefore():Action
        {
            return _closeBefore ;
        }
        
        /**
         * @private
         */
        public function set closeBefore( action:Action ):void
        {
            _closeBefore = action ;
        }
        
        /**
         * Determinates the IFillStyle reference of this display.
         */
        public function get fill():IFillStyle
        {
            return _fillStyle ;
        }
        
        /**
         * @private
         */
        public function set fill( style:IFillStyle ):void
        {
            _fillStyle = style ;
            if ( _locked == 0 ) 
            {
                update() ;
            }
        }
        
        /**
         * Invoked when the state is open (after the close).
         */
        public function get openAfter():Action
        {
            return _openAfter ;
        }
        
        /**
         * @private
         */
        public function set openAfter( action:Action ):void
        {
            _openAfter = action ;
        }
        
        /**
         * Invoked when the state is open (before the close).
         */
        public function get openBefore():Action
        {
            return _openBefore ;
        }
        
        /**
         * @private
         */
        public function set openBefore( action:Action ):void
        {
            _openBefore = action ;
        }
        
        /**
         * Close the state.
         */
        public function close():void
        {
            //
        } 
        
        /**
         * Draw the display.
         */
        public override function draw( ...args:Array ):void
        {
            if( _background )
            {
                fixArea() ;
                
                _background.x      = _real.x ;
                _background.y      = _real.y ;
                _background.width  = _real.width ;
                _background.height = _real.height ;
                
                if( _fillStyle is FillStyle )
                {
                    _background.alpha = ( _fillStyle as FillStyle ).alpha ;
                    _background.color = ( _fillStyle as FillStyle ).color ;
                }
            }
        }
        
        /**
         * Open the state.
         */
        public function open():void
        {
            //
        }
        
        /**
         * @private
         */
        hack var _background:Quad  ;
        
        /**
         * @private
         */
        hack var _closeAfter:Action ;
        
        /**
         * @private
         */
        hack var _closeBefore:Action ;
        
        /**
         * @private
         */
        hack var _fillStyle:IFillStyle = new FillStyle( 0x000000 );
        
        /**
         * @private
         */
        hack var _openAfter:Action ;
        
        /**
         * @private
         */
        hack var _openBefore:Action ;
        
        /**
         * @private
         */
        protected override function addedToStage(e:Event = null):void
        {
            if( !_background )
            {
                _background = new Quad(1,1,0x000000) ;
                addChildAt( _background , 0 ) ;
                draw() ;
            }
        }
    }
}
