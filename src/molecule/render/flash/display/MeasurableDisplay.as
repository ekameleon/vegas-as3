﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2006-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/

package molecule.render.flash.display
{
    import core.maths.clamp;
    
    import graphics.Align;
    import graphics.Measurable;
    
    import system.hack;
    
    import flash.geom.Rectangle;
    
    use namespace hack ;
    
    /**
     * The minimal implementation of the Measurable interface based on the flash.display rendering.
     */
    public class MeasurableDisplay extends MOB implements Measurable
    {
        /**
         * Creates a new MeasurableDisplay instance.
         * @param init An object that contains properties with which to populate the newly instance. If init is not an object, it is ignored.
         * @param locked An optional boolean to defines if the constructor must be locked, the update method is not invoked in the constructor if this flag is true.
         */
        public function MeasurableDisplay( init:Object = null , locked:Boolean = false )
        {
            if( locked )
            {
                lock() ;
            }
            
            initialize( init ) ;
            
            if( locked )
            {
                unlock() ;
            }
        }
        
        /**
         * The alignement of the background.
         * @see graphics.Align
         */
        public function get align():uint
        {
            return _align ;
        }
        
        /**
         * @private
         */
        public function set align( value:uint ):void
        {
            _align = value ;
            if ( _locked == 0 ) 
            {
                update() ;
            }
        }
        
        /**
         * Determinates the virtual height value of this component.
         */
        public function get h():Number 
        {
            return clamp( _h , _minHeight, _maxHeight ) ;
        }
        
        /**
         * @private
         */
        public function set h( value:Number ):void 
        {
            _h = clamp( value , _minHeight, _maxHeight ) ;
            if ( _locked == 0 ) 
            {
                update() ;
            }
            notifyResized() ;
        }
        
        /**
         * This property defined the maximum height of this display.
         */
        public function get maxHeight():Number
        {
            return _maxHeight ;
        }
        
        /**
         * @private
         */
        public function set maxHeight( value:Number ):void
        {
            _maxHeight = value ;
            if ( _maxHeight < _minHeight )
            {
                _maxHeight = _minHeight ;
            }
            if ( _locked == 0 ) 
            {
                update() ;
            }
        }
        
        /**
         * Defines the maximum width of this display.
         */
        public function get maxWidth():Number
        {
            return _maxWidth ;
        }
        
        /**
         * @private
         */
        public function set maxWidth( value:Number ):void
        {
            _maxWidth = value ;
            if ( _maxWidth < _minWidth )
            {
                _maxWidth = _minWidth ;
            }
            if ( _locked == 0 ) 
            {
                update() ;
            }
        }
        
        /**
         * This property defined the mimimun height of this display (This value is >= 0).
         */
        public function get minHeight():Number
        {
            return _minHeight ;
        }
        
        /**
         * @private
         */
        public function set minHeight( value:Number ):void
        {
            _minHeight = value > 0 ? value : 0 ;
            if ( _minHeight > _maxHeight )
            {
                _minHeight = _maxHeight ;
            }
            if ( _locked == 0 ) 
            {
                update() ;
            }
        }
        
        /**
         * This property defined the mimimun width of this display (This value is >= 0).
         */
        public function get minWidth():Number
        {
            return _minWidth ;
        }
        
        /**
         * @private
         */
        public function set minWidth( value:Number ):void
        {
            _minWidth = value > 0 ? value : 0 ;
            if ( _minWidth > _maxWidth )
            {
                _minWidth = _maxWidth ;
            }
            if ( _locked == 0 ) 
            {
                update() ;
            }
        }
        
        /**
         * Determinates the virtual height value of this component.
         */
        public function get w():Number 
        {
            return clamp( _w , _minWidth, _maxWidth ) ;
        }
        
        /**
         * @private
         */
        public function set w( value:Number ):void 
        {
            _w = clamp( value , _minWidth, _maxWidth ) ;
            if ( _locked == 0 ) 
            {
                update() ;
            }
            notifyResized() ;
        }
        
       /**
         * Initialize the object.
         * @param init An object that contains properties with which to populate the newly instance. If init is not an object, it is ignored.
         */
        public function initialize( init:Object = null ):void 
        {
            if( init )
            {
                lock() ;
                for( var prop:String in init )
                {
                    this[prop] = init[prop] ;
                }
                unlock() ;
            }
            if ( _locked == 0 ) 
            {
                update() ;
            }
        }
        
        /**
         * Notify an event when you resize the component.
         */
        public function notifyResized():void 
        {
            viewResize() ;
            _resized.emit( this ) ;
        }
        
        /**
         * Sets the preferred width and height of the display.
         */
        public function setPreferredSize( w:Number , h:Number ):void
        {
            _w = isNaN(w) ? 0 : clamp( w , _minWidth, _maxWidth) ; 
            _h = isNaN(h) ? 0 : clamp( h , _minHeight, _maxHeight) ;
            if ( _locked == 0 ) 
            {
                update() ;
            }
            notifyResized() ;
        }
        
        /**
         * Sets the width and height values of the component.
         */
        public function setSize( w:Number, h:Number ):void
        {
            width  = isNaN(w) ? 0 : clamp( w , _minWidth, _maxWidth) ; 
            height = isNaN(h) ? 0 : clamp( h , _minHeight, _maxHeight) ;
            if ( _locked == 0 ) 
            {
                update() ;
            }
            notifyResized() ;
        }
        
        //////////
        
        /**
         * Invoked when the component is resized.
         * Overrides this method.
         */
        protected function viewResize():void 
        {
            // overrides
        }
        
        //////////
        
        /**
         * @private
         */
        hack var _align:uint = Align.TOP_LEFT ;
        
        /**
         * @private
         */
        hack var _h:Number = 0 ;
        
        /**
         * @private
         */
        hack var _maxHeight:Number ;
        
        /**
         * @private
         */
        hack var _maxWidth:Number ;
        
        /**
         * @private
         */
        hack var _minHeight:Number = 0 ;
        
        /**
         * @private
         */
        hack var _minWidth:Number = 0 ;
        
        /**
         * @private
         */
        hack const _real:Rectangle = new Rectangle();
        
        /**
         * @private
         */
        hack var _w:Number = 0 ;
        
        /**
         * Refresh the real area Rectangle of the background with the current alignement.
         */
        hack function fixArea():Rectangle
        {
            // initialize
            
            _real.width  = w ;
            _real.height = h ;
            _real.x      = 0 ;
            _real.y      = 0 ;
            
            // update
            
            if( _align == Align.BOTTOM ) 
            {
                _real.x -= _real.width / 2 ;
                _real.y -= _real.height ;
            }
            else if ( _align == Align.BOTTOM_LEFT )
            {
                _real.y -= _real.height ;
            }
            else if ( _align == Align.BOTTOM_RIGHT )
            {
                _real.x -= _real.width ;
                _real.y -= _real.height ;
            }
            else if ( _align == Align.CENTER )
            {
                _real.x -= _real.width / 2 ;
                _real.y -= _real.height / 2 ;
            }
            else if ( _align == Align.LEFT )
            {
                _real.y -= _real.height / 2 ;
            }
            else if ( _align == Align.RIGHT )
            {
                _real.x -= _real.width ;
                _real.y -= _real.height / 2 ;
            }
            else if ( _align == Align.TOP )
            {
                _real.x -= _real.width / 2 ;
            }
            else if ( _align == Align.TOP_RIGHT )
            {
                _real.x -= _real.width ;
            }
            
            // result
            
            return _real ;
        }
    }
}