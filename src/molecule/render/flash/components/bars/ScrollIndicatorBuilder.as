﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.bars
{
    import graphics.Direction;
    import graphics.geom.EdgeMetrics;
    
    import molecule.render.flash.CoreBuilder;
    import molecule.render.flash.display.RoundedBackground;
    
    import system.hack;
    
    use namespace hack ;
    
    /**
     * The builder of the ScrollIndicator component.
     */
    public class ScrollIndicatorBuilder extends CoreBuilder
    {
        /**
         * Creates a new ScrollIndicatorBuilder instance.
         * @param target The component to build and update.
         */
        public function ScrollIndicatorBuilder( component:ScrollIndicator = null )
        {
            super( component ) ;
        }
        
        /**
         * Clear the component.
         */
        public override function clear():void
        {
            var comp:ScrollIndicator = _target as ScrollIndicator ;
            if( comp )
            {
                if( comp._bar )
                {
                    if( comp.contains( comp._bar ) )
                    {
                        comp.removeChild( comp._bar ) ;
                    }
                    comp._bar = null ;
                }
                
                if( comp._thumb )
                {
                    if( comp.contains( comp._thumb ) )
                    {
                        comp.removeChild( comp._thumb ) ;
                    }
                    comp._thumb = null ;
                }
            }
        }
        
        /**
         * Run the component building.
         */
        public override function run(...args:*):void
        {
            var comp:ScrollIndicator = _target as ScrollIndicator ;
            if( comp )
            {
                comp._bar   = new RoundedBackground() ;
                comp._thumb = new RoundedBackground() ;
                
                comp.addChild( comp._bar ) ;
                comp.addChild( comp._thumb ) ;
            }
        }
        
        /**
         * Update the component.
         */
        public override function update():void
        {
            var comp:ScrollIndicator = _target as ScrollIndicator ;
            if( comp )
            {
                var bar:RoundedBackground       = comp._bar as RoundedBackground  ;
                var thumb:RoundedBackground     = comp._thumb as RoundedBackground  ;
                var border:EdgeMetrics          = comp._border ;
                var style:ScrollIndicatorStyle = comp.style as ScrollIndicatorStyle ;
                
                if( style )
                {
                    bar.cornerRadius = style.cornerRadius ;
                    bar.fill         = style.barFill ;
                    bar.line         = style.barLine ;
                    
                    thumb.cornerRadius = style.cornerRadius ;
                    thumb.fill = style.thumbFill ;
                    thumb.line = style.thumbLine ;
                }
                
                thumb.x = border.top ;
                thumb.y = border.left ;
                
                bar.setPreferredSize( comp.w , comp.h ) ;
                
                if ( comp.direction == Direction.HORIZONTAL )
                {
                    thumb.setPreferredSize( comp.thumbSize , comp.h - border.vertical ) ;
                }
                else
                {
                    thumb.setPreferredSize( comp.w - border.horizontal , comp.thumbSize ) ;
                }
            }
        }
    }
}
