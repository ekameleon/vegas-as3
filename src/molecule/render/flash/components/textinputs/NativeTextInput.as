﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.flash.components.textinputs
{
    import molecule.Builder;
    import molecule.Style;
    import molecule.render.flash.CoreComponent;
    
    import system.hack;
    
    use namespace hack ;
    
    /**
     * The native text input component.
     */
    public class NativeTextInput extends CoreComponent
    {
        /**
         * Creates a new NativeTextInput instance.
         * @param init An object that contains properties with which to populate the newly instance. If init is not an object, it is ignored.
         */
        public function NativeTextInput( init:Object = null )
        {
            super( init );
        }
        
        /**
         * Indicates if the component is freezed.
         */
        public function get freezed():Boolean
        {
            return (_builder as NativeTextInputBuilder)._freezed ;
        }
        
        /**
         * The current text in the text field.
         */
        public function get text():String
        {
            var b:NativeTextInputBuilder = _builder as NativeTextInputBuilder ;
            return (b && b._stageText) ? b._stageText.text : null ;
        }
        
        /**
         * @private
         */
        public function set text( value:String ):void
        {
            var b:NativeTextInputBuilder = _builder as NativeTextInputBuilder ;
            if ( b && b._stageText ) 
            {
                b._stageText.text = value ;
            }
            else
            {
                _text = value != null ? value : "" ;
            }
        }
        
        /**
         * Freeze the component. 
         * @return true if the component is freezed.
         */
        public function freeze():Boolean
        {
            return (_builder as NativeTextInputBuilder).freeze() ;
        }
        
        /**
         * Enforce the component to refresh the area on the stage in which the internal StageText object is displayed.
         */
        public function refreshViewPort():void
        {
             (_builder as NativeTextInputBuilder).updateLayout() ;
        }
        
        /**
         * Unfreeze the component. 
         * @return true if the component is unfreezed.
         */
        public function unfreeze():Boolean
        {
            return (_builder as NativeTextInputBuilder).unfreeze() ;
        }
        
        /**
         * The internal text value of the input field.
         */
        hack var _text:String = "" ;
        
        /**
         * Returns the <code class="prettyprint">Builder</code> constructor use to initialize this component.
         * @return the <code class="prettyprint">Builder</code> constructor use to initialize this component.
         */
        protected override function getBuilderRenderer():Builder 
        {
            return new NativeTextInputBuilder(this) ;
        }
        
        /**
         * Returns the <code class="prettyprint">Style</code> constructor use to initialize this component.
         * @return the <code class="prettyprint">Style</code> constructor use to initialize this component.
         */
        protected override function getStyleRenderer():Style 
        {
            return new NativeTextInputStyle() ;
        }
    }
}
