﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.render.flash.components.textinputs
{
    import graphics.Align;
    import graphics.FillStyle;
    import graphics.IFillStyle;
    import graphics.ILineStyle;
    import graphics.LineStyle;
    import graphics.geom.EdgeMetrics;
    
    import molecule.Stylus;
    
    import system.hack;
    
    import flash.text.AutoCapitalize;
    import flash.text.ReturnKeyLabel;
    import flash.text.SoftKeyboardType;
    import flash.text.TextFormatAlign;
    import flash.text.engine.FontPosture;
    import flash.text.engine.FontWeight;
    
    use namespace hack ;
    
    /**
     * This style of the NativeTextInput component.
     */
    public class NativeTextInputStyle extends Stylus
    {
        /**
         * Creates a new NativeTextInputStyle instance.
         * @param init An object that contains properties with which to populate the newly Style object. If init is not an object, it is ignored.
         */
        public function NativeTextInputStyle( init:* = null )
        {
            super(init);
        }
        
        /**
         * The alignment of the component.
         * @see graphics.Align
         */
        public var align:uint = Align.TOP_LEFT ;
        
        /**
         * Controls how a device applies auto capitalization to user input. 
         * <p>Valid values are defined as constants in the AutoCapitalize class:</p>
         * <ul>
         * <li>"none"</li>
         * <li>"word"</li>
         * <li>"sentence"</li>
         * <li>"all"</li>
         * </ul>
         * <p>This property is only a hint to the underlying platform, because not all devices and operating systems support this functionality.</p>
         * <p>The default value is AutoCapitalize.NONE.</p>
         */
        public var autoCapitalize:String = AutoCapitalize.NONE ;
        
        /**
         * Indicates whether a device auto-corrects user input for spelling or punctuation mistakes. The default value is false.
         */
        public var autoCorrect:Boolean ;
        
        /**
         * Specifies text color. You specify text color as a number containing three 8-bit RGB components. 
         * The default value is 0x000000.
         */
        public var color:uint ;
        
        /**
         * Indicates whether the text field is a password text field. If true, the text field hides input characters using a substitute character (for example, an asterisk). 
         * <p><b>Important:</b> On iOS, a multiline stage text object does not display substitute characters even when the value of this property is true.</p>
         * <p>The default value is false.</p>
         */
        public var displayAsPassword:Boolean ;
        
        /**
         * Indicates whether the user can edit the text field. The default value is true.
         */
        public var editable:Boolean = true ;
        
        /**
         * Indicates the name of the current font family. A value of null indicates the system default. 
         * <p>To enumerate the available fonts, use flash.text.Font.enumerateFonts(). If the font family is unknown, the default font family is used.</p> 
         * <p>The default value is null.</p>
         */
        public var fontFamily:String ;
        
        /**
         * Specifies the font posture, using constants defined in the FontPosture class ("normal" or "italic"). The default value is FontPosture.NORMAL.
         */
        public var fontPosture:String = FontPosture.NORMAL ;
        
        /**
         * The size in pixels for the current font family.
         */
        public var fontSize:int = 12 ;
        
        /**
         * Specifies the font weight, using constants defined in the FontWeight class ("normal" or "bold"). The default value is FontWeight.NORMAL.
         */
        public var fontWeight:String = FontWeight.NORMAL ;
        
        /**
         * Indicates the locale of the text. StageText uses the standard locale identifiers. For example "en", "en_US" and "en-US" are all English; "ja" is Japanese. See <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php">iso639-2 code list</a> for a list of locale codes.
         * The default value is en.
         */
        public var locale:String = "en" ;
        
        /**
         * Indicates the maximum number of characters that a user can enter into the text field. A script can insert more text than maxChars allows. 
         * If maxChars equals zero, a user can enter an unlimited amount of text into the text field. The default value is 0.
         */
        public var maxChars:int ;
        
        /**
         * Indicates whether the StageText object can display more than one line of text.
         */
        public var multiline:Boolean ;
        
        /**
         * Defines the space inside the border between the border and the actual image or box contents.
         */
        public function get padding():EdgeMetrics
        {
            return _padding ;
        }
        
        /**
         * @private
         */
        public function set padding( em:EdgeMetrics ):void
        {
           _padding.bottom = em ? em.bottom : 0 ;
           _padding.left   = em ? em.left   : 0 ;
           _padding.right  = em ? em.right  : 0 ;
           _padding.top    = em ? em.top    : 0 ;
        }
        
        /**
         * Restricts the set of characters that a user can enter into the text field. The system scans the restrict string from left to right.
         */
        public var restrict:String ;
        
        /**
         * Indicates the label on the Return key for devices that feature a soft keyboard. 
         * <p>The available values are constants defined in the ReturnKeyLabel class:</p>
         * <ul>
         * <li>"default"</li>
         * <li>"done"</li>
         * <li>"go"</li>
         * <li>"next"</li>
         * <li>"search"</li>
         * </ul>
         * <p>This property is only a hint to the underlying platform, because not all devices and operating systems support these values. 
         * This property has no affect on devices that do not feature a soft keyboard.</p>
         * <p>The default value is ReturnKeyLabel.DEFAULT.</p>
         */
        public var returnKeyLabel:String = ReturnKeyLabel.DEFAULT ;
        
        /**
         * Controls the appearance of the soft keyboard.
         * <p>Devices with soft keyboards can customize the keyboard's buttons to match the type of input expected. 
         * For example, if numeric input is expected, a device can use SoftKeyboardType.NUMBER to display only numbers on the soft keyboard.</p> 
         * <p>Valid values are defined as constants in the SoftKeyboardType class:</p>
         * <ul>
         * <li>"default"</li>
         * <li>"punctuation"</li>
         * <li>"url"</li>
         * <li>"number"</li>
         * <li>"contact"</li>
         * <li>"email"</li>
         * </ul>
         * <p>These values serve as hints, to help a device display the best keyboard for the current operation.</p>
         * <p>The default value is SoftKeyboardType.DEFAULT.</p>
         */
        public var softKeyboardType:String = SoftKeyboardType.DEFAULT ;
        
        /**
         * The style name of the class who target this component in this stylesheet. 
         * The style class can accept the attributes "color", "font-family", "fonts-style" , "font-weight"
         */
        public var styleName:String = "native-text-input" ;
        
        /**
         * Indicates the paragraph alignment.
         * <p>Valid values are defined as constants in the TextFormatAlign class:</p>
         * <ul>
         * <li>"left"</li>
         * <li>"center"</li>
         * <li>"right"</li>
         * <li>"justify"</li>
         * <li>"start"</li>
         * <li>"end"</li>
         * </ul>
         * <p>Not all platforms support every textAlign value. For unsupported textAlign values, platforms use the default value (TextFormatAlign.START).</p>
         * <p>The default value is TextFormatAlign.START.</p>
         */
        public var textAlign:String = TextFormatAlign.START ;
        
        /**
         * The theme style when the component is enabled.
         */
        public var theme:IFillStyle = new FillStyle( 0xFFFFFF ) ;
        
        /**
         * The border style when the component is enabled.
         */
        public var themeBorder:ILineStyle = new LineStyle( 2 , 0xCCCCCC ) ;
        
        /**
         * The border style when the component is disabled.
         */
        public var themeBorderDisabled:ILineStyle = new LineStyle( 2 , 0xFFFFFF ) ;
        
        /**
         * The theme style when the component is disabled.
         */
        public var themeDisabled:IFillStyle = new FillStyle( 0xA2A2A2 ) ;
        
        /**
         * @private
         */
        hack const _padding:EdgeMetrics = new EdgeMetrics(2,2,2,2) ;
    }
}
