﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels
{
    import system.events.EventListener;

    import flash.display.Stage;
    import flash.events.Event;
    import flash.events.KeyboardEvent;
    import flash.text.TextField;
    import flash.ui.Keyboard;
    
    /**
     * The pagination label component.
     */
    public class PaginationKeyDown implements EventListener
    {
        /**
         * Creates a new PaginationLabel.as instance.
         */
        public function PaginationKeyDown( pagination:PaginationLabel = null )
        {
            this.pagination = pagination ;
        }
        
        /**
         * Return the _pagination reference.
         */
        public function get pagination():PaginationLabel
        {
            return _pagination ;
        }
        
        /**
         * @private
         */
        public function set pagination( pagination:PaginationLabel ) : void
        {
            if( _pagination )
            {
                _pagination.removeEventListener( Event.ADDED_TO_STAGE, addedToStage ) ;
                _pagination.removeEventListener( Event.REMOVED_FROM_STAGE, removedFromStage ) ;
                removedFromStage() ;
            }
            _pagination = pagination ;
            if( _pagination )
            {
                _pagination.addEventListener( Event.ADDED_TO_STAGE, addedToStage, false , 0, true ) ;
                _pagination.addEventListener( Event.REMOVED_FROM_STAGE, removedFromStage, false, 0, true ) ;
                addedToStage() ;
            }
        }
        
        /**
         * Handles the event.
         */
        public function handleEvent( e:Event ):void
        {
            var event:KeyboardEvent = e as KeyboardEvent ;
            if( event )
            {
                var code:uint = event.keyCode ;
                var builder:PaginationLabelBuilder = pagination.builder as PaginationLabelBuilder ;
                if( builder )
                {
                    var textField:TextField = builder.textField ;
                    if( textField && _stage && _stage.focus == builder.textField )
                    {
                        switch( code )
                        {
                            case Keyboard.LEFT :
                            {
                                if ( pagination.current > 1 )
                                {
                                    pagination.current-- ;
                                }
                                break ; 
                            }
                            case Keyboard.RIGHT :
                            {
                                if ( pagination.current < pagination.total )
                                {
                                    pagination.current++ ;
                                }
                                break ;
                            }
                        }
                    }
                }
            }
        }
        
        /**
         * Invoked when the display is added to the stage.
         */
        protected function addedToStage( e:Event = null ):void
        {
            if( pagination && pagination.stage )
            {
                _stage = pagination.stage ;
                _stage.addEventListener( KeyboardEvent.KEY_DOWN , handleEvent ) ;
            }
        }
        
        /**
         * Invoked when the display is removed from the stage.
         */
        protected function removedFromStage( e:Event = null ):void
        {
            if( _stage )
            {
                _stage.removeEventListener( KeyboardEvent.KEY_DOWN , handleEvent ) ;  
                _stage = null ;
            }
        }
        
        /**
         * @private
         */
        protected var _pagination:PaginationLabel ;
        
        /**
         * @private
         */
        protected var _stage:Stage ;
    }
}
