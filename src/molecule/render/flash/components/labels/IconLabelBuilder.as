﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels
{
    import graphics.Align;

    import molecule.LabelPolicy;

    import system.hack;

    import flash.display.Bitmap;
    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.InteractiveObject;
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.SecurityErrorEvent;
    import flash.net.URLRequest;
    
    use namespace hack ;
    
    /**
     * The builder of the IconLabel component.
     */
    public class IconLabelBuilder extends LabelBuilder
    {
        /**
         * Creates a new IconLabelBuilder instance.
         * @param target The component to build and update.
         */
        public function IconLabelBuilder( target:IconLabel = null )
        {
            super( target ) ;
        }
        
        /**
         * The icon of the builder.
         */
        public var icon:DisplayObject ;
        
        /**
         * The loader of the builder.
         */
        public var loader:Loader ;
        
        /**
         * Attach a new icon in the component.
         */
        public function attachIcon( display:DisplayObject ):void
        {
            icon = display ;
            var comp:Label = target as Label ;
            if( comp )
            {
                if( icon )
                {
                    if ( icon is InteractiveObject )
                    {
                        (icon as InteractiveObject).mouseEnabled = false ;
                    }
                    (target as DisplayObjectContainer).addChild( icon ) ;
                }
                comp.update() ;
            }
        }
        
        /**
         * Clear the component.
         */
        public override function clear():void
        {
            super.clear() ;
            releaseIcon() ;
        }
        
        /**
         * @private
         * Loads an external icon to create the view of the icon.
         */
        public function loadIcon( request:URLRequest ):void
        {
            if( request )
            {
                loader = new Loader() ;
                loader.contentLoaderInfo.addEventListener( Event.INIT                        , initializeIcon  ) ;
                loader.contentLoaderInfo.addEventListener( IOErrorEvent.IO_ERROR             , initializeIcon ) ;
                loader.contentLoaderInfo.addEventListener( SecurityErrorEvent.SECURITY_ERROR , initializeIcon ) ;
                loader.load( request ) ;
            }
            else
            {
                update() ;
            }
        }
        
        /**
         * Release the current icon reference.
         */
        public function releaseIcon():void
        {
            if( loader )
            {
                loader.close() ;
            }
            if( icon )
            {
                if( icon is Bitmap && (icon as Bitmap).bitmapData )
                {
                    (icon as Bitmap).bitmapData.dispose() ;
                }
                if ( (target as DisplayObjectContainer).contains( icon ) )
                {
                    (target as DisplayObjectContainer).removeChild( icon ) ;
                }
                icon = null ;
            }
        }
        
        /**
         * Update the component.
         */
        public override function update():void
        {
            updateTextField() ;
            updateIcon() ;
            updateLayout() ;
        }
        
        /**
         * Update the icon in the component.
         * @private
         */
        public function updateIcon():void
        {
            var comp:Label = target as Label ;
            if( comp )
            {
                var style:IconLabelStyle = comp.style as IconLabelStyle ;
                if( style )
                {
                    if( icon && textField )
                    {
                        if( style.iconPolicy == LabelPolicy.AUTO )
                        {
                            icon.height = textField.height ;
                            icon.scaleX = icon.scaleY ;
                        }
                        else
                        {
                            //icon.scaleX = icon.scaleY = 1 ;
                        }
                        
                        textField.width -= icon.width ;
                        textField.width -= style.iconMargin.horizontal ;
                        textField.getCharBoundaries(0) ;
                        
                        comp.draw() ;
                    }
                }
            }
        }
        
        /**
         * Update the elements in the component.
         * @private
         */
        public override function updateLayout():void
        {
            super.updateLayout() ;
            if( icon && textField )
            {
                var comp:Label = target as Label ;
                if( comp )
                {
                    var style:IconLabelStyle = comp.style as IconLabelStyle ;
                    if( style )
                    {
                        const $x:Number = comp._real.x ;
                        const $y:Number = comp._real.y ;
                        const $h:Number = comp.h ;
                        
                        switch( style.iconAlign )
                        {
                            case Align.LEFT :
                            {
                                icon.x = $x + style.iconMargin.left ;
                                icon.y = $y + ($h - icon.height) / 2 ;
                                textField.x = icon.x + icon.width + style.iconMargin.right + style.padding.left ;
                                break ;
                            }
                            case Align.RIGHT :
                            {
                                icon.x = textField.x + textField.width + style.padding.right + style.iconMargin.left ;
                                icon.y = $y + ($h - icon.height) / 2 ;
                                break ;
                            }
                            case Align.TOP_LEFT :
                            {
                                icon.x = $x + style.iconMargin.left ;
                                icon.y = $y + style.iconMargin.top ;
                                textField.x = icon.x + icon.width + style.iconMargin.right + style.padding.left ;
                                break ;
                            }
                            case Align.TOP_RIGHT :
                            {
                                icon.x = textField.x + textField.width + style.padding.right + style.iconMargin.left ;
                                icon.y = $y + style.iconMargin.top ;
                                break ;
                            }
                            case Align.BOTTOM_LEFT :
                            {
                                icon.x = $x + style.iconMargin.left ;
                                icon.y = $y + $h - icon.height - style.iconMargin.bottom ;
                                textField.x = icon.x + icon.width + style.iconMargin.right + style.padding.left ;
                                break ;
                            }
                            case Align.BOTTOM_RIGHT :
                            {
                                icon.x = textField.x + textField.width + style.padding.right + style.iconMargin.left ;
                                icon.y = $y + $h - icon.height - style.iconMargin.bottom ;
                                break ;
                            }
                        }
                    }
                }
            }
        }
        
        /**
         * @private
         */
        protected function initializeIcon( e:Event ):void
        {
            loader.contentLoaderInfo.removeEventListener( Event.INIT                        , initializeIcon ) ;
            loader.contentLoaderInfo.removeEventListener( IOErrorEvent.IO_ERROR             , initializeIcon ) ;
            loader.contentLoaderInfo.removeEventListener( SecurityErrorEvent.SECURITY_ERROR , initializeIcon ) ;
            
            attachIcon( loader.content ) ;
            
            loader = null ;
        }
    }
}
