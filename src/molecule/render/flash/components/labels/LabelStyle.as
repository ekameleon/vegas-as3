﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels
{
    import graphics.Align;
    import graphics.geom.EdgeMetrics;

    import molecule.LabelPolicy;
    import molecule.Stylus;

    import system.hack;

    import flash.text.AntiAliasType;
    import flash.text.GridFitType;
    import flash.text.TextFieldAutoSize;
    
    use namespace hack ;
    
    /**
     * The style of the label component.
     */
    public class LabelStyle extends Stylus
    {
        /**
         * Creates a new LabelStyle instance.
         * @param init An object that contains properties with which to populate the newly Style object. If init is not an object, it is ignored.
         */
        public function LabelStyle( init:* = null )
        {
            super( init ) ;
        }
        
        /**
         * The antiAliasType value of the label field of the component.
         */
        public var antiAliasType:String = AntiAliasType.NORMAL ;
         
        /**
         * Indicates the autosize value of the label field of the component.
          */
        public var autoSize:String = TextFieldAutoSize.NONE ;
        
        /**
         * The default color of the field of the component.
         */
        public var color:Number = 0x000000 ;
        
        /**
         * A Boolean value that specifies whether extra white space (spaces, line breaks, and so on) in a text field with HTML text is removed. The default value is false. The condenseWhite property only affects text set with the htmlText property, not the text property. If you set text with the text property, condenseWhite is ignored.
         */
        public var condenseWhite:Boolean ;
        
        /**
         * Indicates the embedFonts value of the label field of the component.
         */
        public var embedFonts:Boolean ;
        
        /**
         * The gridFitType value of the label field of the component.
         */
        public function get gridFitType():String
        {
            return _gridFitType ;
        }
        
        /**
         * @private
         */
        public function set gridFitType( value:String ):void
        {
            _gridFitType = value == GridFitType.NONE || value == GridFitType.SUBPIXEL ? value : GridFitType.PIXEL ;
        }
        
        /**
         * Indicates the height value of the label field of the component.
         */
        public var height:Number ;
        
        /**
         * Indicates the html value of the label field of the component.
         */
        public var html:Boolean = true ;
        
        /**
         * Indicates if the text in the button is multiline.
         */
        public var multiline:Boolean ;
        
        /**
         * The padding between the field in the area and the background.
         */
        public function get padding():EdgeMetrics
        {
            return _padding ;
        }
        
        /**
         * @private
         */
        public function set padding( em:EdgeMetrics ):void
        {
           _padding.bottom = em ? em.bottom : 0 ;
           _padding.left   = em ? em.left   : 0 ;
           _padding.right  = em ? em.right  : 0 ;
           _padding.top    = em ? em.top    : 0 ;
        }
        
        /**
         * The policy of the area (default "normal").
         * <p>This property can take the values :</p>
         * <ul>
         * <li>LabelPolicy.NORMAL "normal"</li> 
         * <li>LabelPolicy.AUTO "normal"</li>
         * </ul>
         */
        public function get policy():String
        {
            return _policy ;
        }
        
        /**
         * @private
         */
        public function set policy( value:String ):void
        {
           _policy = value == LabelPolicy.AUTO ? LabelPolicy.AUTO : LabelPolicy.NORMAL ;
        }
        
        /**
         * Indicates the selectable value of the label field of the component.
         */
        public var selectable:Boolean ;
        
        /**
         * Indicates if the border of the field is visible.
         */
        public var textBorder:Boolean ;
        
        /**
         * The color of the field border.
         */
        public var textBorderColor:Number ;
        
        /**
         * The color of the text when is disabled.
         */
        public var textDisabled:Number = 0xCCCCCC ;
        
        /**
         * The alignement of the text in the component (can be Align.CENTER, Align.BOTTOM or Align.TOP only and by default Align.NONE).
         * @see graphics.Align
         */
        public function get textVerticalAlign():uint
        {
            return _textVerticalAlign ;
        }
        
        /**
         * @private
         */
        public function set textVerticalAlign( value:uint ):void
        {
            _textVerticalAlign = textAlignments.indexOf( value ) > -1 ? value : Align.NONE ;
        }
        
        /**
         * The thickness of the glyph edges in this text field.
         */
        public var thickness:Number = 0 ;
        
        /**
         * Indicates if the component use the style text colors.
         */
        public var useTextColor:Boolean ;
        
        /**
         * Indicates the width value of the label field of the component.
         */
        public var width:Number ;
        
        /**
         * The wordwrap value of the fields.
         */
        public var wordWrap:Boolean ;
        
        /**
         * @private
         */
        hack var _gridFitType:String = GridFitType.PIXEL ;
        
        /**
         * @private
         */
        hack const _padding:EdgeMetrics = new EdgeMetrics(2,2,2,2) ;
        
        /**
         * @private
         */
        hack var _policy:String = LabelPolicy.NORMAL ;
        
        /**
         * The valid alignment of the text in the box.
         * @private
         */
        hack static const textAlignments:Vector.<uint> = Vector.<uint>
        ([
            Align.NONE,
            Align.CENTER,
            Align.BOTTOM , 
            Align.TOP 
        ]) ;
        
        /**
         * @private
         */
        hack var _textVerticalAlign:uint = Align.NONE ;
    }
}
