﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels
{
    import graphics.Align;
    import graphics.geom.EdgeMetrics;
    
    import molecule.LabelPolicy;
    import molecule.render.flash.CoreBuilder;
    
    import system.hack;
    
    import flash.text.AntiAliasType;
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    
    use namespace hack ;
    
    /**
     * The builder of the label component.
     */
    public class LabelBuilder extends CoreBuilder
    {
        /**
         * Creates a new LabelBuilder instance.
         * @param target The component to build and update.
         */
        public function LabelBuilder( target:Label = null )
        {
            super( target ) ;
        }
        
        /**
         * @private
         */
        public var textField:TextField ;
        
        /**
         * Clear the component.
         */
        public override function clear():void
        {
            const comp:Label = target as Label ;
            if( textField )
            {
                if( comp && comp.contains( textField ) )
                {
                    comp.removeChild( textField ) ;
                }
                textField = null ;
            }
        }
        
        /**
         * Run the component building.
         */
        public override function run(...args:*):void
        {
            textField = new TextField() ;
            if( target is Label )
            {
                (target as Label).addChild( textField ) ;
            }
        }
        
        /**
         * Update the component.
         */
        public override function update():void
        {
            updateTextField() ;
            updateLayout() ;
        }
        
        /**
         * Update the elements in the component.
         * @private
         */
        public function updateLayout():void
        {
            var comp:Label = target as Label ;
            if( comp )
            {
                var style:LabelStyle = comp.style as LabelStyle ;
                if( style )
                {
                    var padding:EdgeMetrics = style.padding ;
                    
                    textField.getCharBoundaries(0) ;
                    
                    if( style.policy == LabelPolicy.AUTO )
                    {
                        textField.height = textField.textHeight ;
                        comp._h = textField.height + padding.vertical ;
                    }
                    
                    comp.fixArea() ;
                    
                    const $x:Number = comp._real.x ;
                    const $y:Number = comp._real.y ;
                    const $h:Number = comp.h ;
                    
                    textField.x = $x + padding.left  ;
                    textField.y = $y ;
                    
                    switch( style._textVerticalAlign )
                    {
                        case Align.CENTER :
                        {
                            textField.y += ($h - textField.textHeight) / 2 ;
                            break ;
                        }
                        
                        case Align.BOTTOM :
                        {
                            textField.y += $h - textField.textHeight - padding.bottom ;
                            break ;
                        }
                        
                        default :
                        case Align.NONE :
                        case Align.TOP :
                        {
                            textField.y += padding.top ;
                            break ;
                        }
                    }
                }
            }
        }
        
        /**
         * Update the text field.
         * @private
         */
        public function updateTextField():void
        {
            var comp:Label = target as Label ;
            if( comp )
            {
                var style:LabelStyle = comp.style as LabelStyle ;
                if( style )
                {
                    var txt:String = getLabelExpression() ;
                    
                    textField.width         = comp.w - style.padding.horizontal ;
                    textField.height        = comp.h - style.padding.vertical ;
                    
                    textField.styleSheet    = style.styleSheet ;
                    
                    textField.antiAliasType = style.antiAliasType ? style.antiAliasType : AntiAliasType.NORMAL ;
                    textField.autoSize      = style.autoSize ? style.autoSize : TextFieldAutoSize.NONE ;
                    textField.border        = style.textBorder ;
                    textField.borderColor   = style.textBorderColor ;
                    textField.condenseWhite = style.condenseWhite;
                    textField.embedFonts    = style.embedFonts ;
                    textField.gridFitType   = style.gridFitType ;
                    textField.multiline     = style.multiline ;
                    textField.selectable    = style.selectable ;
                    textField.thickness     = style.thickness ;
                    textField.wordWrap      = style.wordWrap ;
                    
                    if ( style.html )
                    {
                        textField.htmlText = txt ;
                    }
                    else
                    {
                        textField.text = txt ;
                    }
                    
                    textField.visible = (txt != null ) && (txt != "") ;
                    
                    if ( style.useTextColor )
                    {
                        textField.textColor = comp.enabled ? style.color : style.textDisabled ;
                    }
                }
            }
        }
        
        /**
         * The custom function who returns the label expression of the component.
         */
        protected function getLabelExpression():String
        {
            return (target as Label).label != null ? (target as Label).label : "" ;
        }
    }
}
