﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels
{
    import graphics.geom.EdgeMetrics;

    import flash.text.StyleSheet;
    
    /**
     * The style of the PaginationLabel component.
     */
    public class PaginationLabelStyle extends LabelStyle 
    {
        /**
         * Creates a new PaginationLabelStyle instance.
         * @param init An object that contains properties with which to populate the newly Style object. If init is not an object, it is ignored.
         */
        public function PaginationLabelStyle( init:* = null )
        {
            super( init );
        }
        
        //////////////
        
        /**
         * The default css string expression.
         */
        public static const DEFAULT_CSS_EXPRESSION:String = 
        <![CDATA[
         p{font-family:Verdana;font-size:11px;color:#FFFFFF;font-weight:bold;}
        .pagination_label_current{font-size:10px;color:#FFFFFF;font-weight:bold;}
        .pagination_label_first{font-size:10px;color:#FCF3AD;font-weight:bold;}
        .pagination_label_last{font-size:10px;color:#FCF3AD;font-weight:bold;}
        .pagination_label_next{font-size:10px;color:#CCCCCC;font-weight:bold;}
        .pagination_label_previous{font-size:10px;color:#CCCCCC;font-weight:bold;}
        .pagination_label_body{font-size:10px;color:#FFFF66;}
        a:hover{font-size:10px;color:#BABA01;}
        ]]>;
        
        //////////////
        
        /**
         * The stylesheet style name of the current page label.
         */
        public var currentStyleName:String = "pagination_label_current" ;
        
        /**
         * The first pattern
         */
        public var firstPattern:String = "<a class='pagination_label_first' href='event:{0}'>{1} </a>" ;
        
        /**
         * The last pattern
         */
        public var lastPattern:String = "<a class='pagination_label_last' href='event:{0}'>{1}</a>" ;
        
        /**
         * The link pattern
         */
        public var linkPattern:String = "<a class='pagination_label_body' href='event:{0}'>{1}</a>" ;
        
        /**
         * The next pattern
         */
        public var nextPattern:String = "<a class='pagination_label_next' href='event:{0}'>{1} </a>" ;
        
        /**
         * The pagination pattern with the expressions: {title} {first} {previous} {body} {next} {last}.
         */
        public var pattern:String = "{title} {first} {previous} {body} {next} {last}" ;
        
        /**
         * The previous pattern
         */
        public var previousPattern:String = "<a class='pagination_label_previous' href='event:{0}'>{1} </a>" ;
        
        /**
         * The separator between two links.
         */
        public var separator:String = " " ;
        
        /**
         * Initialize the Style object.
         */
        public override function initialize():void 
        {
            /////
            
            multiline    = false ;
            useTextColor = false ;
            wordWrap     = true ;
            
            /////
            
            padding = new EdgeMetrics( 2, 2, 2, 2 ) ;
            
            /////
            
            styleSheet   = new StyleSheet() ;
            styleSheet.parseCSS(  DEFAULT_CSS_EXPRESSION  ) ;
        }
    }
}
