﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels
{
    import molecule.Builder;
    import molecule.Iconifiable;
    import molecule.Style;

    import flash.display.DisplayObject;
    import flash.net.URLRequest;
    
    /**
     * The label component with an icon.
     * <p><b>Example :</b></p>
     * <pre class="prettyprint">
     * import molecule.render.flash.components.labels.IconLabel ;
     * import molecule.render.flash.components.labels.IconLabelStyle ;
     * 
     * import graphics.Align ;
     * import graphics.FillStyle ;
     * import graphics.display.Background ;
     * import graphics.geom.EdgeMetrics ;
     * 
     * import flash.text.StyleSheet ;
     * 
     * /////////////
     * 
     * var icon:Background = new Background() ;
     * 
     * icon.fill = new FillStyle( 0xFF0000 );
     * icon.setPreferredSize(20,20) ;
     * 
     * /////////////
     * 
     * var styleSheet:StyleSheet = new StyleSheet() ;
     * 
     * styleSheet.parseCSS
     * (&lt;![CDATA[
     * p
     * {
     *     color: #FFFFFF;
     *     fontFamily: Arial;
     *     size: 12px;
     * }
     * ]]&gt;);
     * 
     * /////////////
     * 
     * var init:Object =
     * {
     *     antiAliasType     : "advanced" , // normal, advanced
     *     autoSize          : "left" ,
     *     embedFonts        : false ,
     *     gridFitType       : "subpixel" , // none, pixel, subpixel
     *     html              : true ,
     *     iconAlign         : Align.LEFT ,
     *     iconMargin        : new EdgeMetrics(8,4,0,4) ,
     *     iconPolicy        : "normal" , // auto or normal
     *     multiline         : true ,
     *     padding           : new EdgeMetrics(8,8,8,8) ,
     *     policy            : "auto" , // auto or normal
     *     selectable        : false ,
     *     textBorder        : false ,
     *     textVerticalAlign : Align.CENTER ,
     *     wordWrap          : true
     * }
     * 
     * var style:IconLabelStyle = new IconLabelStyle( init ) ;
     * 
     * style.styleSheet = styleSheet ;
     * 
     * /////////////
     * 
     * var component:IconLabel = new IconLabel() ;
     * 
     * component.lock() ;
     * 
     * component.align = Align.CENTER ;
     * 
     * component.fill  = new FillStyle( 0xCCCCCC , 0.2 ) ;
     * component.style = style ;
     * 
     * component.x = 300 ;
     * component.y = 100 ;
     * component.w = 250 ;
     * component.h = 60 ;
     * 
     * component.label = "&lt;p&gt;hello world&lt;/p&gt;" ;
     * component.icon  = icon ;
     * 
     * component.unlock() ;
     * 
     * component.update() ;
     * 
     * addChild( component ) ;
     * 
     * /////////////
     * 
     * var alignments:Vector.&lt;int&gt; = Vector.&lt;int&gt;
     * ([
     *     Align.TOP_LEFT, Align.TOP, Align.TOP_RIGHT,
     *     Align.RIGHT,
     *     Align.BOTTOM_RIGHT, Align.BOTTOM, Align.BOTTOM_LEFT,
     *     Align.LEFT, Align.CENTER
     * ]) ;
     * 
     * stage.addEventListener( MouseEvent.CLICK , change ) ;
     * 
     * var index:int ;
     * 
     * function change( e:Event ):void
     * {
     *     if( index >= alignments.length )
     *     {
     *         index = 0 ;
     *     }
     *     component.align = alignments[index++] ;
     * }
     * </pre>
     */
    public class IconLabel extends Label implements Iconifiable
    {
        /**
         * Creates a new IconLabel instance.
         * @param init An object that contains properties with which to populate the newly instance. If init is not an object, it is ignored.
         */
        public function IconLabel( init:Object = null )
        {
            super( init );
        }
        
        /**
         * Indicates the icon reference of the area.
         */
        public function get icon():*
        {
            return (builder as IconLabelBuilder).icon ;
        }
        
        /**
         * @private
         */
        public function set icon( value:* ):void
        {
            var b:IconLabelBuilder = builder as IconLabelBuilder ;
            if( b )
            {
                b.releaseIcon() ;
                if ( value is DisplayObject )
                {
                    b.attachIcon( value ) ;
                }
                else if ( value is URLRequest )
                {
                    b.loadIcon( value as URLRequest ) ;
                }
            }
        }
        
        /**
         * Returns the Builder constructor use to initialize this component.
         * @return the Builder constructor use to initialize this component.
         */
        protected override function getBuilderRenderer():Builder 
        {
            return new IconLabelBuilder( this ) ;
        } 
        
        /**
         * Returns the Style constructor use to initialize this component.
         * @return the Style constructor use to initialize this component.
         */
        protected override function getStyleRenderer():Style 
        {
            return new IconLabelStyle() ;
        }
    }
}
