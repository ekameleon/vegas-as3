﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels
{
    import core.html.paragraph;
    import core.html.span;
    import core.strings.format;
    
    import flash.events.TextEvent;
    
    /**
     * The builder of the PaginationLabel component.
     */
    public class PaginationLabelBuilder extends LabelBuilder 
    {
        /**
         * Creates a new PaginationLabelBuilder instance.
         * @param target the target of the component reference to build.
         */
        public function PaginationLabelBuilder( target:PaginationLabel = null )
        {
            super( target ) ;
        }
        
        /**
         * Runs the build of the component.
         */
        public override function run(...arguments:Array):void
        {
            super.run.apply(this, arguments) ;
            textField.mouseEnabled = true ;
            textField.addEventListener( TextEvent.LINK , onLink ) ;
        }
        
        /**
         * The custom function who returns the label expression of the component.
         */
        protected override function getLabelExpression():String
        {
            if( (target as Label).label )
            {
                return super.getLabelExpression() ;
            }
            else
            {
                var b:PaginationLabel = target as PaginationLabel ;
                var m:PaginationModel = b.model as PaginationModel ;
                var pages:Array        = m.pages ; 
                if ( b && m && pages )
                {
                    var o:Object = 
                    { 
                        body     : "" ,
                        first    : "" ,
                        last     : "" , 
                        next     : "" , 
                        previous : "" , 
                        title    : b.title || ""
                    } ;
                    var c:uint                  = m.current ;
                    var s:PaginationLabelStyle  = b.style as PaginationLabelStyle ;
                    
                    if( c > 1 && b.firstLabel != null && b.firstLabel.length > 0 )
                    {
                        o.first = format( s.firstPattern, 1 , b.firstLabel ) ; 
                    }
                    
                    if( c > 1 && b.previousLabel != null && b.previousLabel.length > 0 )
                    {
                        o.previous = format( s.previousPattern, m.current - 1 , b.previousLabel ) ; 
                    }
                    
                    o.body = "" ;
                    
                    var i:uint ;
                    var size:uint  = pages.length ;
                    var value:uint ; 
                    
                    for ( i = 0 ; i < size ; i++ )
                    {
                        value = pages[i] ;
                        if ( value == c )
                        {
                            o.body += span( value.toString() , s.currentStyleName ) ;
                        }
                        else
                        {
                            o.body += format( s.linkPattern , value, value ) ;
                        }
                        if ( i != (size-1) )
                        {
                            o.body += s.separator || "" ;
                        }
                    }
                    
                    if( m.current < m.total  && b.nextLabel != null && b.nextLabel.length > 0 )
                    {
                        o.next = format( s.nextPattern , m.current + 1 , b.nextLabel ) ;
                    }
                    
                    if( m.current < m.total && b.lastLabel != null && b.lastLabel.length > 0 )
                    {
                        o.last = format( s.lastPattern, m.total , b.lastLabel ) ; 
                    }
                    
                    return paragraph( format( s.pattern , o ) ) ;
                }
                else
                {
                    return "" ;
                }
            }
        }
        
        /**
         * Invoked when a link is selected in the field.
         */
        protected function onLink( e:TextEvent=null ):void 
        {
            (target as PaginationLabel).current = uint(e.text) ;
        }
    }
}
