﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels
{
    import molecule.Builder;
    import molecule.Style;

    import system.signals.Signal;
    import system.signals.Signaler;
    
    /**
     * The pagination label component.
     * <p><b>Example:</b></p>
     * <pre class="prettyprint">
     * import molecule.render.flash.components.labels.PaginationLabel;
     * import molecule.render.flash.components.labels.PaginationLabelStyle;
     * 
     * import graphics.FillStyle ;
     * import graphics.geom.EdgeMetrics ;
     * 
     * ///////
     * 
     * function select( component:PaginationLabel ):void
     * {
     *     trace( "select: " + component.current ) ;
     * }
     * 
     * ///////
     * 
     * var style:PaginationLabelStyle = new PaginationLabelStyle
     * ({
     *     antiAliasType : "advanced" , // normal, advanced
     *     autoSize      : "left" ,
     *     border        : false ,
     *     embedFonts    : false ,
     *     gridFitType   : "subpixel" , // none, pixel, subpixel
     *     html          : true ,
     *     multiline     : true ,
     *     padding       : new EdgeMetrics(8,8,8,8) ,
     *     policy        : "auto" , // auto or normal
     *     selectable    : false ,
     *     wordWrap      : true
     * }) ;
     * 
     * ///////
     * 
     * var component:PaginationLabel = new PaginationLabel() ;
     * 
     * component.selected.connect( select ) ;
     * 
     * component.style = style ;
     * 
     * component.fill = new FillStyle( 0xCCCCCC , 0.2 ) ;
     * 
     * component.w = 500 ;
     * component.h = 40 ;
     * 
     * //component.label = "<p>hello world !</p>" ;
     * 
     * component.set( 15, 30, 4) ; // current,limit,total
     * 
     * component.firstLabel    = "Premier" ;
     * component.lastLabel     = "Dernier" ;
     * component.nextLabel     = "Suivant" ;
     * component.previousLabel = "Précédent" ;
     * 
     * component.x = 50  ;
     * component.y = 50  ;
     * 
     * addChild( component ) ;
     * 
     * /////// optional
     * 
     * import molecule.render.flash.components.labels.PaginationKeyDown;
     * 
     * var controller:PaginationKeyDown = new PaginationKeyDown( component ) ;
     * </pre>
     */
    public class PaginationLabel extends Label
    {
        /**
         * Creates a new PaginationLabel instance.
         * @param init An object that contains properties with which to populate the newly instance. If init is not an object, it is ignored.
         */
        public function PaginationLabel( init:Object = null )
        {
            _selected = new Signal() ;
            lock() ;
            model = new PaginationModel() ;
            unlock() ;
            super( init ) ;
        }
        
        /**
         * Indicates the current page number.
         */
        public function get current():uint 
        {
            return _model.current ;
        }
        
        /**
         * @private
         */
        public function set current( value:uint ):void
        {
            _model.current = value ;
            _selected.emit( this ) ;
        }
        
        /**
         * Indicates the first label value of this display.
         */
        public function get firstLabel():String
        {
            return _firstLabel ;
        }
            
        /**
         * @private
         */
        public function set firstLabel( str:String ):void
        {
            _firstLabel = (str != null) ? str : "" ;
            update() ;
        }
        
        /**
         * Indicates the last label value of this display.
         */
        public function get lastLabel():String
        {
            return _lastLabel  ;
        }
        
        /**
         * @private
         */
        public function set lastLabel( str:String ):void
        {
            _lastLabel = (str != null) ? str : "" ;
            update() ;
        }
        
        /**
         * Indicates the array representation of all page numbers with the current navigator values. 
         */
        public function get pages():Array 
        {
            return _model.pages ;
        }
        
        /**
         * Indicates the limit until the current pages.
         */
        public function get limit() : uint
        {
            return _model.limit ;
        }
        
        /**
         * @private
         */
        public function set limit( value:uint ) : void
        {
           _model.limit = value ;
        }
        
        /**
         * Indicates the next label value of this display.
         */
        public function get nextLabel():String
        {
            return _nextLabel ;
        }
            
        /**
         * @private
          */
        public function set nextLabel( str:String ):void
        {
            _nextLabel = (str != null) ? str : "" ;
            update() ;
        }
        
        /**
         * Defines the pagination model of this component.
         */
        public function get model():PaginationModel
        {
            return _model ;
        }
        
        /**
         * @private
         */
        public function set model( pagination:PaginationModel ) : void
        {
            if ( _model )
            {
                _model.changed.disconnect( _change ) ;
            }
            _model = pagination ? pagination : new PaginationModel() ;
            _model.changed.connect( _change ) ;
            if ( !isLocked() )
            {
                _model.run() ;
            }
        }
        
        /**
         * Indicates the previous label value of this display.
         */
        public function get previousLabel():String
        {
            return _previousLabel ;
        }
            
        /**
         * @private
          */
        public function set previousLabel( str:String ):void
        {
            _previousLabel = (str != null) ? str : "" ;
            update() ;
        }
        
        /**
         * Emits a message when a current page is selected in the component.
         */
        public function get selected():Signaler
        {
            return _selected ;
        }
        
        /**
         * Indicates the title value of this component.
         */
        public function get title():String
        {
            return _title ;
        }
            
        /**
         * @private
          */
        public function set title( str:String ):void
        {
            _title = (str != null) ? str : "" ;
            update() ;
        }
        
        /**
         * Indicates the pages total number.
         */
        public function get total():uint
        {
            return _model.total ;
        }
        
        /**
         * @private
         */
        public function set total( value:uint ) : void
        {
            _model.total = value ;
        }
        
        /**
         * Sets the pagination navigator inside the title.
         */
        public function set( current:uint=1 , total:uint=1 , limit:uint=5 ):void
        {
            _model.set( current, total, limit ) ;
        }
        
        /**
         * Returns the default builder to initialize and create the component.
         * @return the default builder to initialize and create the component.
         */
        protected override function getBuilderRenderer():Builder 
        {
            return new PaginationLabelBuilder( this ) ;
        }
        
        /**
         * Returns the default style reference to initialize and create the component.
         * @return the default style reference to initialize and create the component.
         */
        protected override function getStyleRenderer():Style 
        {
            return new PaginationLabelStyle() ;
        }
        
        /**
         * @private
         */
        private var _firstLabel:String = "First" ;
        
        /**
         * @private
         */
        private var _lastLabel:String = "Last" ;
        
        /**
         * @private
         */
        private var _nextLabel:String = "Next" ;
        
        /**
         * @private
         */
        private var _model:PaginationModel ;
        
        /**
         * @private
         */
        private var _previousLabel:String = "Previous" ;
        
        /**
         * @private
         */
        protected var _selected:Signaler ;
        
        /**
         * @private
         */
        private var _title:String = "Pages" ;
        
        /**
         * @private
         */
        protected function _change( pages:Array , model:PaginationModel ):void
        {
            update() ;
        }
    }
}
