﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.labels
{
    import graphics.Align;
    import graphics.geom.EdgeMetrics;
    import molecule.LabelPolicy;
    
    /**
     * The style of the icon label component.
     */
    public class IconLabelStyle extends LabelStyle
    {
        /**
         * Creates a new IconLabelStyle instance.
         * @param init An object that contains properties with which to populate the newly Style object. If init is not an object, it is ignored.
         */
        public function IconLabelStyle( init:* = null )
        {
            super( init ) ;
        }
        
        
        /**
         * Indicates the icon alignment in the area (default Align.LEFT). 
         * <p>This alignment is defines with the Align enumeration class with the values :</p>
         * <ul>
         * <li>Align.BOTTOM_LEFT</li>
         * <li>Align.BOTTOM_RIGHT</li>
         * <li>Align.LEFT</li>
         * <li>Align.RIGHT</li>
         * <li>Align.TOP_LEFT</li>
         * <li>Align.TOP_RIGHT</li>
         * <li>Align.NONE</li>
         * </ul>
         */
        public function get iconAlign():uint
        {
            return _iconAlign ;
        }
        
        /**
         * @private
         */
        public function set iconAlign( value:uint ):void
        {
            _iconAlign = alignments.indexOf( value ) > -1 ? value : Align.NONE ;
        }
        
        /**
         * The margin between the icon and the elements.
         */
        public function get iconMargin():EdgeMetrics
        {
            return _iconMargin ;
        }
        
        /**
         * @private
         */
        public function set iconMargin( em:EdgeMetrics ):void
        {
           _iconMargin.bottom = em ? em.bottom : 0 ;
           _iconMargin.left   = em ? em.left   : 0 ;
           _iconMargin.right  = em ? em.right  : 0 ;
           _iconMargin.top    = em ? em.top    : 0 ;
        }
        
        /**
         * The policy of the icon in the areaarea (default "normal").
         * <p>This property can take the values :</p>
         * <ul>
         * <li>LabelPolicy.NORMAL "normal" : keep this size</li> 
         * <li>LabelPolicy.AUTO "auto" : take the size of the field.</li>
         * </ul>
         */
        public function get iconPolicy():String
        {
            return _iconPolicy ;
        }
        
        /**
         * @private
         */
        public function set iconPolicy( value:String ):void
        {
           _iconPolicy = value == LabelPolicy.AUTO ? LabelPolicy.AUTO : LabelPolicy.NORMAL ;
        }
        
        /**
         * The valid alignment of the icon in the area.
         * @private
         */
        protected const alignments:Vector.<uint> = Vector.<uint>
        ([
            Align.BOTTOM_LEFT , 
            Align.BOTTOM_RIGHT,
            Align.LEFT, 
            Align.RIGHT, 
            Align.TOP_LEFT, 
            Align.TOP_RIGHT
        ]) ;
        
        /**
         * @private
         */
        protected var _iconAlign:uint = Align.LEFT ;
        
        /**
         * @private
         */
        protected const _iconMargin:EdgeMetrics = new EdgeMetrics( 2, 2, 2, 2 ) ;
        
        /**
         * @private
         */
        protected var _iconPolicy:String = LabelPolicy.NORMAL ;
    }
}
