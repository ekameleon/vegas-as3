﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.buttons
{
    import graphics.FillStyle;
    import graphics.IFillStyle;
    import graphics.ILineStyle;
    import graphics.LineStyle;
    import graphics.easings.expoOut;
    import graphics.transitions.TweenTo;
    
    import flash.display.CapsStyle;
    import flash.display.JointStyle;
    import flash.display.LineScaleMode;
    
    /**
     * The style of the ThumbButton component.
     */
    public class ThumbButtonStyle extends LightButtonStyle 
    {
        /**
         * Creates a new ThumbButtonStyle.
         * @param init An object that contains properties with which to populate the newly Style object. If init is not an object, it is ignored. 
         */
        public function ThumbButtonStyle( init:* = null )
        {
            super( init );
            
        }
        
        /**
         * The theme of the thumb.
         */
        public var theme:IFillStyle = new FillStyle(0xFFFFFF,1) ;
        
        /**
         * The theme border of the thumb.
         */
        public var themeBorder:ILineStyle = new LineStyle(2,0xFFFFFF,1, true, LineScaleMode.NORMAL, CapsStyle.SQUARE, JointStyle.MITER ) ;
        
        /**
         * Invoked in the constructor of the <code class="prettyprint">IStyle</code> instance.
         */
        public override function initialize():void
        {
            tweenOver     = new TweenTo(null, { brightness : 0 } , expoOut, 1, true , false, { brightness : 70 } ) ;
            tweenSelected = new TweenTo(null, { brightness : 0 } , expoOut, 1, true , false, { brightness : 125 } ) ;
        }
    }
}
