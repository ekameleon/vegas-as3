﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.buttons
{
    import graphics.Align;
    import graphics.FillStyle;
    import graphics.IFillStyle;
    import graphics.ILineStyle;
    import graphics.LineStyle;
    import graphics.geom.EdgeMetrics;
    
    import molecule.Stylus;
    
    import system.hack;
    
    import flash.text.AntiAliasType;
    import flash.text.GridFitType;
    import flash.text.StyleSheet;
    import flash.text.TextFieldAutoSize;
    
    use namespace hack ;
    
    /**
     * This style of the BasicButton component.
     */
    public class BasicButtonStyle extends Stylus 
    {
        /**
         * Creates a new BasicButtonStyle instance.
         * @param init An object that contains properties with which to populate the newly Style object. If init is not an object, it is ignored.
         */
        public function BasicButtonStyle( init:* = null )
        {
            super( init );
        }
        
        /**
         * The alignment of the component.
         * @see graphics.Align
         */
        public var align:uint ;
        
        /**
         * The antiAliasType value of the label field of the component.
         */
        public var antiAliasType:String = AntiAliasType.NORMAL ;
         
        /**
         * Indicates the autosize value of the label field of the component.
          */
        public var autoSize:String = TextFieldAutoSize.NONE ;
        
        /**
         * The default color of the field of the component.
         */
        public var color:Number = 0xFFFFFF ;
        
        /**
         * A Boolean value that specifies whether extra white space (spaces, line breaks, and so on) in a text field with HTML text is removed. The default value is false. The condenseWhite property only affects text set with the htmlText property, not the text property. If you set text with the text property, condenseWhite is ignored.
         */
        public var condenseWhite:Boolean ;
        
        /**
         * Indicates if the fields embed fonts.
         */
        public var embedFonts:Boolean ;
        
        /**
         * The gridFitType value of the label field of the component.
         */
        public function get gridFitType():String
        {
            return _gridFitType ;
        }
        
        /**
         * @private
         */
        public function set gridFitType( value:String ):void
        {
            _gridFitType = value == GridFitType.NONE || value == GridFitType.SUBPIXEL ? value : GridFitType.PIXEL ;
        }
        
        /**
         * Indicates the html value of the label field of the component.
         */
        public var html:Boolean = true ;
        
        /**
         * The alignement of the icon in the component (can be Align.LEFT or Align.RIGHT only, default Align.LEFT).
         * @see graphics.Align
         */
        public function get iconAlign():uint
        {
            return _iconAlign ;
        }
        
        /**
         * @private
         */
        public function set iconAlign( value:uint ):void
        {
            _iconAlign = iconAlignments.indexOf( value ) > -1 ? value : Align.LEFT ;
        }
        
        /**
         * Indicates if the icon defines this size automatically.
         */
        public var iconAutoSize:Boolean ;
        
        /**
         * The margin between the icon and the elements.
         */
        public function get iconMargin():EdgeMetrics
        {
            return _iconMargin ;
        }
        
        /**
         * @private
         */
        public function set iconMargin( em:EdgeMetrics ):void
        {
           _iconMargin.bottom = em ? em.bottom : 0 ;
           _iconMargin.left   = em ? em.left   : 0 ;
           _iconMargin.right  = em ? em.right  : 0 ;
           _iconMargin.top    = em ? em.top    : 0 ;
        }
        
        /**
         * Defines the space around elements.
         */
        public function get margin():EdgeMetrics
        {
            return _margin ;
        }
        
        /**
         * @private
         */
        public function set margin( em:EdgeMetrics ):void
        {
           _margin.bottom = em ? em.bottom : 0 ;
           _margin.left   = em ? em.left   : 0 ;
           _margin.right  = em ? em.right  : 0 ;
           _margin.top    = em ? em.top    : 0 ;
        }
        
        /**
         * Indicates if the text in the button is multiline.
         */
        public var multiline:Boolean ;
        
        /**
         * Defines the space inside the border between the border and the actual image or box contents.
         */
        public function get padding():EdgeMetrics
        {
            return _padding ;
        }
        
        /**
         * @private
         */
        public function set padding( em:EdgeMetrics ):void
        {
           _padding.bottom = em ? em.bottom : 0 ;
           _padding.left   = em ? em.left   : 0 ;
           _padding.right  = em ? em.right  : 0 ;
           _padding.top    = em ? em.top    : 0 ;
        }
        
        /**
         * Indicates the selectable value of the label field of the component.
         */
        public var selectable:Boolean ;
        
        /**
         * Indicates if the border of the textfield is visible.
         */
        public var textBorder:Boolean ;
        
        /**
         * The color of the text field border.
         */
        public var textBorderColor:Number ;
        
        /**
         * The color of the text when is disabled.
         */
        public var textDisabled:Number = 0x333333 ;
        
        /**
         * The color of the text when is over.
         */
        public var textRollOver:Number = 0x000000 ;
        
        /**
         * The color of the text when is selected.
         */
        public var textSelected:Number = 0x0099CC ;
        
        /**
         * The alignement of the text in the component (can be Align.CENTER, Align.BOTTOM or Align.TOP only and by default Align.NONE).
         * @see graphics.Align
         */
        public function get textVerticalAlign():uint
        {
            return _textVerticalAlign ;
        }
        
        /**
         * @private
         */
        public function set textVerticalAlign( value:uint ):void
        {
            _textVerticalAlign = textAlignments.indexOf( value ) > -1 ? value : Align.NONE ;
        }
        
        /**
         * The theme style when the component is up.
         */
        public var theme:IFillStyle = new FillStyle( 0x0099CC ) ;
        
        /**
         * The border style when the component is up.
         */
        public var themeBorder:ILineStyle = new LineStyle( 2 , 0x33B5E5 ) ;
        
        /**
         * The border style when the component is disabled.
         */
        public var themeBorderDisabled:ILineStyle = new LineStyle( 2 , 0xA2A2A2 ) ;
        
        /**
         * The border style when the component is over.
         */
        public var themeBorderRollOver:ILineStyle = new LineStyle( 2, 0x0099CC ) ;
        
        /**
         * The border style when the component is selected.
         */
        public var themeBorderSelected:ILineStyle = new LineStyle( 2, 0x33B5E5 ) ;
        
        /**
         * The theme style when the component is disabled.
         */
        public var themeDisabled:IFillStyle = new FillStyle( 0xCCCCCC ) ;
        
        /**
         * The theme style when the component is over.
         */
        public var themeRollOver:IFillStyle = new FillStyle( 0x33B5E5 ) ;
        
        /**
         * The theme style when the component is selected.
         */
        public var themeSelected:IFillStyle = new FillStyle( 0x000000 ) ;
        
        /**
         * The thickness of the glyph edges in this text field.
         */
        public var thickness:Number = 0 ;
        
        /**
         * Indicates if the component use the style text colors.
         */
        public var useTextColor:Boolean = true;
        
        /**
         * The wordwrap value of the fields.
         */
        public var wordWrap:Boolean ;
        
        /**
         * Invoked in the constructor of the <code class="prettyprint">Style</code> instance.
         */
        public override function initialize():void
        {
            _styleSheet = new StyleSheet() ;
            
            _styleSheet.parseCSS
            (<![CDATA[
            p
            {
                color: #FFFFFF;
                fontFamily: Verdana;
                size: 12px;
            }
            ]]>);
        }
        
        /**
         * @private
         */
        hack var _gridFitType:String = GridFitType.PIXEL ;
        
        /**
         * The valid alignment of the icon in the area.
         * @private
         */
        private static const iconAlignments:Vector.<uint> = Vector.<uint>
        ([
            Align.NONE,
            Align.BOTTOM_LEFT , 
            Align.BOTTOM_RIGHT,
            Align.LEFT, 
            Align.RIGHT, 
            Align.TOP_LEFT, 
            Align.TOP_RIGHT
        ]) ;
        
        /**
         * @private
         */
        hack var _iconAlign:uint = Align.LEFT ;
        
        /**
         * @private
         */
        hack const _iconMargin:EdgeMetrics = new EdgeMetrics(2,2,2,2) ;
        
        /**
         * @private
         */
        hack const _margin:EdgeMetrics = new EdgeMetrics() ;
        
        /**
         * @private
         */
        hack const _padding:EdgeMetrics = new EdgeMetrics(2,2,2,2) ;
        
        /**
         * The valid alignment of the text in the box.
         * @private
         */
        hack static const textAlignments:Vector.<uint> = Vector.<uint>
        ([
            Align.NONE,
            Align.CENTER,
            Align.BOTTOM , 
            Align.TOP 
        ]) ;
        
        /**
         * @private
         */
        hack var _textVerticalAlign:uint = Align.NONE ;
    }
}
