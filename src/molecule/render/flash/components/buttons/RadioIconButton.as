﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.components.buttons
{
    import molecule.Builder;
    import molecule.Iconifiable;
    import molecule.Style;
    import molecule.events.ButtonEvent;
    import molecule.events.ComponentEvent;
    import molecule.render.flash.components.labels.IconLabel;
    
    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.net.URLRequest;
    
    /**
     * This button display is a radiobutton component (or checkbox) with an IconTitle inside.
     */
    public class RadioIconButton extends CoreButton implements Iconifiable
    {
        /**
         * Creates a new RadioIconButton instance.
         */
        public function RadioIconButton()
        {
            super() ;
            setPreferredSize( 140 , 18 ) ;
        }
        
        /**
         * Determinates the check visual icon of the button. 
         * This property defines the link id in the library of the internal MovieClip who contains 4 labels "up", "over", "down", "disabled"
         */
        public function get checkIcon():MovieClip
        {
            return (builder as RadioIconButtonBuilder).checkIcon ;
        }
        
        /**
         * @private
         */
        public function set checkIcon( state:MovieClip ):void
        {
            (builder as RadioIconButtonBuilder).attachCheckIcon( state ) ;
        }
        
        /**
         * Determinates the icon of the button. 
         * This property defines the uri of the external bitmap or swf to load.
         */
        public function get icon():*
        {
            return _icon ;
        }
        
        /**
         * @private
         */
        public function set icon( value:* ):void
        {
            _icon = null ;
             if ( value != null && ( value is DisplayObject || value is URLRequest || value is String ) )
             {
                 _icon = value ;
             }
            viewIconChanged() ;
            dispatchEvent( new ButtonEvent( ComponentEvent.ICON_CHANGE ) ) ;
        }
        
        /** 
         * The internal title component of this component.
         */
        public var title:IconLabel ;
        
        /**
         * Returns the Builder constructor use to initialize this component.
         * @return the Builder constructor use to initialize this component.
         */
        protected override function getBuilderRenderer():Builder 
        {
            return new RadioIconButtonBuilder( this ) ;
        }
        
        /**
         * Returns the Style constructor use to initialize this component.
         * @return the Style constructor use to initialize this component.
         */
        protected override function getStyleRenderer():Style 
        {
            return new RadioIconButtonStyle() ;
        }
        
        /**
         * Invoked when the icon property of the component change.
         */
        protected function viewIconChanged():void 
        {
            update() ;
        }
        
        /**
         * @private
         */
        private var _icon:* ;
    }
}
