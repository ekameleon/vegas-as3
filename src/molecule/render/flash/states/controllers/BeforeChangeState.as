﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
*/
package molecule.render.flash.states.controllers 
{
    import molecule.logger;
    import molecule.render.flash.states.process.CloseState;
    import molecule.states.State;
    
    import system.models.ChangeModel;
    import system.process.Chain;
    import system.signals.Receiver;
    
    /**
     * Invoked before the state model is full changed.
     */
    public class BeforeChangeState implements Receiver 
    {
        /**
         * Creates a new BeforeChangeState instance.
         * @param chain the Chain object used to do the transition between the beforeChange and change states. 
         */
        public function BeforeChangeState( chain:Chain = null )
        {
            this.chain = chain ;
        }
        
        /**
         * The state Chain reference of the state engine.
         */
        public var chain:Chain ;
        
        public function receive( ...args:Array ):void
        {
            const state:State       = args[0] as State ;
            const model:ChangeModel = args[1] as ChangeModel ;
            
            logger.info( this + " receive " + state ) ;
            
            if ( chain && state )
            {
                chain.addAction( new CloseState( state ) , 0 , true ) ;
                
                if( model && model.current == null  && !chain.running )
                {
                    chain.run() ;
                }
            }
            else
            {
                logger.warn( this + " failed with the state:" + state+ " and the chain:" + chain ) ;
            }
        }
    }
}
