﻿/*
  Version: MPL 1.1/GPL 2.0/LGPL 2.1
 
  The contents of this file are subject to the Mozilla Public License Version
  1.1 (the "License"); you may not use this file except in compliance with
  the License. You may obtain a copy of the License at
  http://www.mozilla.org/MPL/
  
  Software distributed under the License is distributed on an "AS IS" basis,
  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
  for the specific language governing rights and limitations under the
  License.
  
  The Original Code is [maashaack framework].
  
  The Initial Developers of the Original Code are
  Zwetan Kjukov <zwetan@gmail.com> and Marc Alcaraz <ekameleon@gmail.com>.
  Portions created by the Initial Developers are Copyright (C) 2004-2016
  the Initial Developers. All Rights Reserved.
  
  Contributor(s):
  
  Alternatively, the contents of this file may be used under the terms of
  either the GNU General Public License Version 2 or later (the "GPL"), or
  the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
  in which case the provisions of the GPL or the LGPL are applicable instead
  of those above. If you wish to allow use of your version of this file only
  under the terms of either the GPL or the LGPL, and not to allow others to
  use your version of this file under the terms of the MPL, indicate your
  decision by deleting the provisions above and replace them with the notice
  and other provisions required by the LGPL or the GPL. If you do not delete
  the provisions above, a recipient may use your version of this file under
  the terms of any one of the MPL, the GPL or the LGPL.
 */
package molecule.text
{
    import flash.text.TextField;
    import flash.text.TextFieldAutoSize;
    import flash.text.TextFormat;
    
    /**
     * Splits a text string expression into a collection textfields with a specific size and settings.
     * @param text The text to split.
     * @param width The width component to defines the size of the textfields.
     * @param height The height component to defines the size of the textfields.
     * @param format The TextFormat to set the textfields.
     * @param gridFitType The grid fit type value of the textfields.
     * @param antiAliasType The anti alias type value of the textfields.
     */
    public function splitTextField( text:String , width:Number = 100 , height:Number = 100 , format:TextFormat = null , embedFonts:Boolean = false, autoSize:Boolean = true , gridFitType:String = "pixel" , antiAliasType:String = "advanced" ):Vector.<TextField>
    {
        var fields:Vector.<TextField> = new Vector.<TextField> ;
        
        var base:TextField = new TextField() ;
        
        if( format )
        {
            base.defaultTextFormat = format ;
        }
        
        base.embedFonts = embedFonts ;
        base.multiline  = true ;
        base.wordWrap   = true ;
        
        base.width  = width  ;
        base.height = height ;
        
        base.text = text ;
        
        var field:TextField ;
        var lastChar:uint ;
        
        if( base.maxScrollV > 1 )
        {
            while( ( base.numLines > 0 ) && ( base.numLines > base.bottomScrollV ) )
            {
                if( base.numLines > base.bottomScrollV )
                {
                    lastChar = base.getLineOffset( base.bottomScrollV ) ;
                    
                    field = new TextField() ;
                    
                    if( format )
                    {
                        field.defaultTextFormat = format ;
                    }
                    
                    field.embedFonts = embedFonts ;
                    field.multiline = true ;
                    field.wordWrap  = true ;
                    
                    field.width  = width  ;
                    field.height = height ;
                    
                    if( autoSize )
                    {
                        field.autoSize = TextFieldAutoSize.CENTER ;
                    }
                    
                    field.text = base.text.substring( 0 , lastChar-1 ) ;
                    
                    fields.push( field ) ; 
                    
                    base.replaceText( 0 , lastChar, "" ) ;
                }
            }
        }
        
        if( autoSize )
        {
            base.autoSize = TextFieldAutoSize.CENTER ;
        }
        
        fields.push( base ) ;
        
        return fields.length > 0 ? fields : null ;
    }
}
