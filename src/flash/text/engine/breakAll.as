﻿package flash.text.engine
{
    import flash.text.engine.ElementFormat;
    
    public const breakAll:ElementFormat = new ElementFormat() ;
    {
        breakAll.breakOpportunity = BreakOpportunity.ALL;
    }
}
